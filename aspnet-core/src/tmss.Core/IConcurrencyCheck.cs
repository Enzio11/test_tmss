﻿namespace tmss
{
    public interface IConcurrencyCheck
    {
        byte[] RowVersion { get; set; }
    }
}
