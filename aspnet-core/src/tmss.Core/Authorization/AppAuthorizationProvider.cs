﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace tmss.Authorization
{
  /// <summary>
  /// Application's authorization provider.
  /// Defines permissions for the application.
  /// See <see cref="AppPermissions"/> for all permission names.
  /// </summary>
  public class AppAuthorizationProvider : AuthorizationProvider
  {
    private readonly bool _isMultiTenancyEnabled;

    public AppAuthorizationProvider(bool isMultiTenancyEnabled)
    {
      _isMultiTenancyEnabled = isMultiTenancyEnabled;
    }

    public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
    {
      _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
    }

    public override void SetPermissions(IPermissionDefinitionContext context)
    {
      //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

      var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
      pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

      // Order Methods Master
      var orderMethods = pages.CreateChildPermission(AppPermissions.Pages_OrderMethods, L("OrderMethods"));
      orderMethods.CreateChildPermission(AppPermissions.Pages_OrderMethods_Create, L("CreateNewOrderMethod"));
      orderMethods.CreateChildPermission(AppPermissions.Pages_OrderMethods_Edit, L("EditOrderMethod"));
      orderMethods.CreateChildPermission(AppPermissions.Pages_OrderMethods_Delete, L("DeleteOrderMethod"));

      var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

      var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
      roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
      roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
      roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

      var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
      users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

      var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
      languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
      languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
      languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
      languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

      administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

      var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
      organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
      organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
      organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

      administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

      var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
      webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

      var dynamicParameters = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters, L("DynamicParameters"));
      dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Create, L("CreatingDynamicParameters"));
      dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Edit, L("EditingDynamicParameters"));
      dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Delete, L("DeletingDynamicParameters"));

      var dynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue, L("DynamicParameterValue"));
      dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Create, L("CreatingDynamicParameterValue"));
      dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Edit, L("EditingDynamicParameterValue"));
      dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Delete, L("DeletingDynamicParameterValue"));

      var entityDynamicParameters = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters, L("EntityDynamicParameters"));
      entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Create, L("CreatingEntityDynamicParameters"));
      entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Edit, L("EditingEntityDynamicParameters"));
      entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Delete, L("DeletingEntityDynamicParameters"));

      var entityDynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue, L("EntityDynamicParameterValue"));
      entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Create, L("CreatingEntityDynamicParameterValue"));
      entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Edit, L("EditingEntityDynamicParameterValue"));
      entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Delete, L("DeletingEntityDynamicParameterValue"));

      //TENANT-SPECIFIC PERMISSIONS

      pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

      administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
      administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

      //HOST-SPECIFIC PERMISSIONS

      var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
      editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
      editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
      editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
      editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

      var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
      tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
      tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
      tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
      tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
      tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

      administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
      administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
      administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
      administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);

      // MASTER
      var master = pages.CreateChildPermission(AppPermissions.Pages_Master, L("Master"));
      // MASTER -- Common
      var masterCommon = master.CreateChildPermission(AppPermissions.Pages_Master_Common, L("MasterCommon"));
      var masterSales = master.CreateChildPermission(AppPermissions.Pages_Master_Sales, L("MasterSales"));

            // MASTER -- Sales

            var mstGenDealers = pages.CreateChildPermission(AppPermissions.Pages_MstGenDealers, L("MstGenDealers"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_MstGenDealers_Create, L("CreateNewMstGenDealers"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_MstGenDealers_Edit, L("EditMstGenDealers"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_MstGenDealers_Delete, L("DeleteMstGenDealers"));
            var mstGenProvinces = pages.CreateChildPermission(AppPermissions.Pages_MstGenProvinces, L("MstGenProvinces"));
      mstGenProvinces.CreateChildPermission(AppPermissions.Pages_MstGenProvinces_Create, L("CreateNewMstGenProvinces"));
      mstGenProvinces.CreateChildPermission(AppPermissions.Pages_MstGenProvinces_Edit, L("EditMstGenProvinces"));
      mstGenProvinces.CreateChildPermission(AppPermissions.Pages_MstGenProvinces_Delete, L("DeleteMstGenProvinces"));
       var mstSleColors = pages.CreateChildPermission(AppPermissions.Pages_MstSleColors, L("MstSleColors"));
       mstSleColors.CreateChildPermission(AppPermissions.Pages_MstSleColors_Create, L("CreateNewMstSleColors"));
       mstSleColors.CreateChildPermission(AppPermissions.Pages_MstSleColors_Edit, L("EditMstSleColors"));
       mstSleColors.CreateChildPermission(AppPermissions.Pages_MstSleColors_Delete, L("DeleteMstSleColors"));
       var mstSleYards = pages.CreateChildPermission(AppPermissions.Pages_MstSleYards, L("MstSleYards"));
         mstSleYards.CreateChildPermission(AppPermissions.Pages_MstSleYards_Create, L("CreateNewMstSleYards"));
         mstSleYards.CreateChildPermission(AppPermissions.Pages_MstSleYards_Edit, L("EditMstSleYards"));
         mstSleYards.CreateChildPermission(AppPermissions.Pages_MstSleYards_Delete, L("DeleteMstSleYards"));
       var mstSleAreas = pages.CreateChildPermission(AppPermissions.Pages_MstSleAreas, L("MstSleAreas"));
         mstSleAreas.CreateChildPermission(AppPermissions.Pages_MstSleAreas_Create, L("CreateNewMstSleAreas"));
         mstSleAreas.CreateChildPermission(AppPermissions.Pages_MstSleAreas_Edit, L("EditMstSleAreas"));
            mstSleAreas.CreateChildPermission(AppPermissions.Pages_MstSleAreas_Delete, L("DeleteMstSleAreas"));
            var mstSleLookup = pages.CreateChildPermission(AppPermissions.Pages_MstSleLookup, L("MstSleLookup"));
            mstSleLookup.CreateChildPermission(AppPermissions.Pages_MstSleLookup_Create, L("CreateNewMstSleLookup"));
            mstSleLookup.CreateChildPermission(AppPermissions.Pages_MstSleLookup_Edit, L("EditMstSleLookup"));
            mstSleLookup.CreateChildPermission(AppPermissions.Pages_MstSleLookup_Delete, L("DeleteMstSleLookup"));
            var mstSleDealerGroups = pages.CreateChildPermission(AppPermissions.Pages_MstSleDealerGroups, L("MstSleDealerGroups"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_MstSleDealerGroups_Create, L("CreateNewMstSleDealerGroups"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_MstSleDealerGroups_Edit, L("EditMstSleDealerGroups"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_MstSleDealerGroups_Delete, L("DeleteMstSleDealerGroups"));
            var mstSleLogisticCompanies = pages.CreateChildPermission(AppPermissions.Pages_MstSleLogisticCompanies, L("MstSleLogisticCompanies"));
            mstSleLogisticCompanies.CreateChildPermission(AppPermissions.Pages_MstSleLogisticCompanies_Create, L("CreateNewMstSleLogisticCompanies"));
            mstSleLogisticCompanies.CreateChildPermission(AppPermissions.Pages_MstSleLogisticCompanies_Edit, L("EditMstSleLogisticCompanies"));
            mstSleLogisticCompanies.CreateChildPermission(AppPermissions.Pages_MstSleLogisticCompanies_Delete, L("DeleteMstSleLogisticCompanies"));
            
            //Master Sales Models
            var mstSleModels = masterSales.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model, L("MasterSalesModel"));
            mstSleModels.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_CreateEdit, L("MasterSalesModelCreateEdit"));
            mstSleModels.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Delete, L("MasterSalesModelDelete"));
            // MASTER -- Services
            var masterServices = master.CreateChildPermission(AppPermissions.Pages_Master_Services, L("MasterServices"));
            //Master Service Bank
      var mstSrvBank = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Bank, L("MasterServicesBank"));
      mstSrvBank.CreateChildPermission(AppPermissions.Pages_Master_Services_Bank_CreateEdit, L("MasterServicesBankCreateEdit"));
      mstSrvBank.CreateChildPermission(AppPermissions.Pages_Master_Services_Bank_Delete, L("MasterServicesBankDelete"));

        // Master Service Supplier
      var mstSrvSupplier = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Supplier, L("MasterServicesSupplier"));
      mstSrvSupplier.CreateChildPermission(AppPermissions.Pages_Master_Services_Supplier_CreateEdit, L("MasterServicesSupplierCreateEdit"));
      mstSrvSupplier.CreateChildPermission(AppPermissions.Pages_Master_Services_Supplier_Delete, L("MasterServicesSupplierDelete"));

        //Master Service CustomerType
        var mstSrvCustomerType = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_CustomerType, L("MasterServicesCustomerType"));
            mstSrvCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Services_CustomerType_CreateEdit, L("MasterServicesCustomerTypeCreateEdit"));
            mstSrvCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Services_CustomerType_Delete, L("MasterServicesCustomerTypeDelete"));
        // Master Service DeskAdvisor
        var mstSrvDeskAdvisor = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_DeskAdvisor, L("MasterServicesDeskAdvisor"));
            mstSrvDeskAdvisor.CreateChildPermission(AppPermissions.Pages_Master_Services_DeskAdvisor_CreateEdit, L("MasterServicesDeskAdvisorCreateEdit"));
            mstSrvDeskAdvisor.CreateChildPermission(AppPermissions.Pages_Master_Services_DeskAdvisor_Delete, L("MasterServicesDeskAdvisorDelete"));
        // Master Service Employee
        var mstSrvEmployee = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Employee, L("MasterServicesEmployee"));
            mstSrvEmployee.CreateChildPermission(AppPermissions.Pages_Master_Services_Employee_CreateEdit, L("MasterServicesEmployeeCreateEdit"));
            mstSrvEmployee.CreateChildPermission(AppPermissions.Pages_Master_Services_Employee_Delete, L("MasterServicesEmployeeDelete"));
        // Master Service Division
        var mstSrvDivision = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Division, L("MasterServicesDivision"));
            mstSrvDivision.CreateChildPermission(AppPermissions.Pages_Master_Services_Division_CreateEdit, L("MasterServicesDivisionCreateEdit"));
            mstSrvDivision.CreateChildPermission(AppPermissions.Pages_Master_Services_Division_Delete, L("MasterServicesDivisionDelete"));
        // Master Service Floor
        var mstSrvFloor = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Floor, L("MasterServicesFloor"));
            mstSrvFloor.CreateChildPermission(AppPermissions.Pages_Master_Services_Floor_CreateEdit, L("MasterServicesFloorCreateEdit"));

        // Master Service Floor
        var mstSrvFooter = masterServices.CreateChildPermission(AppPermissions.Pages_Master_Services_Footer, L("MasterServicesFooter"));
            mstSrvFooter.CreateChildPermission(AppPermissions.Pages_Master_Services_Footer_CreateEdit, L("MasterServicesFooterCreateEdit"));
            mstSrvFooter.CreateChildPermission(AppPermissions.Pages_Master_Services_Footer_Delete, L("MasterServicesFooterDelete"));

      // MASTER -- Services Part
      var masterServicesPart = master.CreateChildPermission(AppPermissions.Pages_Master_ServicesPart, L("MasterServicesPart"));
      //Master Service PartsType
      var mstSrvPartsType = masterServicesPart.CreateChildPermission(AppPermissions.Pages_Master_ServicesPart_PartsType, L("MasterServicesPart"));
      mstSrvPartsType.CreateChildPermission(AppPermissions.Pages_Master_ServicesPart_PartsType_CreateEdit, L("MasterServicesPartPartsTypeCreateEdit"));
      mstSrvPartsType.CreateChildPermission(AppPermissions.Pages_Master_ServicesPart_PartsType_Delete, L("MasterServicesPartPartsTypeDelete"));
      // MASTER -- Services Quotation
      var masterServicesQuotation = master.CreateChildPermission(AppPermissions.Pages_Master_ServicesQuotation, L("MasterServicesQuotation"));
      // MASTER -- Services Repair Progress
      var masterServicesRepairProgress = master.CreateChildPermission(AppPermissions.Pages_Master_ServicesRepairProgress, L("MasterServicesRepairProgress"));
      var mstSrvBPGroup = masterServicesRepairProgress.CreateChildPermission(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup, L("MasterServicesRepairProgressBpGroup"));
      mstSrvBPGroup.CreateChildPermission(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_List, L("MasterServicesRepairProgressBpGroupList"));
      mstSrvBPGroup.CreateChildPermission(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_CreateEdit, L("MasterServicesRepairProgressBpGroupCreateEdit"));
      mstSrvBPGroup.CreateChildPermission(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_Delete, L("MasterServicesRepairProgressBpGroupDelete"));
      // MASTER -- Services Warranty
      var masterServicesWarranty = master.CreateChildPermission(AppPermissions.Pages_Master_ServicesWarranty, L("MasterServicesWarranty"));

      // SALES
      var sales = pages.CreateChildPermission(AppPermissions.Pages_Sales, L("Sales"));
      // SALES -- Contract
      var salesContract = sales.CreateChildPermission(AppPermissions.Pages_Sales_Contract, L("SalesContract"));
      // SALES -- Vehicle
      var salesVehicle = sales.CreateChildPermission(AppPermissions.Pages_Sales_Vehicle, L("SalesVehicle"));

      // SERVICES
      var services = pages.CreateChildPermission(AppPermissions.Pages_Services, L("Services"));
      // SERVICES -- Part
      var servicesPart = services.CreateChildPermission(AppPermissions.Pages_Services_Part, L("ServicesPart"));
      // SERVICES -- Quotation
      var servicesQuotation = services.CreateChildPermission(AppPermissions.Pages_Services_Quotation, L("ServicesQuotation"));
      // SERVICES -- Repair Progress
      var servicesRepairProgress = services.CreateChildPermission(AppPermissions.Pages_Services_RepairProgress, L("ServicesRepairProgress"));
      // SERVICES -- Warranty
      var servicesWarranty = services.CreateChildPermission(AppPermissions.Pages_Services_Warranty, L("ServicesWarranty"));
    }

    private static ILocalizableString L(string name)
    {
      return new LocalizableString(name, tmssConsts.LocalizationSourceName);
    }
  }
}