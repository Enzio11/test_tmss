﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Services
{
    [Table("MstSrvEmployee")]
    public partial class MstSrvEmployee : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [Required]
        [StringLength(20)]
        public string EmpCode { get; set; }
        [Required]
        [StringLength(50)]
        public string EmpName { get; set; }
        [StringLength(20)]
        public string Tel { get; set; }
        public long? Sex { get; set; }
        [StringLength(100)]
        public string EmpAddress { get; set; }
        public long? TypeId { get; set; }
        public long? TitleId { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(30)]
        public string EmpColor { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? StartDate { get; set; }
        [StringLength(1)]
        public byte[] EmpImg { get; set; }
        [Required]
        [StringLength(1)]
        public string Status { get; set; }
        public long? IsWork { get; set; }
        public long? TypeJob { get; set; }
        public int TenantId { get; set; }
        public long? DivId { get; set; }

        public MstSrvDeskAdvisor DeskAdvisor { get; set; }
    }
}