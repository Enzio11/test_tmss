﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Services
{
    [Table("MstSrvFloor")]
    public partial class MstSrvFloor : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [StringLength(200)]
        public string FloorName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        public long? Ordering { get; set; }
        [StringLength(1)]
        public string Type { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
    }
}