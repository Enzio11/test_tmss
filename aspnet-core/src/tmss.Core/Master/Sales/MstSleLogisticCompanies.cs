﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace tmss.Master.Sales
{
    [Table("MstSleLogisticCompanies")]
    public class MstSleLogisticCompanies : FullAuditedEntity<long>,IEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }


        [Required]
        public virtual string Code { get; set; }

        [Required]
        public virtual string VnName { get; set; }

        [Required]
        public virtual string EnName { get; set; }

        public virtual string TaxCode { get; set; }

        public virtual string BankNo { get; set; }

        public virtual string Tel { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Address { get; set; }

        public virtual string ContactPerson { get; set; }

        public virtual string Description { get; set; }

        public virtual string OwnerType { get; set; }

        [Required]
        public virtual string Status { get; set; }

        public virtual decimal? Ordering { get; set; }

        public virtual long? LogisticsNumTofs { get; set; }
    }
}