﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Sales
{
    [Table("MstSleModel")]
    public class MstSleModels : FullAuditedEntity<long>, IEntity<long>
    {
        public string MarketingCode { get; set; }
        [StringLength(50)]
        public string ProductionCode { get; set; }
        [StringLength(50)]
        public string EnName { get; set; }
        [StringLength(50)]
        public string VnName { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        [Required]
        [StringLength(50)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal? OrderingRpt { get; set; }
    }
}
