﻿using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_BP_GROUP_EMP")] // ~ SRV_M_WSHOP_BP_GROUP_EMP
  public class MstSrvBPGroupEmp : FullAuditedEntity
  {
    public virtual MstSrvBPGroup MstSrvBPGroup { get; set; }

    // TODO: chuyen thanh bang Employee
    public virtual int EmployeeId { get; set; }
  }
}