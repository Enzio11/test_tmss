﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_BP_GROUP")] // ~ SRV_M_WSHOP_BP_GROUP
  public class MstSrvBPGroup : FullAuditedEntity, IMustHaveTenant
  {
    public virtual string GroupName { get; set; }
    public virtual string GroupDesc { get; set; }
    public virtual int Ordering { get; set; }
    public virtual bool IsActive { get; set; }

    public virtual int TenantId { get; set; }
  }
}