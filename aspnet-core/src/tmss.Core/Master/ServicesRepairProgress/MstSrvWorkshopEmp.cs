﻿using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_WORKSHOP_EMP")] // ~ SRV_M_EMP_WSHOP
  public class MstSrvWorkshopEmp : FullAuditedEntity
  {
    public virtual MstSrvWorkshop MstSrvWorkshop { get; set; }

    // TODO: chuyen thanh bang Employee
    public virtual int EmployeeId { get; set; }
  }
}