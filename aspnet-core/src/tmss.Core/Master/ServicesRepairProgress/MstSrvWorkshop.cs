﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_WORKSHOP")] // ~ SRV_M_WSHOP
  public class MstSrvWorkshop : FullAuditedEntity, IMustHaveTenant
  {
    public virtual string WorkshopCode { get; set; }
    public virtual string WorkshopName { get; set; }
    public virtual string WorkshopDesc { get; set; }
    public virtual int Ordering { get; set; }
    public virtual bool IsActive { get; set; }

    public virtual MstSrvWorkshopType MstSrvWorkshopType { get; set; }

    public virtual int TenantId { get; set; }
  }
}