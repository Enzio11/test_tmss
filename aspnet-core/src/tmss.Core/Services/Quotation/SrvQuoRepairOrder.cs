﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Services.Quotation
{
    [Table("SrvQuoRepairOrder")]
    public class SrvQuoRepairOrder : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public long CusVsId { get; set; }

        //[StringLength(50)]
        public string RepairOrderNo { get; set; }

        //[StringLength(50)]
        public string RoType { get; set; }

        //[StringLength(50)]
        public string RoState { get; set; }

        //[StringLength(255)]
        public string ReqDesc { get; set; }

        //[StringLength(50)]
        public string InrState { get; set; }

        public double? TestPaperPrint { get; set; }

        public double? QuotationPrint { get; set; }

        public long? ReadJustRoId { get; set; }

        public double? RcTypeId { get; set; }

        //[StringLength(50)]
        public string CusWait { get; set; }

        //[StringLength(500)]
        public string ReadJustFixNote { get; set; }

        //[StringLength(500)]
        public string ReadJustReason { get; set; }

        //[StringLength(20)]
        public string Attribute1 { get; set; }

        //[StringLength(50)]
        public string IsCarWash { get; set; }

        //[StringLength(50)]
        public string GetOldParts { get; set; }

        //[StringLength(50)]
        public string RoChange { get; set; }

        public long? CheckTlccpt { get; set; }

        //[Column("OpenRODate", TypeName = "datetime")]
        public DateTime? OpenRoDate { get; set; }

        //[Column("KCalc")]
        //[StringLength(50)]
        public string Kcalc { get; set; }

        //[Column(TypeName = "datetime")]
        public DateTime? CloseRoDate { get; set; }

        //[StringLength(20)]
        public string Attribute2 { get; set; }

        //[Column("InsertQVersion")]
        //[StringLength(50)]
        public string InsertQVersion { get; set; }

        //[Required]
        //[StringLength(50)]
        public string UseProgress { get; set; }

        //[StringLength(50)]
        public string FreePm { get; set; }

        public long? Km { get; set; }

        public long? QcLevel { get; set; }

        public long? TotalAmount { get; set; }

        public long? TotalDiscount { get; set; }

        public long? TotalTaxAmount { get; set; }

        //[Column("GId")]
        public long? Gid { get; set; }

        public string Notes { get; set; }

        //[StringLength(1)]
        public string IsPriority { get; set; }

        public long? EstimateTime { get; set; }

        //[Column(TypeName = "datetime")]
        public DateTime? CarDeliveryTime { get; set; }

        public long? CampaignId { get; set; }

        //[StringLength(1)]
        public string IsCusWait { get; set; }

        //[StringLength(1)]
        public string IsTakeParts { get; set; }

        //[Column(TypeName = "datetime")]
        public DateTime? StartRepairTime { get; set; }

        //[Column(TypeName = "datetime")]
        public DateTime? StartCarWashTime { get; set; }

        //[StringLength(500)]
        public string PrintLog { get; set; }

        public int TenantId { get; set; }
    }
}
