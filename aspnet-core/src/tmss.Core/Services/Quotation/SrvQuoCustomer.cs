﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Services.Quotation
{
    [Table("SrvQuoCustomers")]
    public class SrvQuoCustomer : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public long? CusTypeId { get; set; }

        //[StringLength(20)]
        public string CusNo { get; set; }

        //[StringLength(300)]
        public string CarOwnerName { get; set; }

        //[StringLength(100)]
        public string CarOwnerIdNum { get; set; }

        //[StringLength(300)]
        public string CarOwnerAdd { get; set; }

        //[StringLength(25)]
        public string CarOwnerTel { get; set; }

        //[StringLength(25)]
        public string CarOwnerMobile { get; set; }

        //[StringLength(20)]
        public string CarOwnerFax { get; set; }

        //[StringLength(100)]
        public string CarOwnerEmail { get; set; }

        //[StringLength(50)]
        public string TaxCode { get; set; }

        public long? BankId { get; set; }

        //[StringLength(30)]
        public string AccNo { get; set; }

        //[StringLength(1)]
        public string CusVerify { get; set; }

        //[StringLength(1)]
        public string CusMrs { get; set; }

        //[StringLength(2000)]
        public string OrgName { get; set; }

        //[StringLength(1)]
        public string CallTime { get; set; }

        //[StringLength(1)]
        public string CurFir { get; set; }

        public long? CusContact { get; set; }

        public long? ProvinceId { get; set; }

        public long? DistrictId { get; set; }

        public long? SalesCustormerId { get; set; }

        //[StringLength(255)]
        public string RelativesName { get; set; }

        //[StringLength(255)]
        public string RelativesAddress { get; set; }

        //[StringLength(50)]
        public string RelationshipId { get; set; }

        //[Column("RelativesPrOId")]
        public long? RelativesPrOId { get; set; }

        //[StringLength(255)]
        public string RelativesPhone { get; set; }

        //[StringLength(1)]
        public string Status { get; set; }

        public long? Ordering { get; set; }

        public long? RefId { get; set; }

        public int TenantId { get; set; }

        //public IList<SrvQuoCustomerD> SrvQuoCustomerDs {get; set;}

        //public IList<SrvQuoCusVisit> SrvQuoCusVisits { get; set; }
    }
}
