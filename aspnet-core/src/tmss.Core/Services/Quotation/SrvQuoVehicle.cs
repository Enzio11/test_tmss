﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Services.Quotation
{
    [Table("SrvQuoVehicles")]
    public class SrvQuoVehicle : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        //[StringLength(20)]
        public string RegisterNo { get; set; }

        public long? CmId { get; set; }

        public long? EngineTypeId { get; set; }

        //[StringLength(20)]
        public string EngineNo { get; set; }

        //[StringLength(20)]
        public string FrameNo { get; set; }

        //[StringLength(20)]
        public string VinNo { get; set; }

        public long? VcId { get; set; }

        //[StringLength(1)]
        public string Ckd { get; set; }

        //[Column(TypeName = "datetime")]
        public DateTime? DeliveryDate { get; set; }

        //[StringLength(50)]
        public string NtCode { get; set; }

        public long? SalesCflId { get; set; }

        //[StringLength(50)]
        public string FullModel { get; set; }

        //[StringLength(1)]
        public string Pi { get; set; }

        //[StringLength(1)]
        public string Hybrid { get; set; }

        //[StringLength(1)]
        public string Status { get; set; }

        public long? Ordering { get; set; }

        public long? VhcType { get; set; }

        public long? RefId { get; set; }

        public int TenantId { get; set; }

        public IList<SrvQuoCusVisit> SrvQuoCusVisits { get; set; }
    }
}
