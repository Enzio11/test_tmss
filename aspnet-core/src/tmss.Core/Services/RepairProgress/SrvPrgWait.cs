﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using tmss.Core.Shared.Services.RepairProgress;

namespace tmss.Services.RepairProgress
{
  // [Table("SRV_PRG_WAIT")]  // ~ SRV_B_WSHOP_WAIT_PROCESS
  public class SrvPrgWait : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
  {
    // TODO: chuyen thanh cac bang tuong ung
    public virtual int? AppointmentId { get; set; }
    public virtual int? CustomerVisitId { get; set; }
    public virtual int? ROId { get; set; }
    public virtual int VehicleId { get; set; }
    public virtual int CustomerId { get; set; }

    public virtual ROType JobType { get; set; }
    public virtual QCLevel QCLevel { get; set; }
    public virtual bool? IsCustomerWait { get; set; }
    public virtual bool? IsCarWash { get; set; }
    public virtual bool? IsTakeParts { get; set; }
    public virtual bool? IsPriority { get; set; }
    public virtual RPState? WaitState { get; set; }

    public virtual int TenantId { get; set; }
  }
}