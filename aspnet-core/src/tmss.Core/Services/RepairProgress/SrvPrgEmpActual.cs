﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using tmss.Core.Shared.Services.RepairProgress;
using tmss.Master.ServicesRepairProgress;

namespace tmss.Services.RepairProgress
{
  // [Table("SRV_PRG_EMP_ACTUAL")] // ~ SRV_B_RO_WSHOP_EMP_ACTUAL
  public class SrvPrgEmpActual : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
  {
    // TODO: chuyen thanh bang DEALER, EMPLOYEE
    public virtual int EmployeeId { get; set; }

    public virtual SrvPrgPlan SrvPrgPlan { get; set; }
    public virtual SrvPrgActual SrvPrgActual { get; set; }
    public virtual SrvPrgEmpPlan SrvPrgEmpPlan { get; set; }
    public virtual MstSrvWorkshop MstSrvWorkshop { get; set; }
    public virtual MstSrvBPProcess MstSrvBPProcess { get; set; }
    public virtual MstSrvBPGroup MstSrvBPGroup { get; set; }

    public virtual ROType JobType { get; set; }
    public virtual DateTime? EmployeeActualFromTime { get; set; }
    public virtual DateTime? EmployeeActualToTime { get; set; }
    public virtual int? EmployeeActualCalcTime { get; set; }
    public virtual int? EmployeeActualState { get; set; }

    public virtual int TenantId { get; set; }
  }
}