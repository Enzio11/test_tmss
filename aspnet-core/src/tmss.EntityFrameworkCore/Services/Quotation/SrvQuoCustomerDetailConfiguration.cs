﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.Quotation
{
    // [Table("SRV_D_CUSTOMERS_D")] // ~ SrvQuoCustomersD
    public class SrvQuoCustomerDetailConfiguration : IEntityTypeConfiguration<SrvQuoCustomerDetail>
    {
        public void Configure(EntityTypeBuilder<SrvQuoCustomerDetail> builder)
        {
            // Columns
            builder.ToTable("SrvQuoCustomerDetails");
            builder.Property(t => t.CusId).IsRequired(false);
            builder.Property(t => t.Name).IsRequired(false).HasMaxLength(100);
            builder.Property(t => t.IdNumber).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.Tel).IsRequired(false).HasMaxLength(40);
            builder.Property(t => t.Mobil).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.Address).IsRequired(false).HasMaxLength(200);
            builder.Property(t => t.Email).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.Type).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CallTime).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CusVerify).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Status).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Ordering).IsRequired(false);

            // Key and Relationships
            //builder.HasOne(t => t.srvQuoCustomer).WithMany().HasForeignKey("CusId").OnDelete(DeleteBehavior.Restrict);
        }
    }
}
