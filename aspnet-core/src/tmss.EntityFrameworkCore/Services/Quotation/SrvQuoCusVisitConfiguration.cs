﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.Quotation
{
    // [Table("SRV_B_CUSVISIT")] // ~ SrvQuoCusVisit
    public class SrvQuoCusVisitConfiguration : IEntityTypeConfiguration<SrvQuoCusVisit>
    {
        public void Configure(EntityTypeBuilder<SrvQuoCusVisit> builder)
        {
            // Columns
            builder.ToTable("SrvQuoCusVisit");
            builder.Property(t => t.DlrNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.VhcId).IsRequired(false);
            builder.Property(t => t.RegisterNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.Pds).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CusId).IsRequired(false);
            builder.Property(t => t.CusType).IsRequired(false);
            builder.Property(t => t.Km).IsRequired(false);
            builder.Property(t => t.CallFir).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.LetterFir).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.OtherFir).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CallTime).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.ReqDesc).IsRequired(false).HasMaxLength(1000);
            builder.Property(t => t.Notes).IsRequired(false).HasMaxLength(500);
            builder.Property(t => t.WorkType).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CusState).IsRequired(false);
            builder.Property(t => t.Gid).IsRequired(false);
            builder.Property(t => t.OldCusVsId).IsRequired(false);
            builder.Property(t => t.CloseRoDate).IsRequired(false);
            builder.Property(t => t.MeetCus).IsRequired(false);
            builder.Property(t => t.FirstMeetCus).IsRequired(false);
            builder.Property(t => t.CusDId).IsRequired(false);
            builder.Property(t => t.CustomerId).IsRequired(false);
        }
    }
}
