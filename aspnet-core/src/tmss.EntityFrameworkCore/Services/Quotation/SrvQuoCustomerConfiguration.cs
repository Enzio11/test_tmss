﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.Quotation
{
    public class SrvQuoCustomerConfiguration : IEntityTypeConfiguration<SrvQuoCustomer>
    {
        // [Table("SRV_D_CUSTOMERS")] // ~ SrvQuoCustomers
        public void Configure(EntityTypeBuilder<SrvQuoCustomer> builder)
        {
            // Columns
            builder.ToTable("SrvQuoCustomers");
            builder.Property(t => t.CusTypeId).IsRequired(false);
            builder.Property(t => t.CusNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.CarOwnerName).IsRequired(true).HasMaxLength(300);
            builder.Property(t => t.CarOwnerIdNum).IsRequired(false).HasMaxLength(100);
            builder.Property(t => t.CarOwnerAdd).IsRequired(true).HasMaxLength(300);
            builder.Property(t => t.CarOwnerTel).IsRequired(true).HasMaxLength(25);
            builder.Property(t => t.CarOwnerMobile).IsRequired(false).HasMaxLength(25);
            builder.Property(t => t.CarOwnerFax).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.CarOwnerEmail).IsRequired(false).HasMaxLength(100);
            builder.Property(t => t.TaxCode).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.BankId).IsRequired(false);
            builder.Property(t => t.AccNo).IsRequired(false).HasMaxLength(30);
            builder.Property(t => t.CusVerify).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CusMrs).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.OrgName).IsRequired(false).HasMaxLength(2000);
            builder.Property(t => t.CallTime).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CurFir).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.CusContact).IsRequired(false);
            builder.Property(t => t.ProvinceId).IsRequired(false);
            builder.Property(t => t.DistrictId).IsRequired(false);
            builder.Property(t => t.SalesCustormerId).IsRequired(false);
            builder.Property(t => t.RelativesName).IsRequired(false).HasMaxLength(255);
            builder.Property(t => t.RelativesAddress).IsRequired(false).HasMaxLength(255);
            builder.Property(t => t.RelationshipId).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.RelativesPrOId).IsRequired(false).HasMaxLength(255);
            builder.Property(t => t.RelativesPhone).IsRequired(false).HasMaxLength(255);
            builder.Property(t => t.Status).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Ordering).IsRequired(false);
            builder.Property(t => t.RefId).IsRequired(false);

            // Key and Relationships
        }
    }
}