﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.Quotation
{
    public class SrvQuoVehicleConfiguration : IEntityTypeConfiguration<SrvQuoVehicle>
    {
        public void Configure(EntityTypeBuilder<SrvQuoVehicle> builder)
        {
            // Columns
            builder.ToTable("SrvQuoVehicles");
            builder.Property(t => t.RegisterNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.CmId).IsRequired(false);
            builder.Property(t => t.EngineTypeId).IsRequired(false);
            builder.Property(t => t.EngineNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.FrameNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.VinNo).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.VcId).IsRequired(false);
            builder.Property(t => t.Ckd).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.DeliveryDate).IsRequired(false);
            builder.Property(t => t.NtCode).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.SalesCflId).IsRequired(false);
            builder.Property(t => t.FullModel).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.Pi).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Hybrid).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Status).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.Ordering).IsRequired(false);
            builder.Property(t => t.VhcType).IsRequired(false);
            builder.Property(t => t.RefId).IsRequired(false);
        }
    }
}
