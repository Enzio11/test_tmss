﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.RepairProgress
{
  public class SrvPrgWaitConfiguration : IEntityTypeConfiguration<SrvPrgWait>
  {
    // [Table("SRV_PRG_WAIT")]  // ~ SRV_B_WSHOP_WAIT_PROCESS
    public void Configure(EntityTypeBuilder<SrvPrgWait> builder)
    {
      builder.ToTable("SRV_PRG_WAIT");
      builder.Property(t => t.JobType).HasColumnName("RO_TYPE").IsRequired(true);
      builder.Property(t => t.QCLevel).HasColumnName("QC_LEVEL").IsRequired(true);
      builder.Property(t => t.IsCustomerWait).HasColumnName("IS_CUS_WAIT").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsTakeParts).HasColumnName("IS_TAKE_PARTS").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsPriority).HasColumnName("IS_PRIORITY").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsCarWash).HasColumnName("IS_CAR_WASH").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.WaitState).HasColumnName("STATE").IsRequired(false);

      // TODO: chuyen thanh bang tuong ung
      builder.Property(t => t.CustomerVisitId).HasColumnName("CUS_VS_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("CUS_VS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.ROId).HasColumnName("RO_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("RO_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.AppointmentId).HasColumnName("APP_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("APP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.VehicleId).HasColumnName("VEH_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("VEH_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.CustomerId).HasColumnName("CUS_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("CUS_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}