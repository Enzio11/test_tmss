﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.RepairProgress
{
  public class SrvPrgPlanConfiguration : IEntityTypeConfiguration<SrvPrgPlan>
  {
    // [Table("SRV_PRG_PLAN")]  // ~ SRV_B_RO_WSHOPS_PLAN
    public void Configure(EntityTypeBuilder<SrvPrgPlan> builder)
    {
      builder.ToTable("SRV_PRG_PLAN");
      builder.Property(t => t.JobType).HasColumnName("RO_TYPE").IsRequired(true);
      builder.Property(t => t.QCLevel).HasColumnName("QC_LEVEL").IsRequired(true);
      builder.Property(t => t.IsCustomerWait).HasColumnName("IS_CUS_WAIT").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsTakeParts).HasColumnName("IS_TAKE_PARTS").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsPriority).HasColumnName("IS_PRIORITY").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsCarWash).HasColumnName("IS_CAR_WASH").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.PlanFromTime).HasColumnName("FROM_TIME").IsRequired(false);
      builder.Property(t => t.PlanToTime).HasColumnName("TO_TIME").IsRequired(false);
      builder.Property(t => t.PlanCalcTime).HasColumnName("CALC_TIME").IsRequired(false);

      // TODO: chuyen thanh bang tuong ung
      builder.Property(t => t.CustomerVisitId).HasColumnName("CUS_VS_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("CUS_VS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.ROId).HasColumnName("RO_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("RO_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.AppointmentId).HasColumnName("APP_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("APP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.VehicleId).HasColumnName("VEH_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("VEH_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.CustomerId).HasColumnName("CUS_ID");

      builder.HasOne(t => t.MstSrvWorkshop).WithMany().HasForeignKey("WS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPProcess).WithMany().HasForeignKey("BP_PROCESS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPGroup).WithMany().HasForeignKey("BP_GROUP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.SrvPrgActual).WithMany().HasForeignKey("ACTUAL_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}