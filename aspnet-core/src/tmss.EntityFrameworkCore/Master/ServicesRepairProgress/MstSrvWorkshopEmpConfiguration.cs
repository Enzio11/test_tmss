﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvWorkshopEmpConfiguration : IEntityTypeConfiguration<MstSrvWorkshopEmp>
  {
    // [Table("MST_SRV_WORKSHOP_EMP")] // ~ SRV_M_EMP_WSHOP
    public void Configure(EntityTypeBuilder<MstSrvWorkshopEmp> builder)
    {
      builder.ToTable("MST_SRV_WORKSHOP_EMP");
      builder.HasOne(t => t.MstSrvWorkshop).WithMany().HasForeignKey("WS_ID").OnDelete(DeleteBehavior.Restrict);

      // TODO: chuyen thanh bang EMPLOYEE
      builder.Property(t => t.EmployeeId).HasColumnName("EMP_ID").IsRequired(true);
      //builder.HasOne(t => t.EMPLOYEE_TABLE).WithMany().HasForeignKey("EMP_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}