﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvWorkshopTypeConfiguration : IEntityTypeConfiguration<MstSrvWorkshopType>
  {
    // [Table("MST_SRV_WORKSHOP_TYPE")] // ~ SRV_M_WSHOP_TYPE
    public void Configure(EntityTypeBuilder<MstSrvWorkshopType> builder)
    {
      builder.ToTable("MST_SRV_WORKSHOP_TYPE");
      builder.Property(t => t.WorkshopTypeCode).HasColumnName("WS_TYPE_CODE").IsRequired(true).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.WorkshopTypeName).HasColumnName("WS_TYPE_NAME").IsRequired(false).HasMaxLength(Constants.MaxDescLength);
      builder.Property(t => t.WorkshopTypeDesc).HasColumnName("WS_TYPE_DESC").IsRequired(false).HasMaxLength(Constants.MaxDescLength);
      builder.Property(t => t.ColorCode).HasColumnName("COLOR_CODE").IsRequired(false).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.IsActive).HasColumnName("IS_ACTIVE").HasDefaultValue(true);
    }
  }
}