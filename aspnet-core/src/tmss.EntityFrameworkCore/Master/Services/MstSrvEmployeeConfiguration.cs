﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Services
{
    public class MstSrvEmployeeConfiguration : IEntityTypeConfiguration<MstSrvEmployee>
    {
        public void Configure(EntityTypeBuilder<MstSrvEmployee> builder)
        {
            builder.ToTable("MstSrvEmployee").HasIndex(e => new { e.EmpName, e.EmpCode });
        }
    }
}
