﻿using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using tmss.Authorization.Delegation;
using tmss.Authorization.Roles;
using tmss.Authorization.Users;
using tmss.Chat;
using tmss.Core.Master.Services;
using tmss.Core.Master.ServicesPart;
using tmss.Editions;
using tmss.Friendships;
using tmss.Master;
using tmss.Master.Sale;
using tmss.Master.Sales;
using tmss.Master.Services;
using tmss.Master.ServicesRepairProgress;
using tmss.MultiTenancy;
using tmss.MultiTenancy.Accounting;
using tmss.MultiTenancy.Payments;
using tmss.Services.Quotation;
using tmss.Services.RepairProgress;
using tmss.Storage;

namespace tmss.EntityFrameworkCore
{
    public class tmssDbContext : AbpZeroDbContext<Tenant, Role, User, tmssDbContext>, IAbpPersistedGrantDbContext
  {
    /* Define an IDbSet for each entity of the application */

    public virtual DbSet<BinaryObject> BinaryObjects { get; set; }
    public virtual DbSet<Friendship> Friendships { get; set; }
    public virtual DbSet<ChatMessage> ChatMessages { get; set; }
    public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }
    public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }
    public virtual DbSet<Invoice> Invoices { get; set; }
    public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }
    public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }
    public virtual DbSet<UserDelegation> UserDelegations { get; set; }

    /*ADD NEW HERE - BEGIN*/
   // public virtual DbSet<OrderMethod> OrderMethods { get; set; }
    public virtual DbSet<MstSrvBPGroup> MstSrvBPGroups { get; set; }
    public virtual DbSet<MstSrvBPGroupEmp> MstSrvBPGroupEmps { get; set; }
    public virtual DbSet<MstSrvBPProcess> MstSrvBPProcesses { get; set; }
    public virtual DbSet<MstSrvWorkshop> MstSrvWorkshops { get; set; }
    public virtual DbSet<MstSrvWorkshopEmp> MstSrvWorkshopEmps { get; set; }
    public virtual DbSet<MstSrvWorkshopType> MstSrvWorkshopTypes { get; set; }
    public virtual DbSet<SrvPrgActual> SrvPrgActuals { get; set; }
    public virtual DbSet<SrvPrgEmpActual> SrvPrgEmpActuals { get; set; }
    public virtual DbSet<SrvPrgEmpPlan> SrvPrgEmpPlans { get; set; }
    public virtual DbSet<SrvPrgPlan> SrvPrgPlans { get; set; }
    public virtual DbSet<SrvPrgWait> SrvPrgWaits { get; set; }

    //Master Services
    public virtual DbSet<MstSrvBank> MstSrvBanks { get; set; }
    public virtual DbSet<MstSrvPartsType> MstSrvPartsType { get; set; }
    public virtual DbSet<MstSrvSupplier> MstSrvSupplier { get; set; }
    public virtual DbSet<MstSrvCustomerType> MstSrvCustomerTypes { get; set; }
    public virtual DbSet<MstSrvDeskAdvisor> MstSrvDeskAdvisors { get; set; }
    public virtual DbSet<MstGenDealers> MstGenDealers { get; set; }
    public virtual DbSet<MstGenProvinces> MstGenProvinces { get; set; }
    public virtual DbSet<MstSleLocation>MstSleLocations { get; set; }
    public virtual DbSet<MstSleModels> MstSleModels { get; set; }
    public virtual DbSet<MstSleColors> MstSleColors { get; set; }
    public virtual DbSet<MstSleYards> MstSleYards { get; set; }
    public virtual DbSet<MstSleAreas> MstSleAreas { get; set; }
    public virtual DbSet<MstSleLookup> MstSleLookup { get; set; }
    public virtual DbSet<MstSrvEmployee> MstSrvEmployees { get; set; }
    public virtual DbSet<MstSrvDivision> MstSrvDivisions { get; set; }
    public virtual DbSet<MstSrvDlrConfig> MstSrvDlrConfigs { get; set; }
    public virtual DbSet<MstSrvDlrCalendar> MstSrvDlrCalendars { get; set; }
    public virtual DbSet<MstSrvTitle> MstSrvTitles { get; set; }
    public virtual DbSet<MstSrvJobType> MstSrvJobTypes { get; set; }
    public virtual DbSet<MstSrvEmployeeType> MstSrvEmployeeTypes { get; set; }
    public virtual DbSet<MstSrvFloor> MstSrvFloors { get; set; }
    public virtual DbSet<MstSrvFooter> MstSrvFooters { get; set; }
    public virtual DbSet<MstSrvFooterTmv> MstSrvFooterTmvs { get; set; }
    public virtual DbSet<MstSrvFooterReportType> MstSrvFooterReportTypes { get; set; }
    //Services Quotations
    public virtual DbSet<SrvQuoVehicle> SrvQuoVehicles { get; set; }
    public virtual DbSet<SrvQuoCusVisit> SrvQuoCusVisits { get; set; }
    public virtual DbSet<SrvQuoAppointment> SrvQuoAppointments { get; set; }
    public virtual DbSet<SrvQuoCustomer> SrvQuoCustomers { get; set; }
    public virtual DbSet<SrvQuoCustomerDetail> SrvQuoCustomerDetails { get; set; }
    public virtual DbSet<SrvQuoRepairOrder> SrvQuoRepairOrders { get; set; }
    public virtual DbSet<SrvQuoCusVehicle> SrvQuoCusVehicles { get; set; }
    public virtual DbSet<SrvQuoInOutGate> SrvQuoInOutGates { get; set; }
    public virtual DbSet<MstSleDealerGroups> MstSleDealerGroups { get; set; }
    public virtual DbSet<MstSleLogisticCompanies> MstSleLogisticCompanies { get; set; }
        /*ADD NEW HERE - END*/

        public tmssDbContext(DbContextOptions<tmssDbContext> options)
    : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<BinaryObject>(b =>
      {
        b.HasIndex(e => new { e.TenantId });
      });

      modelBuilder.Entity<ChatMessage>(b =>
      {
        b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
        b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
        b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
        b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
      });

      modelBuilder.Entity<Friendship>(b =>
      {
        b.HasIndex(e => new { e.TenantId, e.UserId });
        b.HasIndex(e => new { e.TenantId, e.FriendUserId });
        b.HasIndex(e => new { e.FriendTenantId, e.UserId });
        b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
      });

      modelBuilder.Entity<Tenant>(b =>
      {
        b.HasIndex(e => new { e.SubscriptionEndDateUtc });
        b.HasIndex(e => new { e.CreationTime });
      });

      modelBuilder.Entity<SubscriptionPayment>(b =>
      {
        b.HasIndex(e => new { e.Status, e.CreationTime });
        b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
      });

      modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
      {
        b.HasQueryFilter(m => !m.IsDeleted)
          .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
          .IsUnique();
      });

      modelBuilder.Entity<UserDelegation>(b =>
      {
        b.HasIndex(e => new { e.TenantId, e.SourceUserId });
        b.HasIndex(e => new { e.TenantId, e.TargetUserId });
      });

      /*ADD NEW HERE - BEGIN*/
      // Master Services
      modelBuilder.ApplyConfiguration(new MstSrvEmployeeConfiguration());

            //Services Quotation
     modelBuilder.ApplyConfiguration(new SrvQuoCustomerConfiguration());
    modelBuilder.ApplyConfiguration(new SrvQuoCusVisitConfiguration());
    modelBuilder.ApplyConfiguration(new SrvQuoVehicleConfiguration());
           modelBuilder.ApplyConfiguration(new SrvQuoCustomerDetailConfiguration());
    // Repair Progress
    modelBuilder.ApplyConfiguration(new MstSrvBPGroupConfiguration());
      modelBuilder.ApplyConfiguration(new MstSrvBPGroupEmpConfiguration());
      modelBuilder.ApplyConfiguration(new MstSrvBPProcessConfiguration());
      modelBuilder.ApplyConfiguration(new MstSrvWorkshopConfiguration());
      modelBuilder.ApplyConfiguration(new MstSrvWorkshopEmpConfiguration());
      modelBuilder.ApplyConfiguration(new MstSrvWorkshopTypeConfiguration());
      modelBuilder.ApplyConfiguration(new SrvPrgActualConfiguration());
      modelBuilder.ApplyConfiguration(new SrvPrgEmpActualConfiguration());
      modelBuilder.ApplyConfiguration(new SrvPrgEmpPlanConfiguration());
      modelBuilder.ApplyConfiguration(new SrvPrgPlanConfiguration());
      modelBuilder.ApplyConfiguration(new SrvPrgWaitConfiguration());

      //Services Quotations

      /*ADD NEW HERE - END*/

      modelBuilder.ConfigurePersistedGrantEntity();
    }
  }
}