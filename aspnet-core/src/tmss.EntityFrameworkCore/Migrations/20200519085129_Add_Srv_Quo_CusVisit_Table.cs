﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_CusVisit_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoCusVisit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DlrNo = table.Column<string>(maxLength: 20, nullable: true),
                    VhcId = table.Column<long>(nullable: true),
                    RegisterNo = table.Column<string>(maxLength: 20, nullable: true),
                    Pds = table.Column<string>(maxLength: 1, nullable: true),
                    CusId = table.Column<long>(nullable: true),
                    CusType = table.Column<long>(nullable: true),
                    Km = table.Column<double>(nullable: true),
                    CallFir = table.Column<string>(maxLength: 1, nullable: true),
                    LetterFir = table.Column<string>(maxLength: 1, nullable: true),
                    OtherFir = table.Column<string>(maxLength: 1, nullable: true),
                    CallTime = table.Column<string>(maxLength: 20, nullable: true),
                    ReqDesc = table.Column<string>(maxLength: 1000, nullable: true),
                    Notes = table.Column<string>(maxLength: 500, nullable: true),
                    WorkType = table.Column<string>(maxLength: 1, nullable: true),
                    CusState = table.Column<double>(nullable: true),
                    Gid = table.Column<double>(nullable: true),
                    OldCusVsId = table.Column<double>(nullable: true),
                    DlrId = table.Column<long>(nullable: false),
                    CloseRoDate = table.Column<DateTime>(nullable: true),
                    MeetCus = table.Column<DateTime>(nullable: true),
                    FirstMeetCus = table.Column<DateTime>(nullable: true),
                    CusDid = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoCusVisit", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoCusVisit");
        }
    }
}
