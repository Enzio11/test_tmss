﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Mst_Srv_Desk_Advisor_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstSrvDeskAdvisor",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DeskName = table.Column<string>(maxLength: 50, nullable: true),
                    AdvisorId = table.Column<long>(nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    BusyToDate = table.Column<DateTime>(nullable: true),
                    UpdateStatusDate = table.Column<DateTime>(nullable: true),
                    Ordering = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSrvDeskAdvisor", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MstSrvDeskAdvisor");
        }
    }
}
