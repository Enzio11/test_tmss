﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Drop_FK_Employee_Division : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MstSrvEmployee_MstSrvDivision_DivId",
                table: "MstSrvEmployee");

            migrationBuilder.DropIndex(
                name: "IX_MstSrvEmployee_DivId",
                table: "MstSrvEmployee");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_MstSrvEmployee_DivId",
                table: "MstSrvEmployee",
                column: "DivId");

            migrationBuilder.AddForeignKey(
                name: "FK_MstSrvEmployee_MstSrvDivision_DivId",
                table: "MstSrvEmployee",
                column: "DivId",
                principalTable: "MstSrvDivision",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
