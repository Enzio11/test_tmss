﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Mst_Srv_Employee_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstSrvEmployee",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EmpCode = table.Column<string>(maxLength: 20, nullable: false),
                    EmpName = table.Column<string>(maxLength: 50, nullable: false),
                    Tel = table.Column<string>(maxLength: 20, nullable: true),
                    Sex = table.Column<long>(nullable: true),
                    EmpAddress = table.Column<string>(maxLength: 100, nullable: true),
                    TypeId = table.Column<long>(nullable: true),
                    TitleId = table.Column<long>(nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    EmpColor = table.Column<string>(maxLength: 30, nullable: true),
                    Birthday = table.Column<DateTime>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EmpImg = table.Column<byte[]>(maxLength: 1, nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: false),
                    IsWork = table.Column<long>(nullable: true),
                    TypeJob = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DivId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSrvEmployee", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MstSrvDeskAdvisor_AdvisorId",
                table: "MstSrvDeskAdvisor",
                column: "AdvisorId",
                unique: true,
                filter: "[AdvisorId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MstSrvEmployee_EmpName_EmpCode",
                table: "MstSrvEmployee",
                columns: new[] { "EmpName", "EmpCode" });

            migrationBuilder.AddForeignKey(
                name: "FK_MstSrvDeskAdvisor_MstSrvEmployee_AdvisorId",
                table: "MstSrvDeskAdvisor",
                column: "AdvisorId",
                principalTable: "MstSrvEmployee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MstSrvDeskAdvisor_MstSrvEmployee_AdvisorId",
                table: "MstSrvDeskAdvisor");

            migrationBuilder.DropTable(
                name: "MstSrvEmployee");

            migrationBuilder.DropIndex(
                name: "IX_MstSrvDeskAdvisor_AdvisorId",
                table: "MstSrvDeskAdvisor");
        }
    }
}
