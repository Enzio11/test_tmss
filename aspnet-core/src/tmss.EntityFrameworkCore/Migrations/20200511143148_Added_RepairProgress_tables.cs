﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Added_RepairProgress_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MST_SRV_BP_GROUP",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    GROUP_NAME = table.Column<string>(maxLength: 32, nullable: false),
                    GROUP_DESC = table.Column<string>(maxLength: 255, nullable: true),
                    ORDERING = table.Column<int>(nullable: false),
                    IS_ACTIVE = table.Column<bool>(nullable: false, defaultValue: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_BP_GROUP", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MST_SRV_BP_PROCESS",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PROCESS_NAME = table.Column<string>(maxLength: 32, nullable: false),
                    PROCESS_DESC = table.Column<string>(maxLength: 255, nullable: true),
                    COLOR_CODE = table.Column<string>(maxLength: 32, nullable: true),
                    ORDERING = table.Column<int>(nullable: false),
                    IS_ACTIVE = table.Column<bool>(nullable: false, defaultValue: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_BP_PROCESS", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MST_SRV_WORKSHOP_TYPE",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WS_TYPE_CODE = table.Column<string>(maxLength: 32, nullable: false),
                    WS_TYPE_NAME = table.Column<string>(maxLength: 255, nullable: true),
                    WS_TYPE_DESC = table.Column<string>(maxLength: 255, nullable: true),
                    COLOR_CODE = table.Column<string>(maxLength: 32, nullable: true),
                    IS_ACTIVE = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_WORKSHOP_TYPE", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SRV_PRG_WAIT",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    APP_ID = table.Column<int>(nullable: true),
                    CUS_VS_ID = table.Column<int>(nullable: true),
                    RO_ID = table.Column<int>(nullable: true),
                    VEH_ID = table.Column<int>(nullable: false),
                    CUS_ID = table.Column<int>(nullable: false),
                    RO_TYPE = table.Column<byte>(nullable: false),
                    QC_LEVEL = table.Column<byte>(nullable: false),
                    IS_CUS_WAIT = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_CAR_WASH = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_TAKE_PARTS = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_PRIORITY = table.Column<bool>(nullable: true, defaultValue: false),
                    STATE = table.Column<byte>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV_PRG_WAIT", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MST_SRV_BP_GROUP_EMP",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    BP_GROUP_ID = table.Column<int>(nullable: true),
                    EMP_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_BP_GROUP_EMP", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MST_SRV_BP_GROUP_EMP_MST_SRV_BP_GROUP_BP_GROUP_ID",
                        column: x => x.BP_GROUP_ID,
                        principalTable: "MST_SRV_BP_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MST_SRV_WORKSHOP",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WS_CODE = table.Column<string>(maxLength: 32, nullable: false),
                    WS_NAME = table.Column<string>(maxLength: 32, nullable: true),
                    WS_DESC = table.Column<string>(maxLength: 255, nullable: true),
                    ORDERING = table.Column<int>(nullable: false),
                    IS_ACTIVE = table.Column<bool>(nullable: false, defaultValue: true),
                    WS_TYPE_ID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_WORKSHOP", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MST_SRV_WORKSHOP_MST_SRV_WORKSHOP_TYPE_WS_TYPE_ID",
                        column: x => x.WS_TYPE_ID,
                        principalTable: "MST_SRV_WORKSHOP_TYPE",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MST_SRV_WORKSHOP_EMP",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WS_ID = table.Column<int>(nullable: true),
                    EMP_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MST_SRV_WORKSHOP_EMP", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MST_SRV_WORKSHOP_EMP_MST_SRV_WORKSHOP_WS_ID",
                        column: x => x.WS_ID,
                        principalTable: "MST_SRV_WORKSHOP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SRV_PRG_ACTUAL",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    APP_ID = table.Column<int>(nullable: true),
                    CUS_VS_ID = table.Column<int>(nullable: true),
                    RO_ID = table.Column<int>(nullable: true),
                    VEH_ID = table.Column<int>(nullable: false),
                    CUS_ID = table.Column<int>(nullable: false),
                    PENDING_ID = table.Column<int>(nullable: false),
                    WS_ID = table.Column<int>(nullable: true),
                    PLAN_ID = table.Column<long>(nullable: false),
                    BP_PROCESS_ID = table.Column<int>(nullable: true),
                    BP_GROUP_ID = table.Column<int>(nullable: true),
                    PENDING_NOTE = table.Column<string>(maxLength: 255, nullable: true),
                    RO_TYPE = table.Column<byte>(nullable: false),
                    QC_LEVEL = table.Column<byte>(nullable: false),
                    IS_CUS_WAIT = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_CAR_WASH = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_TAKE_PARTS = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_PRIORITY = table.Column<bool>(nullable: true, defaultValue: false),
                    FROM_TIME = table.Column<DateTime>(nullable: true),
                    TO_TIME = table.Column<DateTime>(nullable: true),
                    CALC_TIME = table.Column<int>(nullable: true),
                    STATE = table.Column<byte>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV_PRG_ACTUAL", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_ACTUAL_MST_SRV_BP_GROUP_BP_GROUP_ID",
                        column: x => x.BP_GROUP_ID,
                        principalTable: "MST_SRV_BP_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_ACTUAL_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                        column: x => x.BP_PROCESS_ID,
                        principalTable: "MST_SRV_BP_PROCESS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_ACTUAL_MST_SRV_WORKSHOP_WS_ID",
                        column: x => x.WS_ID,
                        principalTable: "MST_SRV_WORKSHOP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SRV_PRG_PLAN",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    APP_ID = table.Column<int>(nullable: true),
                    CUS_VS_ID = table.Column<int>(nullable: true),
                    RO_ID = table.Column<int>(nullable: true),
                    VEH_ID = table.Column<int>(nullable: false),
                    CUS_ID = table.Column<int>(nullable: false),
                    WS_ID = table.Column<int>(nullable: true),
                    BP_PROCESS_ID = table.Column<int>(nullable: true),
                    BP_GROUP_ID = table.Column<int>(nullable: true),
                    ACTUAL_ID = table.Column<long>(nullable: true),
                    RO_TYPE = table.Column<byte>(nullable: false),
                    QC_LEVEL = table.Column<byte>(nullable: false),
                    IS_CUS_WAIT = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_CAR_WASH = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_TAKE_PARTS = table.Column<bool>(nullable: true, defaultValue: false),
                    IS_PRIORITY = table.Column<bool>(nullable: true, defaultValue: false),
                    FROM_TIME = table.Column<DateTime>(nullable: true),
                    TO_TIME = table.Column<DateTime>(nullable: true),
                    CALC_TIME = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV_PRG_PLAN", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_PLAN_SRV_PRG_ACTUAL_ACTUAL_ID",
                        column: x => x.ACTUAL_ID,
                        principalTable: "SRV_PRG_ACTUAL",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_PLAN_MST_SRV_BP_GROUP_BP_GROUP_ID",
                        column: x => x.BP_GROUP_ID,
                        principalTable: "MST_SRV_BP_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_PLAN_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                        column: x => x.BP_PROCESS_ID,
                        principalTable: "MST_SRV_BP_PROCESS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_PLAN_MST_SRV_WORKSHOP_WS_ID",
                        column: x => x.WS_ID,
                        principalTable: "MST_SRV_WORKSHOP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SRV_PRG_EMP_ACTUAL",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EMP_ID = table.Column<int>(nullable: false),
                    PLAN_ID = table.Column<long>(nullable: true),
                    ACTUAL_ID = table.Column<long>(nullable: true),
                    EMP_PLAN_ID = table.Column<long>(nullable: false),
                    WS_ID = table.Column<int>(nullable: true),
                    BP_PROCESS_ID = table.Column<int>(nullable: true),
                    BP_GROUP_ID = table.Column<int>(nullable: true),
                    RO_TYPE = table.Column<byte>(nullable: false),
                    FROM_TIME = table.Column<DateTime>(nullable: true),
                    TO_TIME = table.Column<DateTime>(nullable: true),
                    CALC_TIME = table.Column<int>(nullable: true),
                    STATE = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV_PRG_EMP_ACTUAL", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_ACTUAL_ACTUAL_ID",
                        column: x => x.ACTUAL_ID,
                        principalTable: "SRV_PRG_ACTUAL",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_BP_GROUP_BP_GROUP_ID",
                        column: x => x.BP_GROUP_ID,
                        principalTable: "MST_SRV_BP_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                        column: x => x.BP_PROCESS_ID,
                        principalTable: "MST_SRV_BP_PROCESS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_PLAN_PLAN_ID",
                        column: x => x.PLAN_ID,
                        principalTable: "SRV_PRG_PLAN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_WORKSHOP_WS_ID",
                        column: x => x.WS_ID,
                        principalTable: "MST_SRV_WORKSHOP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SRV_PRG_EMP_PLAN",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EMP_ID = table.Column<int>(nullable: false),
                    PLAN_ID = table.Column<long>(nullable: true),
                    ACTUAL_ID = table.Column<long>(nullable: true),
                    EMP_ACTUAL_ID = table.Column<long>(nullable: true),
                    WS_ID = table.Column<int>(nullable: true),
                    BP_PROCESS_ID = table.Column<int>(nullable: true),
                    BP_GROUP_ID = table.Column<int>(nullable: true),
                    RO_TYPE = table.Column<byte>(nullable: false),
                    FROM_TIME = table.Column<DateTime>(nullable: true),
                    TO_TIME = table.Column<DateTime>(nullable: true),
                    CALC_TIME = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV_PRG_EMP_PLAN", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_SRV_PRG_ACTUAL_ACTUAL_ID",
                        column: x => x.ACTUAL_ID,
                        principalTable: "SRV_PRG_ACTUAL",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_BP_GROUP_BP_GROUP_ID",
                        column: x => x.BP_GROUP_ID,
                        principalTable: "MST_SRV_BP_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                        column: x => x.BP_PROCESS_ID,
                        principalTable: "MST_SRV_BP_PROCESS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_SRV_PRG_EMP_ACTUAL_EMP_ACTUAL_ID",
                        column: x => x.EMP_ACTUAL_ID,
                        principalTable: "SRV_PRG_EMP_ACTUAL",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_SRV_PRG_PLAN_PLAN_ID",
                        column: x => x.PLAN_ID,
                        principalTable: "SRV_PRG_PLAN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_WORKSHOP_WS_ID",
                        column: x => x.WS_ID,
                        principalTable: "MST_SRV_WORKSHOP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MST_SRV_BP_GROUP_EMP_BP_GROUP_ID",
                table: "MST_SRV_BP_GROUP_EMP",
                column: "BP_GROUP_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MST_SRV_WORKSHOP_WS_TYPE_ID",
                table: "MST_SRV_WORKSHOP",
                column: "WS_TYPE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MST_SRV_WORKSHOP_EMP_WS_ID",
                table: "MST_SRV_WORKSHOP_EMP",
                column: "WS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_ACTUAL_BP_GROUP_ID",
                table: "SRV_PRG_ACTUAL",
                column: "BP_GROUP_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_ACTUAL_BP_PROCESS_ID",
                table: "SRV_PRG_ACTUAL",
                column: "BP_PROCESS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_ACTUAL_PLAN_ID",
                table: "SRV_PRG_ACTUAL",
                column: "PLAN_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_ACTUAL_WS_ID",
                table: "SRV_PRG_ACTUAL",
                column: "WS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_ACTUAL_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "ACTUAL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_BP_GROUP_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "BP_GROUP_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_BP_PROCESS_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "BP_PROCESS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_EMP_PLAN_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "EMP_PLAN_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_PLAN_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "PLAN_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_ACTUAL_WS_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "WS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_ACTUAL_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "ACTUAL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_BP_GROUP_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "BP_GROUP_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_BP_PROCESS_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "BP_PROCESS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_EMP_ACTUAL_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "EMP_ACTUAL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_PLAN_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "PLAN_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_EMP_PLAN_WS_ID",
                table: "SRV_PRG_EMP_PLAN",
                column: "WS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_PLAN_ACTUAL_ID",
                table: "SRV_PRG_PLAN",
                column: "ACTUAL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_PLAN_BP_GROUP_ID",
                table: "SRV_PRG_PLAN",
                column: "BP_GROUP_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_PLAN_BP_PROCESS_ID",
                table: "SRV_PRG_PLAN",
                column: "BP_PROCESS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_PRG_PLAN_WS_ID",
                table: "SRV_PRG_PLAN",
                column: "WS_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_SRV_PRG_ACTUAL_SRV_PRG_PLAN_PLAN_ID",
                table: "SRV_PRG_ACTUAL",
                column: "PLAN_ID",
                principalTable: "SRV_PRG_PLAN",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_EMP_PLAN_EMP_PLAN_ID",
                table: "SRV_PRG_EMP_ACTUAL",
                column: "EMP_PLAN_ID",
                principalTable: "SRV_PRG_EMP_PLAN",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_ACTUAL_MST_SRV_BP_GROUP_BP_GROUP_ID",
                table: "SRV_PRG_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_BP_GROUP_BP_GROUP_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_BP_GROUP_BP_GROUP_ID",
                table: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_PLAN_MST_SRV_BP_GROUP_BP_GROUP_ID",
                table: "SRV_PRG_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_MST_SRV_WORKSHOP_MST_SRV_WORKSHOP_TYPE_WS_TYPE_ID",
                table: "MST_SRV_WORKSHOP");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_ACTUAL_MST_SRV_WORKSHOP_WS_ID",
                table: "SRV_PRG_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_WORKSHOP_WS_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_WORKSHOP_WS_ID",
                table: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_PLAN_MST_SRV_WORKSHOP_WS_ID",
                table: "SRV_PRG_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_ACTUAL_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                table: "SRV_PRG_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_PLAN_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                table: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_PLAN_MST_SRV_BP_PROCESS_BP_PROCESS_ID",
                table: "SRV_PRG_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_ACTUAL_SRV_PRG_PLAN_PLAN_ID",
                table: "SRV_PRG_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_PLAN_PLAN_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_PLAN_SRV_PRG_PLAN_PLAN_ID",
                table: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_ACTUAL_ACTUAL_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_PLAN_SRV_PRG_ACTUAL_ACTUAL_ID",
                table: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropForeignKey(
                name: "FK_SRV_PRG_EMP_ACTUAL_SRV_PRG_EMP_PLAN_EMP_PLAN_ID",
                table: "SRV_PRG_EMP_ACTUAL");

            migrationBuilder.DropTable(
                name: "MST_SRV_BP_GROUP_EMP");

            migrationBuilder.DropTable(
                name: "MST_SRV_WORKSHOP_EMP");

            migrationBuilder.DropTable(
                name: "SRV_PRG_WAIT");

            migrationBuilder.DropTable(
                name: "MST_SRV_BP_GROUP");

            migrationBuilder.DropTable(
                name: "MST_SRV_WORKSHOP_TYPE");

            migrationBuilder.DropTable(
                name: "MST_SRV_WORKSHOP");

            migrationBuilder.DropTable(
                name: "MST_SRV_BP_PROCESS");

            migrationBuilder.DropTable(
                name: "SRV_PRG_PLAN");

            migrationBuilder.DropTable(
                name: "SRV_PRG_ACTUAL");

            migrationBuilder.DropTable(
                name: "SRV_PRG_EMP_PLAN");

            migrationBuilder.DropTable(
                name: "SRV_PRG_EMP_ACTUAL");
        }
    }
}
