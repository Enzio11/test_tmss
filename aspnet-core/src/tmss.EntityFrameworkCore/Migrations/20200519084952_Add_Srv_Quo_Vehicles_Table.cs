﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_Vehicles_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoVehicles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    RegisterNo = table.Column<string>(maxLength: 20, nullable: true),
                    CmId = table.Column<long>(nullable: true),
                    EngineTypeId = table.Column<long>(nullable: true),
                    EngineNo = table.Column<string>(maxLength: 20, nullable: true),
                    FrameNo = table.Column<string>(maxLength: 20, nullable: true),
                    VinNo = table.Column<string>(maxLength: 20, nullable: true),
                    VcId = table.Column<long>(nullable: true),
                    Ckd = table.Column<string>(maxLength: 1, nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    NtCode = table.Column<string>(maxLength: 50, nullable: true),
                    SalesCflId = table.Column<long>(nullable: true),
                    FullModel = table.Column<string>(maxLength: 50, nullable: true),
                    Pi = table.Column<string>(maxLength: 1, nullable: true),
                    Hybrid = table.Column<string>(maxLength: 1, nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    Ordering = table.Column<long>(nullable: true),
                    VhcType = table.Column<long>(nullable: true),
                    RefId = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoVehicles", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoVehicles");
        }
    }
}
