﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class ADD_tb_MstGenDealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstGenDealers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(nullable: false),
                    AccountNo = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    BankAddress = table.Column<string>(nullable: true),
                    VnName = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Abbreviation = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsSpecial = table.Column<string>(nullable: true),
                    Ordering = table.Column<int>(nullable: false),
                    ProvinceId = table.Column<string>(nullable: true),
                    DealerId = table.Column<string>(nullable: true),
                    DealerTypeId = table.Column<string>(nullable: true),
                    TaxCode = table.Column<string>(nullable: true),
                    IsSumDealer = table.Column<string>(nullable: true),
                    DealerGroupId = table.Column<decimal>(nullable: false),
                    BiServer = table.Column<string>(nullable: true),
                    TfsAmount = table.Column<string>(nullable: true),
                    PartLeadtime = table.Column<string>(nullable: true),
                    IpAddress = table.Column<string>(nullable: true),
                    Islexus = table.Column<string>(nullable: true),
                    IsSellLexusPart = table.Column<string>(nullable: true),
                    IsDlrSales = table.Column<string>(nullable: true),
                    RecievingAddress = table.Column<string>(nullable: true),
                    DlrFooter = table.Column<string>(nullable: true),
                    IsPrint = table.Column<string>(nullable: true),
                    PasswordSearchVin = table.Column<string>(nullable: true),
                    PortRegion = table.Column<decimal>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstGenDealers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MstGenDealers");
        }
    }
}
