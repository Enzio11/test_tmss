﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_tb_MstGenColors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastModificationtTime",
                table: "MstGenProvinces");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "MstGenProvinces");

            migrationBuilder.DropColumn(
                name: "SubRegionId",
                table: "MstGenProvinces");

            migrationBuilder.CreateTable(
                name: "MstSleColors",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    VnName = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Ordering = table.Column<decimal>(nullable: true),
                    OrderingRpt = table.Column<decimal>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    LastModificationtTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletionTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSleColors", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MstSleColors");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationtTime",
                table: "MstGenProvinces",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RegionId",
                table: "MstGenProvinces",
                type: "numeric(8, 0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<long>(
                name: "SubRegionId",
                table: "MstGenProvinces",
                type: "bigint",
                nullable: true);
        }
    }
}
