﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class add_update_table_mstSleArea : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MstSleAreas",
                table: "MstSleAreas");

            migrationBuilder.RenameTable(
                name: "MstSleAreas",
                newName: "MstSleArea");

            migrationBuilder.AlterColumn<long>(
                name: "YardId",
                table: "MstSleArea",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MstSleArea",
                table: "MstSleArea",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MstSleArea",
                table: "MstSleArea");

            migrationBuilder.RenameTable(
                name: "MstSleArea",
                newName: "MstSleAreas");

            migrationBuilder.AlterColumn<decimal>(
                name: "YardId",
                table: "MstSleAreas",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AddPrimaryKey(
                name: "PK_MstSleAreas",
                table: "MstSleAreas",
                column: "Id");
        }
    }
}
