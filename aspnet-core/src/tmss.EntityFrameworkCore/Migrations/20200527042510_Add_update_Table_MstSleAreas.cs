﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_update_Table_MstSleAreas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastModificationtTime",
                table: "MstSleYards");

            migrationBuilder.DropColumn(
                name: "LastModificationtTime",
                table: "MstSleLookup");

            migrationBuilder.DropColumn(
                name: "LastModificationtTime",
                table: "MstSleColors");

            migrationBuilder.DropColumn(
                name: "LastModificationtTime",
                table: "MstSleAreas");

            migrationBuilder.AddColumn<decimal>(
                name: "YardId",
                table: "MstSleAreas",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "YardId",
                table: "MstSleAreas");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationtTime",
                table: "MstSleYards",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationtTime",
                table: "MstSleLookup",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationtTime",
                table: "MstSleColors",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationtTime",
                table: "MstSleAreas",
                type: "datetime2",
                nullable: true);
        }
    }
}
