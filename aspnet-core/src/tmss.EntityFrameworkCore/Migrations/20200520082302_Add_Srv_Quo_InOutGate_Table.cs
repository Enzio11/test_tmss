﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_InOutGate_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoInOutGate",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VhcId = table.Column<long>(nullable: true),
                    RegisterNo = table.Column<string>(maxLength: 20, nullable: true),
                    OutDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    InDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PicId = table.Column<long>(nullable: true),
                    PicName = table.Column<string>(maxLength: 50, nullable: true),
                    Reason = table.Column<string>(maxLength: 500, nullable: true),
                    PrintCount = table.Column<long>(nullable: true),
                    Stt = table.Column<string>(maxLength: 10, nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoInOutGate", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoInOutGate");
        }
    }
}
