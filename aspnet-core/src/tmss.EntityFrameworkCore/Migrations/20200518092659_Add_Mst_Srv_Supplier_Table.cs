﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Mst_Srv_Supplier_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstSrvSupplier",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SupplierCode = table.Column<string>(maxLength: 20, nullable: true),
                    SupplierName = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 100, nullable: true),
                    AccNo = table.Column<string>(maxLength: 30, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Taxcode = table.Column<string>(maxLength: 50, nullable: true),
                    Tel = table.Column<string>(maxLength: 30, nullable: true),
                    Pic = table.Column<string>(maxLength: 100, nullable: true),
                    Fax = table.Column<string>(maxLength: 30, nullable: true),
                    BankId = table.Column<long>(nullable: true),
                    PicTel = table.Column<string>(maxLength: 30, nullable: true),
                    CountryId = table.Column<long>(nullable: true),
                    PicMobi = table.Column<string>(maxLength: 30, nullable: true),
                    LeadTime = table.Column<double>(nullable: true),
                    Ordering = table.Column<long>(nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSrvSupplier", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MstSrvSupplier");
        }
    }
}
