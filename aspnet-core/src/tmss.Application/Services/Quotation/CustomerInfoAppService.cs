﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using tmss.Services.Quotation.CustomerInfo;
using tmss.Services.Quotation.CustomerInfo.Dto;

namespace tmss.Services.Quotation
{
    public class CustomerInfoAppService : tmssAppServiceBase, ICustomerInfoAppService
    {
        private readonly IRepository<SrvQuoCustomer, long> _srvQuoCustomerRepo;
        private readonly IRepository<SrvQuoCusVisit, long> _srvQuoCusVisitRepo;
        private readonly IRepository<SrvQuoVehicle, long> _srvQuoVehicleRepo;
        private readonly IRepository<SrvQuoRepairOrder, long> _srvQuoRepairOrderRepo;
        private readonly IRepository<SrvQuoCusVehicle, long> _srvQuoCusVehicleRepo;
        private readonly IRepository<SrvQuoAppointment, long> _srvQuoAppointmentRepo;

        public CustomerInfoAppService(IRepository<SrvQuoCustomer, long> srvQuoCustomerRepo, IRepository<SrvQuoCusVisit, long> srvQuoCusVisitRepo,
            IRepository<SrvQuoVehicle, long> srvQuoVehicleRepo, IRepository<SrvQuoRepairOrder, long> srvQuoRepairOrderRepo,
            IRepository<SrvQuoCusVehicle, long> srvQuoCusVehicleRepo, IRepository<SrvQuoAppointment, long> srvQuoAppointmentRepo)
        {
            _srvQuoCustomerRepo = srvQuoCustomerRepo;
            _srvQuoCusVisitRepo = srvQuoCusVisitRepo;
            _srvQuoVehicleRepo = srvQuoVehicleRepo;
            _srvQuoRepairOrderRepo = srvQuoRepairOrderRepo;
            _srvQuoCusVehicleRepo = srvQuoCusVehicleRepo;
            _srvQuoAppointmentRepo = srvQuoAppointmentRepo;
        }

        public Task CreateOrEdit(CreateOrEditCustomerInfoDto input)
        {
            throw new NotImplementedException();
        }

        public Task Delete(EntityDto input)
        {
            throw new NotImplementedException();
        }

        public async Task<PagedResultDto<GetCustomerInfoForViewDto>> GetAll(GetAllCustomerInfoInput input)
        {
            var query = GetCustomersFilteredQuery(input);

            var totalCount = await query.CountAsync();

            return new PagedResultDto<GetCustomerInfoForViewDto>(
                totalCount,
                await query.ToListAsync()
                );
        }

        public async Task<List<GetCustomerInfoForViewDto>> GetAllList()
        {
            var query = GetCustomersFilteredQuery(null);

            var totalCount = await query.CountAsync();

            return await query.ToListAsync();
        }

        public Task<GetCustomerInfoForEditOutput> GetCustomerInfoForEdit(EntityDto input)
        {
            throw new NotImplementedException();
        }

        public Task<GetCustomerInfoForViewDto> GetCustomerInfoForView(int id)
        {
            throw new NotImplementedException();
        }

        private IQueryable<GetCustomerInfoForViewDto> GetCustomersFilteredQuery(GetAllCustomerInfoInput? input)
        {
            var customers = _srvQuoCustomerRepo.GetAll();
            var customerVisits = _srvQuoCusVisitRepo.GetAll();
            var vehicles = _srvQuoVehicleRepo.GetAll();
            var repairOrders = _srvQuoRepairOrderRepo.GetAll();
            var customerVehicles = _srvQuoCusVehicleRepo.GetAll();
            var appoitments = _srvQuoAppointmentRepo.GetAll();

            //var query = from qccv in queryCustomerCusVehicle
            //            join qcra in queryCusVisitRoAppt on qccv.VehicleId equals qcra.VehicleId into custInfoJoined
            //            from qcra in custInfoJoined
            //            select (new GetCustomerInfoForViewDto
            //            {
            //                VehicleId = qccv.VehicleId,
            //                RegisterNo = qcra.RegisterNo,
            //                CustomerId = qccv.CustomerId,
            //                CarOwnername = qccv.CarOwnerName,
            //                CarOwnerMobil = qccv.CarOwnerMobile,
            //                CarOwnerAdd = qccv.CarOwnerAdd,
            //                VinNo = qcra.VinNo,
            //                CusVsStatus = 0, //todo
            //                CusVsId = qcra.CusVisitId,
            //                RoId = qcra.RoId,
            //                AppId = qcra.AppId,
            //                CusNo = qccv.CusNo,
            //                CarOwnerTel = qccv.CarOwnerTel,
            //                CarOwnerFax = qccv.CarOwnerFax,
            //                OrgName = qccv.OrgName,
            //                TaxCode = qccv.TaxCode,
            //                IsCustomerSaler = qccv.CusId == null ? "true" : "false"
            //            });

            var query = from cus in _srvQuoCustomerRepo.GetAll()
                        join cusV in _srvQuoCusVehicleRepo.GetAll() on cus.Id equals cusV.CusId into cVJoined
                        from cusV in cVJoined.DefaultIfEmpty()
                        where cus.CarOwnerName == "Đỗ Thanh Tâm"
                        join v in _srvQuoVehicleRepo.GetAll() on cusV.VehiclesId equals v.Id into vCvJoined
                        from v in vCvJoined.DefaultIfEmpty()
                            //from v in SrvQuoVehicles
                        join c in (from t in _srvQuoCusVisitRepo.GetAll()
                                   where t.Id == (from t2 in _srvQuoCusVisitRepo.GetAll()
                                                  where t2.VhcId == t.VhcId
                                                  select t2.Id).Max()
                                                   && (t.CusState == null || t.CusState != 1)
                                   select t) on v.Id equals c.VhcId into vcJoined
                        from c in vcJoined.DefaultIfEmpty()
                        join ro in (from x in _srvQuoRepairOrderRepo.GetAll()
                                    where x.Id == (from x2 in _srvQuoRepairOrderRepo.GetAll()
                                                   where x2.CusVsId == x.CusVsId
                                                   select x2.Id).Max()
                                                    && "7".Contains(x.RoState)
                                    select x) on c.Id equals ro.CusVsId into cRoJoined
                        from ro in cRoJoined.DefaultIfEmpty()
                        join appt in (from y in _srvQuoAppointmentRepo.GetAll()
                                      where y.Id == (from y2 in _srvQuoAppointmentRepo.GetAll()
                                                     where y2.CusVsId == y.CusVsId
                                                     select y2.Id).Max()
                                                            && "HSDCW".Contains(y.AppStatus)
                                                            && "N".Contains(y.IsDone)
                                      select y) on c.Id equals appt.CusVsId into cApptJoined
                        from appt in cApptJoined.DefaultIfEmpty()
                            //where v.Id == 1000005032
                        orderby v.Id descending, c.CusId descending, c.Id descending
                        select new GetCustomerInfoForViewDto
                        {
                            VehicleId = v.Id,
                            RegisterNo = c.RegisterNo,
                            CustomerId = cus.Id,
                            CarOwnername = cus.CarOwnerName,
                            CarOwnerMobil = cus.CarOwnerMobile,
                            CarOwnerAdd = cus.CarOwnerAdd,
                            VinNo = v.VinNo,
                            CusVsStatus = 0, //TODO
                            CusVsId = c.Id,
                            RoId = ro.Id,
                            AppId = appt.Id,
                            CusNo = cus.CusNo,
                            CarOwnerTel = cus.CarOwnerTel,
                            CarOwnerFax = cus.CarOwnerFax,
                            OrgName = cus.OrgName,
                            TaxCode = cus.TaxCode,
                            IsCustomerSaler = cusV.CusId == null ? "true" : "false"

                            //cus.CarOwnerName,
                            //cus.CarOwnerMobile,
                            //VehicleId = v.Id,
                            //v.VinNo,
                            //c.RegisterNo,
                            //CustomerId = cus.Id,
                            //CustVisitId = c.Id,
                            //CustVisitState = c.CusState,
                            //RoId = ro.Id,
                            //RoState = ro.RoState,
                            //ApptId = appt.Id,
                            //ApptStatus = appt.AppStatus,
                            //ApptDone = appt.IsDone
                        };
            return query;
        }
    }
}
