﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using tmss.Application.Shared.Services.Master.OrderMethod;
using tmss.Core.Services.Master.OrderMethod;
using Abp.Linq.Extensions;
using System.Linq;
using System.Threading.Tasks;
using tmss.Dto;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using tmss.Authorization;

namespace tmss.Application.Services.Master.OrderMethodAppService
{
    [AbpAuthorize(AppPermissions.Pages_OrderMethods)]
    public class OrderMethodAppService : tmssAppServiceBase, IOrderMethodAppService
    {
        private readonly IRepository<OrderMethod> _orderMethodRepository;

        public OrderMethodAppService(IRepository<OrderMethod> orderMethodRepository)
        {
            _orderMethodRepository = orderMethodRepository;
        }

        // Check CREATE or UPDATE action
        public async Task CreateOrEdit(CreateOrEditOrderMethodDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            } else
            {
                await Update(input);
            }
        }

        // DELETE
        public async Task Delete(EntityDto input)
        {
            await _orderMethodRepository.DeleteAsync(input.Id);
        }

        // GET ALL
        public async Task<PagedResultDto<GetOrderMethodForViewDto>> GetAll(GetAllOrderMethodInput input)
        {
            var filteredOrderMethods = _orderMethodRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter));

            var pageAndFilteredOrderMethods = filteredOrderMethods.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var orderMethods = from o in pageAndFilteredOrderMethods
                               select new GetOrderMethodForViewDto()
                               {
                                   OrderMethod = new OrderMethodDto
                                   {
                                       Code = o.Code,
                                       Name = o.Name,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredOrderMethods.CountAsync();

            return new PagedResultDto<GetOrderMethodForViewDto>(
                totalCount,
                await orderMethods.ToListAsync()
                );
        }

        // Get by ID for EDIT
        [AbpAuthorize(AppPermissions.Pages_OrderMethods_Edit)]
        public async Task<GetOrderMethodForEditOutput> GetOrderMethodForEdit(EntityDto input)
        {
            var orderMethod = await _orderMethodRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOrderMethodForEditOutput { OrderMethod = ObjectMapper.Map<CreateOrEditOrderMethodDto>(orderMethod) };

            return output;
        }

        // GET by ID for VIEW
        public async Task<GetOrderMethodForViewDto> GetOrderMethodForView(int id)
        {
            var orderMethod = await _orderMethodRepository.GetAsync(id);
            var output = new GetOrderMethodForViewDto { OrderMethod = ObjectMapper.Map<OrderMethodDto>(orderMethod) };

            return output;
        }

        // Get all for EXPORT EXCEL
        public Task<FileDto> GetOrderMethodToExcel(GetAllOrderMethodForExcelInput input)
        {
            throw new System.NotImplementedException();
        }

        // CREATE
        [AbpAuthorize(AppPermissions.Pages_OrderMethods_Create)]
        protected virtual async Task Create(CreateOrEditOrderMethodDto input)
        {
            var orderMethod = ObjectMapper.Map<OrderMethod>(input);
            await _orderMethodRepository.InsertAsync(orderMethod);
        }

        // UPDATE
        [AbpAuthorize(AppPermissions.Pages_OrderMethods_Edit)]
        protected virtual async Task Update(CreateOrEditOrderMethodDto input)
        {
            var orderMethod = await _orderMethodRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, orderMethod);
        }
    }
}
