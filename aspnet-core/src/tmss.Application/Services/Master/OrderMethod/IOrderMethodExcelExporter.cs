﻿using System.Collections.Generic;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using tmss.Dto;

namespace tmss.Services.Master.OrderMethod
{
    public interface IOrderMethodExcelExporter
    {
        FileDto ExportToFile(List<GetOrderMethodForViewDto> orderMethods);
    }
}
