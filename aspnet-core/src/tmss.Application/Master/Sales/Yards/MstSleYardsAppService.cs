﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstSleYards)]
	public class MstSleYardsAppService : tmssAppServiceBase, IMstSleYardsAppService
	{
		private readonly IRepository<MstSleYards, long> _mstSleYardsRepository;


		public MstSleYardsAppService(IRepository<MstSleYards, long> mstSleYardsRepository)
		{
			_mstSleYardsRepository = mstSleYardsRepository;

		}

		public async Task<PagedResultDto<GetMstSleYardsForViewDto>> GetAll(GetAllMstSleYardsInput input)
		{

			var filteredMstSleYards = _mstSleYardsRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Status.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

			var pagedAndFilteredMstSleYards = filteredMstSleYards
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstSleYards = from o in pagedAndFilteredMstSleYards
							  select new GetMstSleYardsForViewDto()
							  {
								  MstSleYards = new MstSleYardsDto
								  {
									  Code = o.Code,
									  Name = o.Name,
									  Address = o.Address,
									  Status = o.Status,
									  Ordering = o.Ordering,
									  Description = o.Description,
									  CreationTime = o.CreationTime,
									  IsDeleted = o.IsDeleted,
									  DeletionTime = o.DeletionTime,
									  Id = o.Id
								  }
							  };

			var totalCount = await filteredMstSleYards.CountAsync();

			return new PagedResultDto<GetMstSleYardsForViewDto>(
				totalCount,
				await mstSleYards.ToListAsync()
			);
		}
		public async Task<List<MstSleYardsDto>> GetAllYard()
		{
			var filteredMstSleYards = _mstSleYardsRepository.GetAll();
			var mstSleYards = from o in filteredMstSleYards
							  select new MstSleYardsDto()
							  {
									  Code = o.Code,
									  Name = o.Name,
									  Address = o.Address,
									  Status = o.Status,
									  Ordering = o.Ordering,
									  Description = o.Description,
									  CreationTime = o.CreationTime,
									  IsDeleted = o.IsDeleted,
									  DeletionTime = o.DeletionTime,
									  Id = o.Id
							  };
			return await mstSleYards.ToListAsync();
		}

		public async Task<GetMstSleYardsForViewDto> GetMstSleYardsForView(long id)
		{
			var mstSleYards = await _mstSleYardsRepository.GetAsync(id);

			var output = new GetMstSleYardsForViewDto { MstSleYards = ObjectMapper.Map<MstSleYardsDto>(mstSleYards) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleYards_Edit)]
		public async Task<GetMstSleYardsForEditOutput> GetMstSleYardsForEdit(EntityDto<long> input)
		{
			var mstSleYards = await _mstSleYardsRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstSleYardsForEditOutput { MstSleYards = ObjectMapper.Map<CreateOrEditMstSleYardsDto>(mstSleYards) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstSleYardsDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleYards_Create)]
		protected virtual async Task Create(CreateOrEditMstSleYardsDto input)
		{
			var mstSleYards = ObjectMapper.Map<MstSleYards>(input);


			if (AbpSession.TenantId != null)
			{
				mstSleYards.TenantId = (int)AbpSession.TenantId;
			}


			await _mstSleYardsRepository.InsertAsync(mstSleYards);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleYards_Edit)]
		protected virtual async Task Update(CreateOrEditMstSleYardsDto input)
		{
			var mstSleYards = await _mstSleYardsRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstSleYards);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleYards_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstSleYardsRepository.DeleteAsync(input.Id);
		}
	}
}