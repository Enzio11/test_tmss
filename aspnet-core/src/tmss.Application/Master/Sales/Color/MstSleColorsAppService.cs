﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstSleColors)]
	public class MstSleColorsAppService : tmssAppServiceBase, IMstSleColorsAppService
	{
		private readonly IRepository<MstSleColors, long> _mstSleColorsRepository;


		public MstSleColorsAppService(IRepository<MstSleColors, long> mstSleColorsRepository)
		{
			_mstSleColorsRepository = mstSleColorsRepository;
		}

		public async Task<PagedResultDto<GetMstSleColorsForViewDto>> GetAll(GetAllMstSleColorsInput input)
		{

			var filteredMstSleColors = _mstSleColorsRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Status.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName == input.VnNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName == input.EnNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(input.MinOrderingRptFilter != null, e => e.OrderingRpt >= input.MinOrderingRptFilter)
						.WhereIf(input.MaxOrderingRptFilter != null, e => e.OrderingRpt <= input.MaxOrderingRptFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

			var pagedAndFilteredMstSleColors = filteredMstSleColors
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstSleColors = from o in pagedAndFilteredMstSleColors
							   select new GetMstSleColorsForViewDto()
							   {
								   MstSleColors = new MstSleColorsDto
								   {
									   Code = o.Code,
									   VnName = o.VnName,
									   EnName = o.EnName,
									   Description = o.Description,
									   Status = o.Status,
									   Ordering = o.Ordering,
									   OrderingRpt = o.OrderingRpt,
									   CreationTime = o.CreationTime,
									   IsDeleted = o.IsDeleted,
									   DeletionTime = o.DeletionTime,
									   Id = o.Id
								   }
							   };

			var totalCount = await filteredMstSleColors.CountAsync();

			return new PagedResultDto<GetMstSleColorsForViewDto>(
				totalCount,
				await mstSleColors.ToListAsync()
			);
		}

		public async Task<GetMstSleColorsForViewDto> GetMstSleColorsForView(long id)
		{
			var mstSleColors = await _mstSleColorsRepository.GetAsync(id);

			var output = new GetMstSleColorsForViewDto { MstSleColors = ObjectMapper.Map<MstSleColorsDto>(mstSleColors) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleColors_Edit)]
		public async Task<GetMstSleColorsForEditOutput> GetMstSleColorsForEdit(EntityDto<long> input)
		{
			var mstSleColors = await _mstSleColorsRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstSleColorsForEditOutput { MstSleColors = ObjectMapper.Map<CreateOrEditMstSleColorsDto>(mstSleColors) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstSleColorsDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleColors_Create)]
		protected virtual async Task Create(CreateOrEditMstSleColorsDto input)
		{
			var mstSleColors = ObjectMapper.Map<MstSleColors>(input);


			if (AbpSession.TenantId != null)
			{
				mstSleColors.TenantId = (int)AbpSession.TenantId;
			}


			await _mstSleColorsRepository.InsertAsync(mstSleColors);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleColors_Edit)]
		protected virtual async Task Update(CreateOrEditMstSleColorsDto input)
		{
			var mstSleColors = await _mstSleColorsRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstSleColors);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleColors_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstSleColorsRepository.DeleteAsync(input.Id);
		}

	}
}