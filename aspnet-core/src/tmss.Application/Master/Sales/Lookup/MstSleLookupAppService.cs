﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using tmss.Master.Sales.Lookup;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstSleLookup)]
	public class MstSleLookupAppService : tmssAppServiceBase, IMstSleLookupAppService
	{
		private readonly IRepository<MstSleLookup, long> _mstSleLookupRepository;


		public MstSleLookupAppService(IRepository<MstSleLookup, long> mstSleLookupRepository)
		{
			_mstSleLookupRepository = mstSleLookupRepository;

		}

		public async Task<PagedResultDto<GetMstSleLookupForViewDto>> GetAll(GetAllMstSleLookupInput input)
		{

			var filteredMstSleLookup = _mstSleLookupRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter) || e.Status.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Value.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ValueFilter), e => e.Value == input.ValueFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

			var pagedAndFilteredMstSleLookup = filteredMstSleLookup
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstSleLookup = from o in pagedAndFilteredMstSleLookup
							   select new GetMstSleLookupForViewDto()
							   {
								   MstSleLookup = new MstSleLookupDto
								   {
									   Code = o.Code,
									   Name = o.Name,
									   Status = o.Status,
									   Description = o.Description,
									   Ordering = o.Ordering,
									   Value = o.Value,
									   CreationTime = o.CreationTime,
									   IsDeleted = o.IsDeleted,
									   DeletionTime = o.DeletionTime,
									   Id = o.Id
								   }
							   };

			var totalCount = await filteredMstSleLookup.CountAsync();

			return new PagedResultDto<GetMstSleLookupForViewDto>(
				totalCount,
				await mstSleLookup.ToListAsync()
			);
		}

		public async Task<GetMstSleLookupForViewDto> GetMstSleLookupForView(long id)
		{
			var mstSleLookup = await _mstSleLookupRepository.GetAsync(id);

			var output = new GetMstSleLookupForViewDto { MstSleLookup = ObjectMapper.Map<MstSleLookupDto>(mstSleLookup) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleLookup_Edit)]
		public async Task<GetMstSleLookupForEditOutput> GetMstSleLookupForEdit(EntityDto<long> input)
		{
			var mstSleLookup = await _mstSleLookupRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstSleLookupForEditOutput { MstSleLookup = ObjectMapper.Map<CreateOrEditMstSleLookupDto>(mstSleLookup) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstSleLookupDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleLookup_Create)]
		protected virtual async Task Create(CreateOrEditMstSleLookupDto input)
		{
			var mstSleLookup = ObjectMapper.Map<MstSleLookup>(input);


			if (AbpSession.TenantId != null)
			{
				mstSleLookup.TenantId = (int)AbpSession.TenantId;
			}


			await _mstSleLookupRepository.InsertAsync(mstSleLookup);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleLookup_Edit)]
		protected virtual async Task Update(CreateOrEditMstSleLookupDto input)
		{
			var mstSleLookup = await _mstSleLookupRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstSleLookup);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleLookup_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstSleLookupRepository.DeleteAsync(input.Id);
		}
	}
}