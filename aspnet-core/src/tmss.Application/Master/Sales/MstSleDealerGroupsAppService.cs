﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using tmss.Master.Sales.DealerGroup.Dto;
using tmss.Master.Sales.DealerGroup;

namespace tmss.Master.Sale
{
	[AbpAuthorize(AppPermissions.Pages_MstSleDealerGroups)]
	public class MstSleDealerGroupsAppService : tmssAppServiceBase, IMstSleDealerGroupsAppService
	{
		private readonly IRepository<MstSleDealerGroups, long> _mstSleDealerGroupsRepository;


		public MstSleDealerGroupsAppService(IRepository<MstSleDealerGroups, long> mstSleDealerGroupsRepository)
		{
			_mstSleDealerGroupsRepository = mstSleDealerGroupsRepository;

		}

		public async Task<PagedResultDto<GetMstSleDealerGroupsForViewDto>> GetAll(GetAllMstSleDealerGroupsInput input)
		{

			var filteredMstSleDealerGroups = _mstSleDealerGroupsRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.GroupName.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Status.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.GroupNameFilter), e => e.GroupName == input.GroupNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter);

			var pagedAndFilteredMstSleDealerGroups = filteredMstSleDealerGroups
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstSleDealerGroups = from o in pagedAndFilteredMstSleDealerGroups
									 select new GetMstSleDealerGroupsForViewDto()
									 {
										 MstSleDealerGroups = new MstSleDealerGroupsDto
										 {
											 GroupName = o.GroupName,
											 Description = o.Description,
											 CreationTime = o.CreationTime,
											 IsDeleted = o.IsDeleted,
											 DeletionTime = o.DeletionTime,
											 Status = o.Status,
											 Ordering = o.Ordering,
											 Id = o.Id
										 }
									 };

			var totalCount = await filteredMstSleDealerGroups.CountAsync();

			return new PagedResultDto<GetMstSleDealerGroupsForViewDto>(
				totalCount,
				await mstSleDealerGroups.ToListAsync()
			);
		}

		public async Task<GetMstSleDealerGroupsForViewDto> GetMstSleDealerGroupsForView(long id)
		{
			var mstSleDealerGroups = await _mstSleDealerGroupsRepository.GetAsync(id);

			var output = new GetMstSleDealerGroupsForViewDto { MstSleDealerGroups = ObjectMapper.Map<MstSleDealerGroupsDto>(mstSleDealerGroups) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleDealerGroups_Edit)]
		public async Task<GetMstSleDealerGroupsForEditOutput> GetMstSleDealerGroupsForEdit(EntityDto<long> input)
		{
			var mstSleDealerGroups = await _mstSleDealerGroupsRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstSleDealerGroupsForEditOutput { MstSleDealerGroups = ObjectMapper.Map<CreateOrEditMstSleDealerGroupsDto>(mstSleDealerGroups) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstSleDealerGroupsDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		protected virtual async Task Create(CreateOrEditMstSleDealerGroupsDto input)
		{
			var mstSleDealerGroups = ObjectMapper.Map<MstSleDealerGroups>(input);


			await _mstSleDealerGroupsRepository.InsertAsync(mstSleDealerGroups);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleDealerGroups_Edit)]
		protected virtual async Task Update(CreateOrEditMstSleDealerGroupsDto input)
		{
			var mstSleDealerGroups = await _mstSleDealerGroupsRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstSleDealerGroups);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleDealerGroups_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstSleDealerGroupsRepository.DeleteAsync(input.Id);
		}
	}
}