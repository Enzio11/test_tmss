﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using tmss.Master.Sales.Provinces;
using tmss.Master.Sales.Provinces.Dto;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstGenProvinces)]
	public class MstGenProvincesAppService : tmssAppServiceBase, IMstGenProvincesAppService
	{
		private readonly IRepository<MstGenProvinces, long> _mstGenProvincesRepository;
		public MstGenProvincesAppService(IRepository<MstGenProvinces, long> mstGenProvincesRepository)
		{
			_mstGenProvincesRepository = mstGenProvincesRepository;

		}

		public async Task<PagedResultDto<GetMstGenProvincesForViewDto>> GetAll(GetAllMstGenProvincesInput input)
		{

			var filteredMstGenProvinces = _mstGenProvincesRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter) || e.Status.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(input.MinPopulationAmountFilter != null, e => e.PopulationAmount >= input.MinPopulationAmountFilter)
						.WhereIf(input.MaxPopulationAmountFilter != null, e => e.PopulationAmount <= input.MaxPopulationAmountFilter)
						.WhereIf(input.MinSquareAmountFilter != null, e => e.SquareAmount >= input.MinSquareAmountFilter)
						.WhereIf(input.MaxSquareAmountFilter != null, e => e.SquareAmount <= input.MaxSquareAmountFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

			var pagedAndFilteredMstGenProvinces = filteredMstGenProvinces
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstGenProvinces = from o in pagedAndFilteredMstGenProvinces
								  select new GetMstGenProvincesForViewDto()
								  {
									  MstGenProvinces = new MstGenProvinceDto
									  {
										  Code = o.Code,
										  Name = o.Name,
										  Ordering = o.Ordering,
										  Status = o.Status,
										  SubRegionId=o.SubRegionId,
										  PopulationAmount = o.PopulationAmount,
										  SquareAmount = o.SquareAmount,
										  CreationTime = o.CreationTime,
										  IsDeleted = o.IsDeleted,
										  DeletionTime = o.DeletionTime,
										  Id = o.Id
									  }
								  };

			var totalCount = await filteredMstGenProvinces.CountAsync();

			return new PagedResultDto<GetMstGenProvincesForViewDto>(
				totalCount,
				await mstGenProvinces.ToListAsync()
			);
		}

		public async Task<GetMstGenProvincesForViewDto> GetMstGenProvincesForView(long id)
		{
			var mstGenProvinces = await _mstGenProvincesRepository.GetAsync(id);

			var output = new GetMstGenProvincesForViewDto { MstGenProvinces = ObjectMapper.Map<MstGenProvinceDto>(mstGenProvinces) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenProvinces_Edit)]
		public async Task<GetMstGenProvincesForEditOutput> GetMstGenProvincesForEdit(EntityDto<long> input)
		{
			var mstGenProvinces = await _mstGenProvincesRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstGenProvincesForEditOutput { MstGenProvinces = ObjectMapper.Map<CreateOrEditMstGenProvinceDto>(mstGenProvinces) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstGenProvinceDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenProvinces_Create)]
		protected virtual async Task Create(CreateOrEditMstGenProvinceDto input)
		{
			var mstGenProvinces = ObjectMapper.Map<MstGenProvinces>(input);


			if (AbpSession.TenantId != null)
			{
				mstGenProvinces.TenantId = (int)AbpSession.TenantId;
			}


			await _mstGenProvincesRepository.InsertAsync(mstGenProvinces);
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenProvinces_Edit)]
		protected virtual async Task Update(CreateOrEditMstGenProvinceDto input)
		{
			var mstGenProvinces = await _mstGenProvincesRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstGenProvinces);
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenProvinces_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstGenProvincesRepository.DeleteAsync(input.Id);
		}
	}
}