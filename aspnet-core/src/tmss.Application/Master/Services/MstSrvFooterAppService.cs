﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Core.Master.Services;
using tmss.Master.Services.Footer;
using tmss.Master.Services.Footer.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Footer)]
    public class MstSrvFooterAppService : tmssAppServiceBase, IMstSrvFooterAppService
    {
        private readonly IRepository<MstSrvFooter, long> _fooRepo;
        private readonly IRepository<MstSrvFooterReportType, long> _fooReportRepo;
        private readonly IRepository<MstSrvFooterTmv, long> _tmvFooRepo;

        public MstSrvFooterAppService(IRepository<MstSrvFooter, long> fooRepo,
            IRepository<MstSrvFooterReportType, long> fooReportRepo,
            IRepository<MstSrvFooterTmv, long> tmvFooRepo)
        {
            _fooRepo = fooRepo;
            _fooReportRepo = fooReportRepo;
            _tmvFooRepo = tmvFooRepo;
        }
        public async Task CreateOrEdit(CreateOrEditFooterDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Footer_CreateEdit)]
        private Task Create(CreateOrEditFooterDto input)
        {
            throw new NotImplementedException();
        }

        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Footer_CreateEdit)]
        private async Task Update(CreateOrEditFooterDto input)
        {
            var footer = await _fooRepo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, footer);
            await _fooRepo.UpdateAsync(footer);
        }


        public async Task<PagedResultDto<GetFooterForViewDto>> GetAll(GetAllFooterInput input)
        {
            var filteredFooters = _fooRepo.GetAll();

            var pageAndFilteredFooters = filteredFooters.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);
            var footerReportTypes = _fooReportRepo.GetAll().OrderBy("id asc");
            var footerTmvs = _tmvFooRepo.GetAll();

            var footers = from o in footerReportTypes
                          join foo in pageAndFilteredFooters on o.Id equals foo.TypeId
                          join fooTmv in footerTmvs on o.Id equals fooTmv.TypeId
                        select new GetFooterForViewDto()
                        {
                            Footer = new FooterDto
                            {
                                Id = o.Id,
                                Code = o.Code,
                                Name = o.Name,
                                Description = o.Description,
                                FooterTmv = fooTmv.FooterTmv,
                                TenantFooter = foo.Footer
                            }
                        };

            var totalCount = await filteredFooters.CountAsync();

            return new PagedResultDto<GetFooterForViewDto>(
                totalCount,
                await footers.ToListAsync()
                );
        }
    }
}
