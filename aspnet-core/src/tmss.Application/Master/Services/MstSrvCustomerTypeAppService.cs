﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Core.Master.Services;
using tmss.Master.Services.CustomerType;
using tmss.Master.Services.CustomerType.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_CustomerType)]
    public class MstSrvCustomerTypeAppService : tmssAppServiceBase, IMstSrvCustomerTypeAppService
    {
        private readonly IRepository<MstSrvCustomerType, long> _repo;
        public MstSrvCustomerTypeAppService(IRepository<MstSrvCustomerType, long> repo)
        {
            _repo = repo;
        }

        public async Task CreateOrEdit(CreateOrEditCustomerTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_CustomerType_CreateEdit)]
        private async Task Update(CreateOrEditCustomerTypeDto input)
        {
            var cusType = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, cusType);
            await _repo.UpdateAsync(cusType);
        }

        //CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_CustomerType_CreateEdit)]
        private async Task Create(CreateOrEditCustomerTypeDto input)
        {
            var cusTypeExists = _repo.FirstOrDefaultAsync(o => o.CusTypeCode == input.CusTypeCode && o.Status == "Y");
            if (cusTypeExists.Result != null)
            {
                throw new UserFriendlyException(L("DataIsAlreadyExisted"));
            }
            //Check BankCode is exists or not
            var cusType = ObjectMapper.Map<MstSrvCustomerType>(input);
            cusType.Status = "Y";

            await _repo.InsertAsync(cusType);
        }

        public async Task Delete(EntityDto input)
        {
            var cusType = await _repo.FirstOrDefaultAsync((long)input.Id);
            await _repo.DeleteAsync(cusType);
        }

        public async Task<PagedResultDto<GetCustomerTypeForViewDto>> GetAll(GetAllCustomerTypeInput input)
        {
            var filteredCustomerTypes = _repo.GetAll()
                .Where(e => e.Status.Contains("Y"));

            var pageAndFilteredCustomerTypes = filteredCustomerTypes.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var customerTypes = from o in pageAndFilteredCustomerTypes
                        select new GetCustomerTypeForViewDto()
                        {
                            CustomerType = new CustomerTypeDto
                            {
                                Id = o.Id,
                                CusTypeCode = o.CusTypeCode,
                                CusTypeName = o.CusTypeName,
                            }
                        };

            var totalCount = await filteredCustomerTypes.CountAsync();

            return new PagedResultDto<GetCustomerTypeForViewDto>(
                totalCount,
                await customerTypes.ToListAsync()
                );
        }

        //get ById or edit
        [AbpAuthorize(AppPermissions.Pages_Master_Services_CustomerType_CreateEdit)]
        public async Task<GetCustomerTypeForEditOutput> GetCustomerTypeForEdit(EntityDto input)
        {
            var cusType = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetCustomerTypeForEditOutput { CustomerType = ObjectMapper.Map<CreateOrEditCustomerTypeDto>(cusType) };

            return output;
        }

        public async Task<GetCustomerTypeForViewDto> GetCustomerTypeForView(int id)
        {
            var bank = await _repo.GetAsync(id);
            var output = new GetCustomerTypeForViewDto { CustomerType = ObjectMapper.Map<CustomerTypeDto>(bank) };

            return output;
        }
    }
}
