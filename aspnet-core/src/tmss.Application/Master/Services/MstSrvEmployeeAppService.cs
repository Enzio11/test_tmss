﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Services.Division.Dto;
using tmss.Master.Services.Employee;
using tmss.Master.Services.Employee.Dto;
using tmss.Master.Services.Title;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Employee)]
    public class MstSrvEmployeeAppService : tmssAppServiceBase, IMstSrvEmployeeAppService
    {    
        private readonly IRepository<MstSrvEmployee, long> _repo;
        private readonly IRepository<MstSrvDivision, long> _divRepo;
        private readonly IRepository<MstSrvTitle, long> _titleRepo;

        public MstSrvEmployeeAppService(IRepository<MstSrvEmployee, long> repo, IRepository<MstSrvDivision, long> divRepo, IRepository<MstSrvTitle, long> titleRepo)
        {
            _repo = repo;
            _divRepo = divRepo;
            _titleRepo = titleRepo;
        }

        public async Task CreateOrEdit(CreateOrEditEmployeeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Employee_CreateEdit)]
        private async Task Update(CreateOrEditEmployeeDto input)
        {
            var employee = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, employee);
            await _repo.UpdateAsync(employee);
        }

        // CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Employee_CreateEdit)]
        private async Task Create(CreateOrEditEmployeeDto input)
        {
            var employee = ObjectMapper.Map<MstSrvEmployee>(input);
            employee.TenantId = AbpSession.TenantId.Value;

            await _repo.InsertAsync(employee);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Services_Employee_Delete)]
        public async Task Delete(EntityDto input)
        {
            var emp = await _repo.FirstOrDefaultAsync((long)input.Id);
            emp.Status = "N";
            await _repo.UpdateAsync(emp);
            // await _repo.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetEmployeeForViewDto>> GetAll(GetAllEmployeeInput input, long DivId)
        {

            var filteredEmps = _repo.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.EmpCode), e => false || e.EmpCode.Contains(input.EmpCode))
                .WhereIf(!string.IsNullOrWhiteSpace(input.EmpName), e => false || e.EmpCode.Contains(input.EmpName))
                .Where(e => e.Status.Contains("Y") && e.DivId == DivId);

            var pageAndFilteredEmps = filteredEmps.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var employees = from o in pageAndFilteredEmps
                        select new GetEmployeeForViewDto()
                        {
                            Employee = new EmployeeDto
                            {
                                Id = o.Id,
                                EmpCode = o.EmpCode,
                                EmpName = o.EmpName,
                                DivId = o.DivId
                            }
                        };

            var totalCount = await filteredEmps.CountAsync();

            return new PagedResultDto<GetEmployeeForViewDto>(
                totalCount,
                await employees.ToListAsync()
                );
        }
        // CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Employee_CreateEdit)]
        public async Task<GetEmployeeForEditOutput> GetEmployeeForEdit(EntityDto input)
        {
            var emp = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetEmployeeForEditOutput { Employee = ObjectMapper.Map<CreateOrEditEmployeeDto>(emp) };

            return output;
        }

        public async Task<GetEmployeeForViewDto> GetEmployeeForView(int id)
        {
            var emp = await _repo.GetAsync(id);
            var output = new GetEmployeeForViewDto { Employee = ObjectMapper.Map<EmployeeDto>(emp) };
            return output;
        }

        public async Task<List<DivisionDto>> GetAllDivision()
        {
            //// Check user is host or not
            //try
            //{
            //    int tenantId = AbpSession.TenantId.Value;
            //}
            //catch (Exception)
            //{
            //    throw new UserFriendlyException(L("TenantsDontHavePermission"));
            //}
            var divs = _divRepo.GetAll();
            var divisions = from o in divs
                select new DivisionDto
                {
                    Id = o.Id,
                    DivCode = o.DivCode,
                    DivName = o.DivName,
                    ParentDivId = o.ParentDivId
                };

            return await divisions.ToListAsync();
        }

        public async Task<List<TitleDto>> GetAllTitle()
        {
            var titles = _titleRepo.GetAll();
            
            var titleList = from o in titles
                            select new TitleDto
                            {
                                Id = o.Id,
                                TitleName = o.TitleName,
                                TitleCode = o.TitleCode
                            };

            return await titleList.ToListAsync();
        }


    }
}
