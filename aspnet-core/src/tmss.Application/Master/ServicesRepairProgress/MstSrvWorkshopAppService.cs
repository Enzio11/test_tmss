﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using tmss.Master.ServicesRepairProgress.Workshop.Dto;
using tmss.Master.ServicesRepairProgress.Workshop;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.UI;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvWorkshopAppService : tmssAppServiceBase, IMstSrvWorkshopAppService, IOrdering
  {
    private readonly IRepository<MstSrvWorkshop> _workshopRepository;
    public MstSrvWorkshopAppService(IRepository<MstSrvWorkshop> workshopRepository)
    {
      _workshopRepository = workshopRepository;
    }

    public async Task CreateOrUpdateWorkshop(CreateOrUpdateWorkshopInput input)
    {
      if (input.Id.HasValue)
      {
        var existingOrdering = await _workshopRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering && x.Id != input.Id);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var workshop = await _workshopRepository.GetAsync(input.Id.Value);
        workshop.WorkshopCode = input.WorkshopCode;
        workshop.WorkshopName = input.WorkshopName;
        workshop.WorkshopDesc = input.WorkshopDesc;
        workshop.Ordering = input.Ordering;
        workshop.IsActive = input.IsActive;
        await _workshopRepository.UpdateAsync(workshop);
      }
      else
      {
        var existingOrdering = await _workshopRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var workshop = ObjectMapper.Map<MstSrvWorkshop>(input);
        await _workshopRepository.InsertAsync(workshop);
      }
    }

    public async Task DeleteWorkshop(EntityDto<int> input)
    {
      await _workshopRepository.DeleteAsync(input.Id);
    }

    public async Task<PagedResultDto<WorkshopListDto>> GetvWorkshops(GetWorkshopsInput input)
    {
      var query = _workshopRepository
                  .GetAll()
                  .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.WorkshopCode.Contains(input.Filter) ||
                                                               p.WorkshopName.Contains(input.Filter))
                  .Where(p => p.IsActive == input.IsActive);
      var workshopCount = await query.CountAsync();
      var workshops = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

      return new PagedResultDto<WorkshopListDto>(
        workshopCount,
        ObjectMapper.Map<List<WorkshopListDto>>(workshops)
      );
    }

    public async Task<GetWorkshopForEditOutput> GetWorkshopForEdit(NullableIdDto<int> input)
    {
      var workshop = await _workshopRepository.GetAsync(input.Id.Value);
      return ObjectMapper.Map<GetWorkshopForEditOutput>(workshop);
    }

    public async Task<int> GetMaxOrdering()
    {
      var maxOrdering = await _workshopRepository.GetAll().OrderByDescending(x => x.Ordering).FirstOrDefaultAsync();
      if (maxOrdering != null)
        return maxOrdering.Ordering + 1;
      else
        return 1;
    }
  }
}