﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using tmss.Master.ServicesRepairProgress.BPProcess;
using tmss.Master.ServicesRepairProgress.BPProcess.Dto;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvBPProcessAppService : tmssAppServiceBase, IMstSrvBPProcessAppService, IOrdering
  {
    private readonly IRepository<MstSrvBPProcess> _BPProcessRepository;
    public MstSrvBPProcessAppService(IRepository<MstSrvBPProcess> BPProcessRepository)
    {
      _BPProcessRepository = BPProcessRepository;
    }

    public async Task CreateOrUpdateBPProcess(CreateOrUpdateBPProcessInput input)
    {
      if (input.Id.HasValue)
      {
        var existingOrdering = await _BPProcessRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering && x.Id != input.Id);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var BPProcess = await _BPProcessRepository.GetAsync(input.Id.Value);
        BPProcess.ProcessName = input.ProcessName;
        BPProcess.ProcessDesc = input.ProcessDesc;
        BPProcess.ColorCode = input.ColorCode;
        BPProcess.Ordering = input.Ordering;
        BPProcess.IsActive = input.IsActive;
        await _BPProcessRepository.UpdateAsync(BPProcess);
      }
      else
      {
        var existingOrdering = await _BPProcessRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var BPProcess = ObjectMapper.Map<MstSrvBPProcess>(input);
        await _BPProcessRepository.InsertAsync(BPProcess);
      }
    }

    public async Task DeleteBPProcess(EntityDto<int> input)
    {
      await _BPProcessRepository.DeleteAsync(input.Id);
    }

    public async Task<PagedResultDto<BPProcessListDto>> GetBPProcesses(GetBPProcessesInput input)
    {
      var query = _BPProcessRepository
                  .GetAll()
                  .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.ProcessName.Contains(input.Filter))
                  .Where(p => p.IsActive == input.IsActive);
      var BPProcessCount = await query.CountAsync();
      var BPProcesses = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

      return new PagedResultDto<BPProcessListDto>(
        BPProcessCount,
        ObjectMapper.Map<List<BPProcessListDto>>(BPProcesses)
      );
    }

    public async Task<GetBPProcessForEditOutput> GetBPProcessForEdit(NullableIdDto<int> input)
    {
      var BPProcess = await _BPProcessRepository.GetAsync(input.Id.Value);
      return ObjectMapper.Map<GetBPProcessForEditOutput>(BPProcess);
    }

    public async Task<int> GetMaxOrdering()
    {
      var maxOrdering = await _BPProcessRepository.GetAll().OrderByDescending(x => x.Ordering).FirstOrDefaultAsync();
      if (maxOrdering != null)
        return maxOrdering.Ordering + 1;
      else
        return 1;
    }
  }
}