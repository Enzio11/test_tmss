﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tmss.Master.ServicesRepairProgress.WorshopType;
using tmss.Master.ServicesRepairProgress.WorshopType.Dto;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvWorkshopTypeAppService : tmssAppServiceBase, IMstSrvWorkshopTypeAppService
  {
    private readonly IRepository<MstSrvWorkshopType> _workshopTypeRepository;
    public MstSrvWorkshopTypeAppService(IRepository<MstSrvWorkshopType> workshopTypeRepository)
    {
      _workshopTypeRepository = workshopTypeRepository;
    }

    public async Task CreateOrUpdateWorkshopType(CreateOrUpdateWorkshopTypeInput input)
    {
      if (input.Id.HasValue)
      {
        var workshopType = await _workshopTypeRepository.GetAsync(input.Id.Value);
        workshopType.WorkshopTypeCode = input.WorkshopTypeCode;
        workshopType.WorkshopTypeName = input.WorkshopTypeName;
        workshopType.WorkshopTypeDesc = input.WorkshopTypeDesc;
        workshopType.ColorCode = input.ColorCode;
        workshopType.IsActive = input.IsActive;
        await _workshopTypeRepository.UpdateAsync(workshopType);
      }
      else
      {
        var workshopType = ObjectMapper.Map<MstSrvWorkshopType>(input);
        await _workshopTypeRepository.InsertAsync(workshopType);
      }
    }

    public async Task DeleteWorkshopType(EntityDto<int> input)
    {
      await _workshopTypeRepository.DeleteAsync(input.Id);
    }

    public async Task<PagedResultDto<WorkshopTypeListDto>> GetvWorkshopTypes(GetWorkshopTypesInput input)
    {
      var query = _workshopTypeRepository
                  .GetAll()
                  .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.WorkshopTypeCode.Contains(input.Filter) ||
                                                               p.WorkshopTypeName.Contains(input.Filter))
                  .Where(p => p.IsActive == input.IsActive);
      var workshopTypeCount = await query.CountAsync();
      var workshopTypes = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

      return new PagedResultDto<WorkshopTypeListDto>(
        workshopTypeCount,
        ObjectMapper.Map<List<WorkshopTypeListDto>>(workshopTypes)
      );
    }

    public async Task<GetWorkshopTypeForEditOutput> GetWorkshopTypeForEdit(NullableIdDto<int> input)
    {
      var workshopType = await _workshopTypeRepository.GetAsync(input.Id.Value);
      return ObjectMapper.Map<GetWorkshopTypeForEditOutput>(workshopType);
    }
  }
}