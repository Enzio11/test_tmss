﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Title
{
    public class TitleDto : EntityDto<long>
    {
        [StringLength(50)]
        public string TitleCode { get; set; }
        [StringLength(50)]
        public string TitleName { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public long? EmpTypeId { get; set; }
        [Required]
        [StringLength(1)]
        public string Status { get; set; }
    }
}
