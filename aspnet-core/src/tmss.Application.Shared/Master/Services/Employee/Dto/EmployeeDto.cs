﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace tmss.Master.Services.Employee.Dto
{
    public class EmployeeDto : EntityDto<long>
    {
        [Required]
        [StringLength(20)]
        public string EmpCode { get; set; }
        [Required]
        [StringLength(50)]
        public string EmpName { get; set; }

        public long? DivId { get; set; }
    }
}
