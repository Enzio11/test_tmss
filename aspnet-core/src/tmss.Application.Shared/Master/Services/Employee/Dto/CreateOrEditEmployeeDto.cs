﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Employee.Dto
{
    public class CreateOrEditEmployeeDto : EntityDto<long?>
    {
        [Required]
        [StringLength(20)]
        public string EmpCode { get; set; }
        [Required]
        [StringLength(50)]
        public string EmpName { get; set; }
        [StringLength(20)]
        public string Tel { get; set; }
        public long? Sex { get; set; }
        [StringLength(100)]
        public string EmpAddress { get; set; }
        public long? TypeId { get; set; }
        public long? TitleId { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(30)]
        public string EmpColor { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? StartDate { get; set; }
        public byte[] EmpImg { get; set; }
        public long? DivId { get; set; }
    }
}
