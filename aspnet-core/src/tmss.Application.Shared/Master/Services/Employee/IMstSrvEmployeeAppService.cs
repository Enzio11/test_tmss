﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.Master.Services.Division.Dto;
using tmss.Master.Services.Employee.Dto;
using tmss.Master.Services.Title;

namespace tmss.Master.Services.Employee
{
    public interface IMstSrvEmployeeAppService : IApplicationService
    {
        Task<PagedResultDto<GetEmployeeForViewDto>> GetAll(GetAllEmployeeInput input, long DivId);

        Task<GetEmployeeForViewDto> GetEmployeeForView(int id);

        Task<GetEmployeeForEditOutput> GetEmployeeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployeeDto input);

        Task Delete(EntityDto input);

        Task<List<DivisionDto>> GetAllDivision();

        Task<List<TitleDto>> GetAllTitle();
    }
}
