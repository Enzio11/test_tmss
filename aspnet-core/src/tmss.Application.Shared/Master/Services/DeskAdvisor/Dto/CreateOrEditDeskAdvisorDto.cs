﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace tmss.Master.Services.DeskAdvisor.Dto
{
    public class CreateOrEditDeskAdvisorDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string DeskName { get; set; }
        public long? AdvisorId { get; set; }
        public string AdvisorCode { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
    }
}
