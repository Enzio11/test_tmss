﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Services.DeskAdvisor.Dto
{
    public class GetDeskAdvisorForEditOutput
    {
        public CreateOrEditDeskAdvisorDto DeskAdvisor { get; set; }
    }
}
