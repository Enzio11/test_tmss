﻿namespace tmss.Master.Services.Floor.Dto
{
    public class GetFloorForViewDto
    {
        public FloorDto Floor { get; set; }
    }
}
