﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Floor.Dto
{
    public class FloorDto : Entity<long>
    {
        [StringLength(200)]
        public string FloorName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        public long? Ordering { get; set; }
        [StringLength(1)]
        public string Type { get; set; }
    }
}
