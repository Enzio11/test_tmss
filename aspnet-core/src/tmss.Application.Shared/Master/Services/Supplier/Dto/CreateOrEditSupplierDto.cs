﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Supplier.Dto
{
    public class CreateOrEditSupplierDto : EntityDto<long?>
    {
        [StringLength(20)]
        public string SupplierCode { get; set; }
        [StringLength(50)]
        public string SupplierName { get; set; }
        [StringLength(100)]
        public string Address { get; set; }
        [StringLength(30)]
        public string AccNo { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(50)]
        public string Taxcode { get; set; }
        [StringLength(30)]
        public string Tel { get; set; }
        [StringLength(100)]
        public string Pic { get; set; }
        [StringLength(30)]
        public string Fax { get; set; }
        public long? BankId { get; set; }
        [StringLength(30)]
        public string PicTel { get; set; }
        public long? CountryId { get; set; }
        [StringLength(30)]
        public string PicMobi { get; set; }
        public double? LeadTime { get; set; }
        public long? Ordering { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
    }
}
