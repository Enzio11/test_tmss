﻿namespace tmss.Master.Services.Supplier.Dto
{
    public class GetSupplierForEditOutputDto
    {
        public CreateOrEditSupplierDto Supplier { get; set; }
    }
}
