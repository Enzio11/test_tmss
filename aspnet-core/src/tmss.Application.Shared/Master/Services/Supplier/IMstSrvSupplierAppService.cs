﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Services.Supplier.Dto;

namespace tmss.Master.Services.Supplier
{
    public interface IMstSrvSupplierAppService : IApplicationService
    {
        Task<PagedResultDto<GetSupplierForViewDto>> GetAll(GetAllSupplierInput input);

        Task<GetSupplierForViewDto> GetBankForView(int id);

        Task<GetSupplierForEditOutputDto> GetBankForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSupplierDto input);

        Task Delete(EntityDto input);
    }
}
