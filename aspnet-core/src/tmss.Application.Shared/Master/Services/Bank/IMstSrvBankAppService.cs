﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Services.Bank.Dto;

namespace tmss.Master.Services.Bank
{
    public interface IMstSrvBankAppService : IApplicationService
    {
        Task<PagedResultDto<GetBankForViewDto>> GetAll(GetAllBankInput input);

        Task<GetBankForViewDto> GetBankForView(int id);

        Task<GetBankForEditOutput> GetBankForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBankDto input);

        Task Delete(EntityDto input);
    }
}
