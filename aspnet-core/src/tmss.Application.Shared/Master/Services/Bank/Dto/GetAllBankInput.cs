﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.Bank.Dto
{
    public class GetAllBankInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
