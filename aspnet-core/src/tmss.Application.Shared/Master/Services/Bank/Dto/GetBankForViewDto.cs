﻿namespace tmss.Master.Services.Bank.Dto
{
    public class GetBankForViewDto
    {
        public MstSrvBankDto Bank { get; set; }
    }
}
