﻿namespace tmss.Master.Services.Bank.Dto
{
    public class GetBankForEditOutput
    {
        public CreateOrEditBankDto Bank { get; set; }
    }
}
