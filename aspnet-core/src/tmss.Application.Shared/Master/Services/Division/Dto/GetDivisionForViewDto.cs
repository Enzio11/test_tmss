﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.Division.Dto
{
    public class GetDivisionForViewDto : EntityDto<long>
    {
        public DivisionDto Division { get; set; }
    }
}
