﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.CustomerType.Dto
{
    public class CreateOrEditCustomerTypeDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string CusTypeCode { get; set; }
        [StringLength(50)]
        public string CusTypeName { get; set; }
    }
}
