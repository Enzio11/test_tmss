﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.CustomerType.Dto
{
    public class CustomerTypeDto : EntityDto<long>
    {
        [StringLength(50)]
        public string CusTypeCode { get; set; }
        [StringLength(50)]
        public string CusTypeName { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
    }
}
