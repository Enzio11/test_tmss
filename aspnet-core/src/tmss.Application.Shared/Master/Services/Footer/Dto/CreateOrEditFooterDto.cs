﻿using Abp.Domain.Entities;

namespace tmss.Master.Services.Footer.Dto
{
    public class CreateOrEditFooterDto : Entity<long?>
    {
        public string Footer { get; set; }
    }
}
