﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Footer.Dto
{
    public class FooterDto : EntityDto<long>
    {
        [StringLength(20)]
        public string Code { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(225)]
        public string Description { get; set; }
        public string FooterTmv { get; set; }
        public string TenantFooter { get; set; }
    }
}
