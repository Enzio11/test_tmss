﻿namespace tmss.Master.Services.Footer.Dto
{
    public class GetFooterForViewDto
    {
        public FooterDto Footer { get; set; }
    }
}
