﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Services.Footer.Dto;

namespace tmss.Master.Services.Footer
{
    public interface IMstSrvFooterAppService : IApplicationService
    {
        Task<PagedResultDto<GetFooterForViewDto>> GetAll(GetAllFooterInput input);

        Task CreateOrEdit(CreateOrEditFooterDto input);

    }
}
