﻿
using tmss.Master.Sales.Provinces.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstGenProvincesForEditOutput
    {
        public CreateOrEditMstGenProvinceDto MstGenProvinces { get; set; }


    }
}