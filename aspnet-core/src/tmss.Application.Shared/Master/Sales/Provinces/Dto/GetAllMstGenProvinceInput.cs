﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Master.Sales.Dtos
{
    public class GetAllMstGenProvincesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string NameFilter { get; set; }

        public decimal? MaxOrderingFilter { get; set; }
        public decimal? MinOrderingFilter { get; set; }

        public string StatusFilter { get; set; }
        public int SubRegionIdFilter { get; set; }

        public decimal? MaxPopulationAmountFilter { get; set; }
        public decimal? MinPopulationAmountFilter { get; set; }

        public decimal? MaxSquareAmountFilter { get; set; }
        public decimal? MinSquareAmountFilter { get; set; }

        public DateTime? MaxCreationTimeFilter { get; set; }
        public DateTime? MinCreationTimeFilter { get; set; }

        public DateTime? MaxLastModificationtTimeFilter { get; set; }
        public DateTime? MinLastModificationtTimeFilter { get; set; }

        public int IsDeletedFilter { get; set; }

        public DateTime? MaxDeletionTimeFilter { get; set; }
        public DateTime? MinDeletionTimeFilter { get; set; }



    }
}