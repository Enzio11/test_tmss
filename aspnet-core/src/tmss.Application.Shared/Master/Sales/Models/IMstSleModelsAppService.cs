﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Sales.Models.Dto;

namespace tmss.Master.Sales.Models
{
    public interface IMstSleModelsAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleModelsForViewDto>> GetAll(GetAllMstSleModelsInput input);
        Task<GetMstSleModelsForViewDto> GetMstSleModelsForView(long id);
        Task<GetMstSleModelsForEditOutput> GetMstSleModelsForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleModelsDto input);
        Task Delete(EntityDto<long> input);
    }
}
