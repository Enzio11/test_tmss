﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Sales.Models.Dto
{
    public class MstSleModelsDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }

        public string VnName { get; set; }

        public string EnName { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

        public decimal? OrderingRpt { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }

    }
}
