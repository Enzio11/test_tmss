﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.Dto
{
    public class MstSleLogisticCompaniesDto : EntityDto<long>
    {
        public string Code { get; set; }

        public string VnName { get; set; }

        public string EnName { get; set; }

        public string TaxCode { get; set; }

        public string BankNo { get; set; }

        public string Tel { get; set; }

        public string Fax { get; set; }

        public string Address { get; set; }

        public string ContactPerson { get; set; }

        public string Description { get; set; }

        public string OwnerType { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

        public long? LogisticsNumTofs { get; set; }

    }
}