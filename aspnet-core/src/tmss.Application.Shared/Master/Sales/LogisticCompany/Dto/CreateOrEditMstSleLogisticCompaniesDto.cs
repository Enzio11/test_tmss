﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.LogisticCompany.Dto
{
    public class CreateOrEditMstSleLogisticCompaniesDto : EntityDto<long?>
    {

        [Required]
        public string Code { get; set; }


        [Required]
        public string VnName { get; set; }


        [Required]
        public string EnName { get; set; }


        public string TaxCode { get; set; }


        public string BankNo { get; set; }


        public string Tel { get; set; }


        public string Fax { get; set; }


        public string Address { get; set; }


        public string ContactPerson { get; set; }


        public string Description { get; set; }


        public string OwnerType { get; set; }


        [Required]
        public string Status { get; set; }


        public decimal? Ordering { get; set; }


        public long? LogisticsNumTofs { get; set; }


        [Required]
        public DateTime CreationTime { get; set; }


        public DateTime? LastModificationtTime { get; set; }


        [Required]
        public bool IsDeleted { get; set; }


        public DateTime? DeletionTime { get; set; }



    }
}
