﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.LogisticCompany.Dto
{
    public class GetMstSleLogisticCompaniesForEditOutput
    {
        public CreateOrEditMstSleLogisticCompaniesDto MstSleLogisticCompanies { get; set; }


    }
}
