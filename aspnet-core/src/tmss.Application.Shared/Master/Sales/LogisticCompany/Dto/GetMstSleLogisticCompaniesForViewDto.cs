﻿
using tmss.Master.Sales.Dto;

namespace tmss.Master.Sales.LogisticCompany.Dto
{
    public class GetMstSleLogisticCompaniesForViewDto
    {
        public MstSleLogisticCompaniesDto MstSleLogisticCompanies { get; set; }


    }
}
