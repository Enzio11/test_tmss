﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Dto;
using tmss.Master.Sales.Dtos;

namespace tmss.Master.Sales.Lookup
{
    public interface IMstSleLookupAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleLookupForViewDto>> GetAll(GetAllMstSleLookupInput input);

        Task<GetMstSleLookupForViewDto> GetMstSleLookupForView(long id);

        Task<GetMstSleLookupForEditOutput> GetMstSleLookupForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleLookupDto input);

        Task Delete(EntityDto<long> input);

    }
}