﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Dto;

namespace tmss.Master.Sales
{
    public interface IMstSleAreasAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleAreasForViewDto>> GetAll(GetAllMstSleAreasInput input);

        Task<GetMstSleAreasForViewDto> GetMstSleAreasForView(long id);

        Task<GetMstSleAreasForEditOutput> GetMstSleAreasForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleAreasDto input);

        Task Delete(EntityDto<long> input);


    }
}