﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class CreateOrEditMstSleAreasDto : EntityDto<long?>
    {

        [Required]
        public string Name { get; set; }


        public string Status { get; set; }


        public string Description { get; set; }


        public decimal? Ordering { get; set; }
        [Required]
        public decimal YardId { get; set; }


        public string IsNoneAssignment { get; set; }



        [Required]
        public DateTime CreationTime { get; set; }


        public DateTime? LastModificationtTime { get; set; }


        [Required]
        public bool IsDeleted { get; set; }


        public DateTime? DeletionTime { get; set; }



    }
}