﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.DealerGroup.Dto
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto, ISortedResultRequest
    {
        public string Filter { get; set; }
    }
}