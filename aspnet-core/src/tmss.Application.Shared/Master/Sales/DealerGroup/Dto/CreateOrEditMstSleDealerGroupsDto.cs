﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.DealerGroup.Dto
{
	public class CreateOrEditMstSleDealerGroupsDto : EntityDto<long?>
	{

		public string GroupName { get; set; }


		public string Description { get; set; }


		[Required]
		public DateTime CreationTime { get; set; }


		public DateTime? LastModificationtTime { get; set; }


		[Required]
		public bool IsDeleted { get; set; }


		public DateTime? DeletionTime { get; set; }


		public string Status { get; set; }


		public decimal? Ordering { get; set; }



	}
}