﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;

namespace tmss.Master.Sales.Dtos
{
    public class GetAllMstSleColorsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string VnNameFilter { get; set; }

        public string EnNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public string StatusFilter { get; set; }

        public decimal? MaxOrderingFilter { get; set; }
        public decimal? MinOrderingFilter { get; set; }

        public decimal? MaxOrderingRptFilter { get; set; }
        public decimal? MinOrderingRptFilter { get; set; }

        public DateTime? MaxCreationTimeFilter { get; set; }
        public DateTime? MinCreationTimeFilter { get; set; }

        public DateTime? MaxLastModificationtTimeFilter { get; set; }
        public DateTime? MinLastModificationtTimeFilter { get; set; }

        public int IsDeletedFilter { get; set; }

        public DateTime? MaxDeletionTimeFilter { get; set; }
        public DateTime? MinDeletionTimeFilter { get; set; }



    }
}