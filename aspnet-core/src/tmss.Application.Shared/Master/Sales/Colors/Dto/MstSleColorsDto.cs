﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class MstSleColorsDto : EntityDto<long>
    {
        public string Code { get; set; }

        public string VnName { get; set; }

        public string EnName { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

        public decimal? OrderingRpt { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }



    }
}