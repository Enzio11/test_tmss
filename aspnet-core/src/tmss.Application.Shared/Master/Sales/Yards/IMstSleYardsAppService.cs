﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Dto;

namespace tmss.Master.Sales
{
    public interface IMstSleYardsAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleYardsForViewDto>> GetAll(GetAllMstSleYardsInput input);

        Task<GetMstSleYardsForViewDto> GetMstSleYardsForView(long id);

        Task<GetMstSleYardsForEditOutput> GetMstSleYardsForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleYardsDto input);

        Task Delete(EntityDto<long> input);


    }
}