﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.ServicesRepairProgress.BPProcess.Dto;

namespace tmss.Master.ServicesRepairProgress.BPProcess
{
  public interface IMstSrvBPProcessAppService : IApplicationService
  {
    Task<PagedResultDto<BPProcessListDto>> GetBPProcesses(GetBPProcessesInput input);
    Task<GetBPProcessForEditOutput> GetBPProcessForEdit(NullableIdDto<int> input);
    Task CreateOrUpdateBPProcess(CreateOrUpdateBPProcessInput input);
    Task DeleteBPProcess(EntityDto<int> input);
  }
}