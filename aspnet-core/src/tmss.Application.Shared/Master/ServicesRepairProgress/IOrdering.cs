﻿using System.Threading.Tasks;

namespace tmss.Master.ServicesRepairProgress
{
  public interface IOrdering
  {
    Task<int> GetMaxOrdering();
  }
}