﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.ServicesRepairProgress.Workshop.Dto;

namespace tmss.Master.ServicesRepairProgress.Workshop
{
  public interface IMstSrvWorkshopAppService : IApplicationService
  {
    Task<PagedResultDto<WorkshopListDto>> GetvWorkshops(GetWorkshopsInput input);
    Task<GetWorkshopForEditOutput> GetWorkshopForEdit(NullableIdDto<int> input);
    Task CreateOrUpdateWorkshop(CreateOrUpdateWorkshopInput input);
    Task DeleteWorkshop(EntityDto<int> input);
  }
}