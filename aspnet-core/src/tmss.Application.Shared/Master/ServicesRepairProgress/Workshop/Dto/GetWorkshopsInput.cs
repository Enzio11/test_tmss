﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using tmss.Dto;

namespace tmss.Master.ServicesRepairProgress.Workshop.Dto
{
  public class GetWorkshopsInput : PagedAndSortedInputDto, IShouldNormalize, ISortedResultRequest
  {
    public string Filter { get; set; }
    public bool IsActive { get; set; }
    public int? WorkshopType { get; set; }

    public void Normalize()
    {
      if (string.IsNullOrEmpty(Sorting))
      {
        Sorting = "Ordering,WorkshopCode";
      }

      Filter = Filter?.Trim();
    }
  }
}