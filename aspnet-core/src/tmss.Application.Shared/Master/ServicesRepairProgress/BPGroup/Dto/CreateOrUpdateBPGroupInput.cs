﻿using System.ComponentModel.DataAnnotations;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress.BPGroup.Dto
{
  public class CreateOrUpdateBPGroupInput
  {
    [Range(1, int.MaxValue)]
    public int? Id { get; set; }

    [Required]
    [MaxLength(Constants.MaxNameLength)]
    public string GroupName { get; set; }

    [MaxLength(Constants.MaxDescLength)]
    public string GroupDesc { get; set; }

    [Required]
    public int Ordering { get; set; }

    [Required]
    public bool IsActive { get; set; }
  }
}