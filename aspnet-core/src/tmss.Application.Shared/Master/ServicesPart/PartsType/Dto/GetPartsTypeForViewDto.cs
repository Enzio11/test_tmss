﻿namespace tmss.Master.ServicesPart.PartsType.Dto
{
    public class GetPartsTypeForViewDto
    {
        public PartsTypeDto PartsType { get; set; }
    }
}