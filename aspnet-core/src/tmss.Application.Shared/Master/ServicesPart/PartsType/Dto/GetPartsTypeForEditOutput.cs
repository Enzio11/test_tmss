﻿namespace tmss.Master.ServicesPart.PartsType.Dto
{
    public class GetPartsTypeForEditOutput
    {
        public CreateOrEditPartsTypeDto PartsType { get; set; }
    }
}