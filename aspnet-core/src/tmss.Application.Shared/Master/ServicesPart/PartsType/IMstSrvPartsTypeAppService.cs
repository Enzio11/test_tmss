﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using tmss.Master.ServicesPart.PartsType.Dto;

namespace tmss.Master.ServicesPart.PartsType
{
    public interface IMstSrvPartsTypeAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditPartsTypeDto input);
        Task Delete(EntityDto input);
        Task<GetPartsTypeForEditOutput> GetPartsTypeForEdit(EntityDto input);
        Task<GetPartsTypeForViewDto> GetPartsTypeForView(int id);
        Task<PagedResultDto<GetPartsTypeForViewDto>> GetAll(GetAllPartsTypeInput input);
    }
}
