﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using tmss.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod
{
    public interface IOrderMethodAppService : IApplicationService
    {
        Task<PagedResultDto<GetOrderMethodForViewDto>> GetAll(GetAllOrderMethodInput input);

        Task<GetOrderMethodForViewDto> GetOrderMethodForView(int id);

        Task<GetOrderMethodForEditOutput> GetOrderMethodForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOrderMethodDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetOrderMethodToExcel(GetAllOrderMethodForExcelInput input);
    }
}
