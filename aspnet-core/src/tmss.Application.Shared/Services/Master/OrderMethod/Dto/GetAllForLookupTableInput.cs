﻿using Abp.Application.Services.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
