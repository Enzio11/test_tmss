﻿namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class GetOrderMethodForViewDto
    {
        public OrderMethodDto OrderMethod { get; set; }
    }
}
