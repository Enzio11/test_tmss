﻿using Abp.Application.Services.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class GetAllOrderMethodInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string NameFilter { get; set; }
    }
}
