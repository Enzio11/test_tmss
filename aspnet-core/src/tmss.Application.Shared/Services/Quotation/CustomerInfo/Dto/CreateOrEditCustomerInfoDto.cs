﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace tmss.Services.Quotation.CustomerInfo.Dto
{
    public class CreateOrEditCustomerInfoDto : EntityDto<long?>
    {
        public string RegiterNo { get; set; }
        public string VinNo { get; set; }
        [StringLength(300)]
        public string CarOwnerName { get; set; }
        [StringLength(20)]
        public string CusNo { get; set; } // Customer Code???
        [StringLength(500)]
        public string OrgName { get; set; } // Old system: length = 2000
        public long? CusTypeId { get; set; } // Loai
        [StringLength(50)]
        public string TaxCode { get; set; }
        [StringLength(100)]
        public string CarOwnerEmail { get; set; }
        [StringLength(300)]
        public string CarOwnerAdd { get; set; }
        public long? ProvinceId { get; set; }
        public long? DistrictId { get; set; }
        [StringLength(25)]
        public string CarOwnerTel { get; set; }
        [StringLength(25)]
        public string CarOwnerMobile { get; set; }
        [StringLength(20)]
        public string CarOwnerFax { get; set; } // Consider to remove from screen
        [StringLength(100)]
        public string CarOwnerIdNum { get; set; } // Consider to display on screen
        public long VehicleId { get; set; }
        public long CusVsId { get; set; }
        public long AppId { get; set; }
        public long CampId { get; set; }
        public long CarFamilyType { get; set; }
        public string FullModel { get; set; }
        public long Type { get; set; } // Driver or CarOnwner
        public long VcId { get; set; }
        public string ColorCode { get; set; }
        public long RoId { get; set; }
        public int Km { get; set; }
        public long EngineTypeId { get; set; }
        public string EngineCode { get; set; }
        public string CusNote { get; set; }
        public long? CustomerDriverId { get; set; }
        public string InGate { get; set; }
        public bool Pds { get; set; }
        public long CusDriverId { get; set; } 
        public string Hybrid { get; set; }
        public long CmId { get; set; } // Car Model Id
        public string Ckd { get; set; } // 
        public long VhcType { get; set; } // Toyota, Lexus, Non-Toyota
        public DateTime DeliveryDate { get; set; }

        /*
         *
          orgname: res.customer ? res.customer.orgname : undefined,
          taxcode: res.customer ? res.customer.taxcode : undefined,
          cusno: res.customer ? res.customer.cusno : undefined,
          vehiclesId: val.vehicleId,
          registerNo: this.transformRegisterNo(val.registerNo),
          cusvsId: val.cusvsId,
          meetcus: res.cusVisit ? res.cusVisit.meetcus : null,
          appointmentId: val.appId,
          campId: null,
          customerId: res.customer ? res.customer.id : undefined,
          cfType: res.carFamily ? res.carFamily.cfType : undefined,
          fullmodel: res.vehicle ? res.vehicle.fullmodel : undefined,
          type: res.customerD ? res.customerD.type : '1',
          vcId: res.vehicleColor ? res.vehicleColor.id : undefined,
          vccode: res.vehicleColor ? res.vehicleColor.vcCode : undefined,
          roId: val.roId,
          usedCarId: res.usedCarList ? res.usedCarList.id : undefined,
          km: res.usedCarList ? res.usedCarList.km : undefined,
          effectToDate: res.usedCarList ? res.usedCarList.effectToDate : undefined,
          enginetypeId: res.engineType ? res.engineType.id : undefined,
          enginecode: res.engineType ? res.engineType.engineCode : undefined,
          cusNote: res.cusNote ? res.cusNote : undefined,
          cusDId: res.customerD && res.customerD.id ? res.customerD.id : undefined,
          inGate: res.inGate ? res.inGate : undefined,
          pds: this.transformRegisterNo(val.registerNo).indexOf('PDS') === 0,
          listCampaignId: res.listCampaignId,
          hybrid: res.vehicle && res.vehicle.hybrid === '1'
         */
    }
}
