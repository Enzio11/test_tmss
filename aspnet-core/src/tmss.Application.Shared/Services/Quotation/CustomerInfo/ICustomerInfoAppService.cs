﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Services.Quotation.CustomerInfo.Dto;

namespace tmss.Services.Quotation.CustomerInfo
{
    public interface ICustomerInfoAppService : IApplicationService
    {
        Task<PagedResultDto<GetCustomerInfoForViewDto>> GetAll(GetAllCustomerInfoInput input);

        Task<List<GetCustomerInfoForViewDto>> GetAllList();

        Task<GetCustomerInfoForViewDto> GetCustomerInfoForView(int id);

        Task<GetCustomerInfoForEditOutput> GetCustomerInfoForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCustomerInfoDto input);

        Task Delete(EntityDto input);
    }
}
