﻿namespace tmss.Services.RepairProgress
{
  public class Constants
  {
    public const int MaxNameLength = 32;
    public const int MaxDescLength = 255;
  }
}