import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
	imports: [
		RouterModule.forChild([
			{
				path: '',
				children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
					{ path: '**', redirectTo: 'dashboard' }
				]
			}
		])
	],
	exports: [
		RouterModule
	]
})
export class SalesRoutingModule { }