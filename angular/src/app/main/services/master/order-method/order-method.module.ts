import { NgModule } from "@angular/core";
import { OrderMethodComponent } from "./order-method.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TableModule } from "primeng/table";
import { PaginatorModule } from "primeng/paginator";
import { ProgressBarModule } from "primeng/progressbar";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
// import {
//     ModalModule,
//     TooltipModule,
//     TabsModule,
//     BsDropdownModule,
//     PopoverModule,
//     BsDatepickerModule,
// } from "ngx-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
// import { AbpModule } from "@abp/abp.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { OrderMethodRoutingRoutes } from "./order-method-routing.module";
import { CommonModule } from "@angular/common";
import { ViewOrderMethodModalComponent } from "./view-order-method-modal.component";
import { CreateOrEditOrderMethodModalComponent } from "./create-or-edit-order-method-modal.component";
import { AppCommonModule } from "@app/shared/common/app-common.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TableModule,
        PaginatorModule,
        ProgressBarModule,
        HttpClientModule,
        HttpClientJsonpModule,
        OrderMethodRoutingRoutes,
        ModalModule.forRoot(),
        TooltipModule.forRoot(),
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        BsDatepickerModule.forRoot(),
        // AbpModule,
        UtilsModule,
        AppCommonModule
    ],
    declarations: [
        OrderMethodComponent,
        ViewOrderMethodModalComponent,
        CreateOrEditOrderMethodModalComponent
    ],
})
export class OrderMethodModule {}
