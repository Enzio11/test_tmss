import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
// import { ModalDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetOrderMethodForViewDto, OrderMethodDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewOrderMethodModal',
    templateUrl: './view-order-method-modal.component.html'
})
export class ViewOrderMethodModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetOrderMethodForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetOrderMethodForViewDto();
        this.item.orderMethod = new OrderMethodDto();
    }

    show(item: GetOrderMethodForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
