import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from "@angular/core";
// import { ModalDirective } from "ngx-bootstrap";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from "rxjs/operators";
import {
    OrderMethodServiceProxy,
    CreateOrEditOrderMethodDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";

@Component({
    selector: "createOrEditOrderMethodModal",
    templateUrl: "./create-or-edit-order-method-modal.component.html",
})
export class CreateOrEditOrderMethodModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    orderMethod: CreateOrEditOrderMethodDto = new CreateOrEditOrderMethodDto();

    constructor(
        injector: Injector,
        private _orderMethodServiceProxy: OrderMethodServiceProxy
    ) {
        super(injector);
    }

    show(orderMethodId?: number): void {
        if (!orderMethodId) {
            this.orderMethod = new CreateOrEditOrderMethodDto();
            this.orderMethod.id = orderMethodId;

            this.active = true;
            this.modal.show();
        } else {
            this._orderMethodServiceProxy
                .getOrderMethodForEdit(orderMethodId)
                .subscribe((result) => {
                    this.orderMethod = result.orderMethod;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._orderMethodServiceProxy
            .createOrEdit(this.orderMethod)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
