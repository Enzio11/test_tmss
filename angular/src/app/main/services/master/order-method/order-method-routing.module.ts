import { Routes, RouterModule } from "@angular/router";
import { OrderMethodComponent } from "./order-method.component";

const routes: Routes = [
    {
        path: "",
        component: OrderMethodComponent,
        data: { permission: "Pages.OrderMethods" },
    },
];

export const OrderMethodRoutingRoutes = RouterModule.forChild(routes);
