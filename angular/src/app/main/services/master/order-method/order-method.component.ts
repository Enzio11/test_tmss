import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
    OrderMethodServiceProxy,
    OrderMethodDto,
} from "@shared/service-proxies/service-proxies";
// import { NotifyService } from "@abp/notify/notify.service";
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from "@shared/common/app-component-base";
import { TokenAuthServiceProxy } from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
// import { Table } from "primeng/components/table/table";
import { Table } from 'primeng/table';
// import { Paginator } from "primeng/components/paginator/paginator";
import { Paginator } from 'primeng/paginator';
// import { LazyLoadEvent } from "primeng/components/common/lazyloadevent";
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from "@shared/utils/file-download.service";
import { EntityTypeHistoryModalComponent } from "@app/shared/common/entityHistory/entity-type-history-modal.component";
import * as _ from "lodash";
import * as moment from "moment";
import { CreateOrEditOrderMethodModalComponent } from "./create-or-edit-order-method-modal.component";
import { ViewOrderMethodModalComponent } from "./view-order-method-modal.component";

@Component({
    templateUrl: "./order-method.component.html",
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class OrderMethodComponent extends AppComponentBase {
    @ViewChild("createOrEditOrderMethodModal", { static: true })
    createOrEditOrderMethodModal: CreateOrEditOrderMethodModalComponent;
    @ViewChild("viewOrderMethodModalComponent", { static: true })
    viewOrderMethodModal: ViewOrderMethodModalComponent;
    @ViewChild("entityTypeHistoryModal", { static: true })
    entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    codeFilter = "";
    nameFilter = "";

    _entityTypeFullName = "tmss.Core.Services.Master.OrderMethod.OrderMethod";
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _orderMethodServiceProxy: OrderMethodServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return (
            customSettings.EntityHistory &&
            customSettings.EntityHistory.isEnabled &&
            _.filter(
                customSettings.EntityHistory.enabledEntities,
                (entityType) => entityType === this._entityTypeFullName
            ).length === 1
        );
    }

    getOrderMethods(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._orderMethodServiceProxy
            .getAll(
                this.filterText,
                this.codeFilter,
                this.nameFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrderMethod(): void {
        this.createOrEditOrderMethodModal.show();
    }

    showHistory(orderMethod: OrderMethodDto): void {
        this.entityTypeHistoryModal.show({
            entityId: orderMethod.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: "",
        });
    }

    deleteOrderMethod(orderMethod: OrderMethodDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._orderMethodServiceProxy
                    .delete(orderMethod.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    // exportToExcel(): void {
    //   this._orderMethodServiceProxy.getCoronasToExcel(
    //     this.filterText,
    //     this.nameFilter,
    //     this.symptomFilter,
    //   )
    //     .subscribe(result => {
    //       this._fileDownloadService.downloadTempFile(result);
    //     });
    // }

    exportToExcel(): void {}
}
