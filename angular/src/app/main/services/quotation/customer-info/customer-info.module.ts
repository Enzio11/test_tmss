import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerInfoComponent } from './customer-info.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CustomerInfoComponent]
})
export class CustomerInfoModule { }
