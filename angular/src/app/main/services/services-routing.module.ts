import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GeneralRepairComponent } from './repair-progress/general-repair/general-repair.component';

@NgModule({
	imports: [
		RouterModule.forChild([
			{
				path: '',
				children: [
          { path: 'general-repair', component: GeneralRepairComponent },
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
					{ path: '**', redirectTo: 'dashboard' }
				]
			}
		])
	],
	exports: [
		RouterModule
	]
})
export class ServicesRoutingModule { }