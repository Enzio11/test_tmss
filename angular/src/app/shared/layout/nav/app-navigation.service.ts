﻿import { PermissionCheckerService } from 'abp-ng2-module';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {
  constructor(
      private _permissionCheckerService: PermissionCheckerService,
      private _appSessionService: AppSessionService
  ) {
  }

  getMenu(): AppMenu {
    return new AppMenu('MainMenu', 'MainMenu', [
      new AppMenuItem('Dashboard', 'Pages.Administration.Host.Dashboard', 'flaticon-line-graph', '/app/admin/hostDashboard'),
      new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'flaticon-line-graph', '/app/main/dashboard'),
      new AppMenuItem('Master', 'Pages.Master', 'flaticon-book', '', [], [
        new AppMenuItem('Common', 'Pages.Master.Common', 'flaticon-map', '', [], [
        ]),
        new AppMenuItem('Sales', 'Pages.Master.Sales', 'flaticon-map', '', [], [
          new AppMenuItem('Dealer List managerment', 'Pages.Master.Sales.MstGenDealers', 'flaticon-list-3','/app/master/dealer'),
          new AppMenuItem('Province List managerment', 'Pages.MstGenProvinces', 'flaticon-list-3','/app/master/province'),
          new AppMenuItem('Colors List Managerment', 'Pages.MstSleColors', 'flaticon-list-3','/app/master/colors'),
          new AppMenuItem('Yards Managerment', 'Pages.MstSleYards', 'flaticon-list-3','/app/master/yards'),
          new AppMenuItem('Yards Region Management', 'Pages.MstSleAreas', 'flaticon-list-3','/app/master/areas'),
          new AppMenuItem('Lookup', 'Pages.MstSleLookup', 'flaticon-list-3','/app/master/lookup'),
          new AppMenuItem('Dealer Group', 'Pages.Master.Sales.MstSleDealerGroups', 'flaticon-list-3','/app/master/groupdealer'),
          new AppMenuItem('Models List', null, 'flaticon-list-3', '/app/master/models')
        ]),
        new AppMenuItem('Services', 'Pages.Master.Services', 'flaticon-map', '', [], [
          new AppMenuItem('Bank', 'Pages.Master.Services.Bank', 'flaticon-list-3', '/app/master/bank-service'),
          new AppMenuItem('Supplier', 'Pages.Master.Services.Supplier', 'flaticon-list-3', '/app/master/supplier'),
          new AppMenuItem('Customer Types', 'Pages.Master.Services.CustomerType', 'flaticon-list-3', '/app/master/customer-type'),
          new AppMenuItem('Desk Advisor', 'Pages.Master.Services.DeskAdvisor', 'flaticon-list-3', '/app/master/desk-advisor'),
          new AppMenuItem('Employee', 'Pages.Master.Services.Employee', 'flaticon-list-3', '/app/master/employee'),
          new AppMenuItem('Division', 'Pages.Master.Services.Division', 'flaticon-list-3', '/app/master/division'),
          new AppMenuItem('Floor', 'Pages.Master.Services.Floor', 'flaticon-list-3', '/app/master/floor'),
          new AppMenuItem('Footer Declaration', 'Pages.Master.Services.Footer', 'flaticon-list-3', '/app/master/footer-catgegory'),
          new AppMenuItem('Dealer Config', 'Pages.Master.Services.DlrConfig', 'flaticon-list-3', '/app/master/dlr-config'),
          new AppMenuItem('Parameter Operation Agent', 'Pages.Master.Services.ParamOperationAgent', 'flaticon-list-3', '/app/master/parameter-operation-agent'),
          new AppMenuItem('General Repair', 'Pages.Master.Services.GeneralRepair', 'flaticon-list-3', '/app/master/general-repair'),
          new AppMenuItem('Apply GJ Job', 'Pages.Master.Services.ApplyJob', 'flaticon-list-3', '/app/master/apply-gj-job'),
          new AppMenuItem('Apply BP Job', 'Pages.Master.Services.ApplyBPJob', 'flaticon-list-3', '/app/master/apply-bp-job'),
          new AppMenuItem('Apply BP Job', 'Pages.Master.Services.ApplyBPJob', 'flaticon-list-3', '/app/master/apply-bp-job')
        ]),
        new AppMenuItem('Part', 'Pages.Master.ServicesPart', 'flaticon-map', '', [], [
          new AppMenuItem('Parts Type', 'Pages.Master.ServicesPart.PartsType', 'flaticon-list-3', '/app/master/parts-type'),
        ]),
        new AppMenuItem('Quotation', 'Pages.Master.ServicesQuotation', 'flaticon-map', '', [], [
          new AppMenuItem('Customer Information', 'Pages.Services.Quotation.CustomerInfo', 'flaticon-list-3', '/app/services/quotation/customer-info')
        ]),
        new AppMenuItem('Repair Progress', 'Pages.Master.ServicesRepairProgress', 'flaticon-map', '', [], [
          new AppMenuItem('BP Group', 'Pages.Master.ServicesRepairProgress.BPGroup', 'flaticon-list-3', '/app/master/bp-group'),
        ]),
        new AppMenuItem('Warranty', 'Pages.Master.ServicesWarranty', 'flaticon-map', '', [], [
        ])
      ]),
      new AppMenuItem('Sales', 'Pages.Sales', 'flaticon-book', '', [], [
        new AppMenuItem('Contract', 'Pages.Sales.Contract', 'flaticon-map', '', [], [
        ]),
        new AppMenuItem('Vehicle', 'Pages.Sales.Vehicle', 'flaticon-map', '', [], [
        ])
      ]),
      new AppMenuItem('Services', 'Pages.Services', 'flaticon-book', '', [], [
        new AppMenuItem('Part', 'Pages.Services.Part', 'flaticon-map', '', [], [

        ]),
        new AppMenuItem('Quotation', 'Pages.Services.Quotation', 'flaticon-map', '', [], [
        ]),
        new AppMenuItem('Repair Progress', 'Pages.Services.RepairProgress', 'flaticon-map', '', [], [
          new AppMenuItem('General Repair', '', 'flaticon-list-3', '/app/services/general-repair'),
        ]),
        new AppMenuItem('Warranty', 'Pages.Services.Warranty', 'flaticon-map', '', [], [
        ])
      ]),
      new AppMenuItem('Tenants', 'Pages.Tenants', 'flaticon-list-3', '/app/admin/tenants'),
      new AppMenuItem('Editions', 'Pages.Editions', 'flaticon-app', '/app/admin/editions'),
      new AppMenuItem('Administration', '', 'flaticon-interface-8', '', [], [
        new AppMenuItem('OrganizationUnits', 'Pages.Administration.OrganizationUnits', 'flaticon-map', '/app/admin/organization-units'),
        new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-suitcase', '/app/admin/roles'),
        new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-users', '/app/admin/users'),
        new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages', ['/app/admin/languages/{name}/texts']),
        new AppMenuItem('AuditLogs', 'Pages.Administration.AuditLogs', 'flaticon-folder-1', '/app/admin/auditLogs'),
        new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-lock', '/app/admin/maintenance'),
        new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
        new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
        new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-settings', '/app/admin/hostSettings'),
        new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-settings', '/app/admin/tenantSettings'),
        new AppMenuItem('WebhookSubscriptions', 'Pages.Administration.WebhookSubscription', 'flaticon2-world', '/app/admin/webhook-subscriptions'),
        new AppMenuItem('DynamicParameters', '', 'flaticon-interface-8', '', [], [
          new AppMenuItem('Definitions', 'Pages.Administration.DynamicParameters', '', '/app/admin/dynamic-parameter'),
          new AppMenuItem('EntityDynamicParameters', 'Pages.Administration.EntityDynamicParameters', '', '/app/admin/entity-dynamic-parameter'),
        ])
      ]),
      new AppMenuItem('DemoUiComponents', 'Pages.DemoUiComponents', 'flaticon-shapes', '/app/admin/demo-ui-components')
    ]);
  }

  checkChildMenuItemPermission(menuItem): boolean {
    for (let i = 0; i < menuItem.items.length; i++) {
      let subMenuItem = menuItem.items[i];

      if (subMenuItem.permissionName === '' || subMenuItem.permissionName === null) {
        if (subMenuItem.route) {
          return true;
        }
      } else if (this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
        return true;
      }

      if (subMenuItem.items && subMenuItem.items.length) {
        let isAnyChildItemActive = this.checkChildMenuItemPermission(subMenuItem);
        if (isAnyChildItemActive) {
          return true;
        }
      }
    }
    return false;
  }

  showMenuItem(menuItem: AppMenuItem): boolean {
    if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
      return false;
    }

    let hideMenuItem = false;

    if (menuItem.requiresAuthentication && !this._appSessionService.user) {
      hideMenuItem = true;
    }

    if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
      hideMenuItem = true;
    }

    if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
      if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
        hideMenuItem = true;
      }
    }

    if (!hideMenuItem && menuItem.items && menuItem.items.length) {
      return this.checkChildMenuItemPermission(menuItem);
    }

    return !hideMenuItem;
  }

  /**
   * Returns all menu items recursively
   */
  getAllMenuItems(): AppMenuItem[] {
    let menu = this.getMenu();
    let allMenuItems: AppMenuItem[] = [];
    menu.items.forEach(menuItem => {
      allMenuItems = allMenuItems.concat(this.getAllMenuItemsRecursive(menuItem));
    });

    return allMenuItems;
  }

  private getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {
    if (!menuItem.items) {
      return [menuItem];
    }

    let menuItems = [menuItem];
    menuItem.items.forEach(subMenu => {
      menuItems = menuItems.concat(this.getAllMenuItemsRecursive(subMenu));
    });

    return menuItems;
  }
}