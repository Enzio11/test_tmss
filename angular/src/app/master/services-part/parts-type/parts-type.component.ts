import { AppComponentBase } from '@shared/common/app-component-base';
import { MstSrvPartsTypeServiceProxy, TokenAuthServiceProxy, PartsTypeDto } from './../../../../shared/service-proxies/service-proxies';
import { ViewPartsTypeModalComponent } from './view-parts-type-modal.component';
import { Component, ViewEncapsulation, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import { CreateOrEditPartsTypeModalComponent } from './create-or-edit-parts-type-modal.component';
import { NotifyService } from 'abp-ng2-module';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { Table } from 'primeng/table';
import { LazyLoadEvent } from 'primeng/api/lazyloadevent';
import { Paginator } from 'primeng/paginator';

@Component({
  selector: 'app-parts-type',
  templateUrl: './parts-type.component.html'
})
export class PartsTypeComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true })
  createOrEditModal: CreateOrEditPartsTypeModalComponent;
  @ViewChild('viewModal', { static: true })
  viewModal: ViewPartsTypeModalComponent;
  @ViewChild('entityTypeHistoryModal', { static: true })
  entityTypeHistoryModal: EntityTypeHistoryModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.PartsType.PartsType';
  entityHistoryEnabled = false;

  advancedFiltersAreShown = false;
  filterText = '';

  constructor(
    injector: Injector,
    private _serviceProxy: MstSrvPartsTypeServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
  }

  private setIsEntityHistoryEnabled(): boolean {
    let customSettings = (abp as any).custom;
    return (
      customSettings.EntityHistory &&
      customSettings.EntityHistory.isEnabled &&
      _.filter(
        customSettings.EntityHistory.enabledEntities,
        (entityType) => entityType === this._entityTypeFullName
      ).length === 1
    );
  }

  getPartsType(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();

    this._serviceProxy
      .getAll(
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  createPartsType(): void {
    this.createOrEditModal.show();
  }

  showHistory(partsType: PartsTypeDto): void {
    this.entityTypeHistoryModal.show({
      entityId: partsType.id.toString(),
      entityTypeFullName: this._entityTypeFullName,
      entityTypeDescription: '',
    });
  }

  deletePartsType(partsType: PartsTypeDto): void {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
      if (isConfirmed) {
        this._serviceProxy
          .delete(partsType.id)
          .subscribe(() => {
            this.reloadPage();
            this.notify.success(this.l('SuccessfullyDeleted'));
          });
      }
    });
  }
}
