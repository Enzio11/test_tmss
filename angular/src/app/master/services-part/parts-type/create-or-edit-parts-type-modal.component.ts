import { CreateOrEditPartsTypeDto, MstSrvPartsTypeServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from '@angular/core';
// import { ModalDirective } from "ngx-bootstrap";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditPartsTypeModal',
    templateUrl: './create-or-edit-parts-type-modal.component.html',
})
export class CreateOrEditPartsTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    partsType: CreateOrEditPartsTypeDto = new CreateOrEditPartsTypeDto();

    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvPartsTypeServiceProxy
    ) {
        super(injector);
    }

    show(typeId?: number): void {
        if (!typeId) {
            this.partsType = new CreateOrEditPartsTypeDto();
            this.partsType.id = typeId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getPartsTypeForEdit(typeId)
                .subscribe((result) => {
                    this.partsType = result.partsType;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._serviceProxy
            .createOrEdit(this.partsType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
