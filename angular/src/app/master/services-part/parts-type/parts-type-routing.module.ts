import { PartsTypeComponent } from './parts-type.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: PartsTypeComponent,
        data: { permission: 'Pages.Master.ServicesPart.PartsType' },
    },
];

export const PartsTypeRoutingRoutes = RouterModule.forChild(routes);
