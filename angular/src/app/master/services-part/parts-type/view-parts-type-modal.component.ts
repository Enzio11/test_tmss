import { PartsTypeDto, GetPartsTypeForViewDto } from './../../../../shared/service-proxies/service-proxies';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
// import { ModalDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewPartsTypeModal',
    templateUrl: './view-parts-type-modal.component.html'
})
export class ViewPartsTypeModalComponent extends AppComponentBase {

    @ViewChild('viewModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetPartsTypeForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetPartsTypeForViewDto();
        this.item.partsType = new PartsTypeDto();
    }

    show(item: GetPartsTypeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
