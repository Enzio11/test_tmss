import {
    Component,
    OnInit,
    ViewEncapsulation,
    ViewChild,
    Injector,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/public_api";
import { CreateOrEditMstSleModelsModalComponent } from "./create-or-edit-mstSleModels-modal.component";
import { ViewMstSleModelsModalComponent } from "./view-mstSleModels-modal.component";
import * as _ from "lodash";
import * as moment from "moment";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    MstSleModelsServiceProxy,
    TokenAuthServiceProxy,
    MstSleModelsDto,
} from "@shared/service-proxies/service-proxies";
import { NotifyService } from "abp-ng2-module";
import { ActivatedRoute } from "@angular/router";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: "./mstSleModels.component.html",
    styleUrls: ["./mstSleModels.component.less"],
    animations: [appModuleAnimation()],
})
export class MstSleModelsComponent extends AppComponentBase {
    @ViewChild("createOrEditMstSleModelsModal", { static: true })
    createOrEditMstSleModelsModal: CreateOrEditMstSleModelsModalComponent;
    @ViewChild("viewMstSleColorsModalComponent", { static: true })
    viewMstSleModelsModal: ViewMstSleModelsModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    marketingCodeFilter = "";
    productionCodeFilter = "";
    vnNameFilter = "";
    enNameFilter = "";
    descriptionFilter = "";
    statusFilter = "";
    maxOrderingFilter: number;
    maxOrderingFilterEmpty: number;
    minOrderingFilter: number;
    minOrderingFilterEmpty: number;
    maxOrderingRptFilter: number;
    maxOrderingRptFilterEmpty: number;
    minOrderingRptFilter: number;
    minOrderingRptFilterEmpty: number;
    maxCreationTimeFilter: moment.Moment;
    minCreationTimeFilter: moment.Moment;
    maxLastModificationtTimeFilter: moment.Moment;
    minLastModificationtTimeFilter: moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter: moment.Moment;
    minDeletionTimeFilter: moment.Moment;

    constructor(
        injector: Injector,
        private _mstSleModelsServiceProxy: MstSleModelsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstSleModels(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstSleModelsServiceProxy
            .getAll(
                this.filterText,
                this.marketingCodeFilter,
                this.productionCodeFilter,
                this.vnNameFilter,
                this.enNameFilter,
                this.descriptionFilter,
                this.statusFilter,
                this.maxCreationTimeFilter,
                this.minCreationTimeFilter,
                this.maxLastModificationtTimeFilter,
                this.minLastModificationtTimeFilter,
                this.maxOrderingFilter == null
                    ? this.maxOrderingFilterEmpty
                    : this.maxOrderingFilter,
                this.minOrderingFilter == null
                    ? this.minOrderingFilterEmpty
                    : this.minOrderingFilter,
                this.maxOrderingRptFilter == null
                    ? this.maxOrderingRptFilterEmpty
                    : this.maxOrderingRptFilter,
                this.minOrderingRptFilter == null
                    ? this.minOrderingRptFilterEmpty
                    : this.minOrderingRptFilter,
                this.isDeletedFilter,
                this.maxDeletionTimeFilter,
                this.minDeletionTimeFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleModels(): void {
        this.createOrEditMstSleModelsModal.show();
    }

    deleteMstSleModels(mstSleModels: MstSleModelsDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleModelsServiceProxy
                    .delete(mstSleModels.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    exportToExcel(): void {}

    ngOnInit() {}
}
