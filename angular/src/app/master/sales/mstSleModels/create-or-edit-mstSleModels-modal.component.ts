import { Component, ViewChild, Output, EventEmitter, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap/modal";
import { CreateOrEditMstSleModelsDto, MstSleModelsServiceProxy } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { finalize } from "rxjs/operators";

@Component({
    selector: "createOrEditMstSleModelsModal",
    templateUrl: "./create-or-edit-mstSleModels-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleModels-modal.component.less"],
})
export class CreateOrEditMstSleModelsModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstSleModels: CreateOrEditMstSleModelsDto = new CreateOrEditMstSleModelsDto();

    lastModificationtTime: Date;
    deletionTime: Date;

    constructor(
        injector: Injector,
        private _mstSleModelsServiceProxy: MstSleModelsServiceProxy
    ) {
        super(injector);
    }

    show(mstSleModelsId?: number): void {
        this.lastModificationtTime = null;
        this.deletionTime = null;

        if (!mstSleModelsId) {
            this.mstSleModels = new CreateOrEditMstSleModelsDto();
            this.mstSleModels.id = mstSleModelsId;
            this.mstSleModels.creationTime = moment().startOf("day");

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleModelsServiceProxy
                .getMstSleModelsForEdit(mstSleModelsId)
                .subscribe((result) => {
                    this.mstSleModels = result.mstSleModels;

                    if (this.mstSleModels.lastModificationTime) {
                        this.lastModificationtTime = this.mstSleModels.lastModificationTime.toDate();
                    }
                    if (this.mstSleModels.deletionTime) {
                        this.deletionTime = this.mstSleModels.deletionTime.toDate();
                    }

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;

        if (this.lastModificationtTime) {
            if (!this.mstSleModels.lastModificationTime) {
                this.mstSleModels.lastModificationTime = moment(
                    this.lastModificationtTime
                ).startOf("day");
            } else {
                this.mstSleModels.lastModificationTime = moment(
                    this.lastModificationtTime
                );
            }
        } else {
            this.mstSleModels.lastModificationTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstSleModels.deletionTime) {
                this.mstSleModels.deletionTime = moment(
                    this.deletionTime
                ).startOf("day");
            } else {
                this.mstSleModels.deletionTime = moment(this.deletionTime);
            }
        } else {
            this.mstSleModels.deletionTime = null;
        }
        this._mstSleModelsServiceProxy
            .createOrEdit(this.mstSleModels)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}