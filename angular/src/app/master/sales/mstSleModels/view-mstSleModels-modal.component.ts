import { Component, ViewChild, Output, EventEmitter, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap/modal";
import { GetMstSleModelsForViewDto, MstSleModelsDto } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "viewMstSleModelsModal",
    templateUrl: "./view-mstSleModels-modal.component.html",
})
export class ViewMstSleModelsModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstSleModelsForViewDto;

    constructor(injector: Injector) {
        super(injector);
        this.item = new GetMstSleModelsForViewDto();
        this.item.mstSleModels = new MstSleModelsDto();
    }

    show(item: GetMstSleModelsForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}