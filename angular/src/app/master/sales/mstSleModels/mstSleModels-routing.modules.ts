import { Routes, RouterModule } from "@angular/router";
import { MstSleModelsComponent } from "./mstSleModels.component";
import { CreateOrEditMstSleModelsModalComponent } from "./create-or-edit-mstSleModels-modal.component";
import { ViewMstSleModelsModalComponent } from "./view-mstSleModels-modal.component";

const routes: Routes = [
    {
        path: '',
        component: MstSleModelsComponent,
    },
    {
        path: '',
        component: CreateOrEditMstSleModelsModalComponent,
    },
    {
        path: '',
        component: ViewMstSleModelsModalComponent,
    },
];

export const MstSleModelsRoutingRoute = RouterModule.forChild(routes);
