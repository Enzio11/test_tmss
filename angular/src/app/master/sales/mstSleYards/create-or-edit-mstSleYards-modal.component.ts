import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleYardsServiceProxy, CreateOrEditMstSleYardsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMstSleYardsModal',
    templateUrl: './create-or-edit-mstSleYards-modal.component.html',
    styleUrls:["./create-or-edit-mstSleYards-modal.component.less"]
})
export class CreateOrEditMstSleYardsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstSleYards: CreateOrEditMstSleYardsDto = new CreateOrEditMstSleYardsDto();

    lastModificationtTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstSleYardsServiceProxy: MstSleYardsServiceProxy
    ) {
        super(injector);
    }

    show(mstSleYardsId?: number): void {
    this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstSleYardsId) {
            this.mstSleYards = new CreateOrEditMstSleYardsDto();
            this.mstSleYards.id = mstSleYardsId;
            this.mstSleYards.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleYardsServiceProxy.getMstSleYardsForEdit(mstSleYardsId).subscribe(result => {
                this.mstSleYards = result.mstSleYards;

                if (this.mstSleYards.lastModificationtTime) {
					this.lastModificationtTime = this.mstSleYards.lastModificationtTime.toDate();
                }
                if (this.mstSleYards.deletionTime) {
					this.deletionTime = this.mstSleYards.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationtTime) {
            if (!this.mstSleYards.lastModificationtTime) {
                this.mstSleYards.lastModificationtTime = moment(this.lastModificationtTime).startOf('day');
            }
            else {
                this.mstSleYards.lastModificationtTime = moment(this.lastModificationtTime);
            }
        }
        else {
            this.mstSleYards.lastModificationtTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstSleYards.deletionTime) {
                this.mstSleYards.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstSleYards.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstSleYards.deletionTime = null;
        }
            this._mstSleYardsServiceProxy.createOrEdit(this.mstSleYards)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
