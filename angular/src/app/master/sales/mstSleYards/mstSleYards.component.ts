import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstSleYardsServiceProxy, MstSleYardsDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstSleYardsModalComponent } from './create-or-edit-mstSleYards-modal.component';
import { ViewMstSleYardsModalComponent } from './view-mstSleYards-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './mstSleYards.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls:["./mstSleYards.component.less"],
    animations: [appModuleAnimation()]
})
export class MstSleYardsComponent extends AppComponentBase {

    @ViewChild('createOrEditMstSleYardsModal', { static: true }) createOrEditMstSleYardsModal: CreateOrEditMstSleYardsModalComponent;
    @ViewChild('viewMstSleYardsModalComponent', { static: true }) viewMstSleYardsModal: ViewMstSleYardsModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    codeFilter = '';
    nameFilter = '';
    addressFilter = '';
    statusFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;
    descriptionFilter = '';
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationtTimeFilter : moment.Moment;
		minLastModificationtTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
		minDeletionTimeFilter : moment.Moment;




    constructor(
        injector: Injector,
        private _mstSleYardsServiceProxy: MstSleYardsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstSleYards(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstSleYardsServiceProxy.getAll(
            this.filterText,
            this.codeFilter,
            this.nameFilter,
            this.addressFilter,
            this.statusFilter,
            this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
            this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
            this.descriptionFilter,
            this.maxCreationTimeFilter,
            this.minCreationTimeFilter,
            this.maxLastModificationtTimeFilter,
            this.minLastModificationtTimeFilter,
            this.isDeletedFilter,
            this.maxDeletionTimeFilter,
            this.minDeletionTimeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleYards(): void {
        this.createOrEditMstSleYardsModal.show();
    }

    deleteMstSleYards(mstSleYards: MstSleYardsDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstSleYardsServiceProxy.delete(mstSleYards.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
      
    }
}
