import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstSleYardsForViewDto, MstSleYardsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstSleYardsModal',
    templateUrl: './view-mstSleYards-modal.component.html'
})
export class ViewMstSleYardsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstSleYardsForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstSleYardsForViewDto();
        this.item.mstSleYards = new MstSleYardsDto();
    }

    show(item: GetMstSleYardsForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
