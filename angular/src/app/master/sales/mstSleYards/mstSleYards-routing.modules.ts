import { Routes, RouterModule } from '@angular/router';
import { MstSleYardsComponent } from './mstSleYards.component';
import {CreateOrEditMstSleYardsModalComponent} from './create-or-edit-mstSleYards-modal.component'
import {ViewMstSleYardsModalComponent} from './view-mstSleYards-modal.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstSleYardsComponent,
        data: { permission: 'Pages.MstSleYards' },
    },   {
        path: '',
        component: MstSleYardsComponent,
        data: { permission: 'Pages.MstSleYards.Delete' },
    },
    {
        path: '',
        component: CreateOrEditMstSleYardsModalComponent,
        data: { permission: 'Pages.MstSleYards.Create' },
    },
    {
        path: '',
        component: ViewMstSleYardsModalComponent,
        data: { permission: 'Pages.Pages.MstSleYards' },
    },
    
];

export const MstSleYardsRoutingRoute = RouterModule.forChild(routes);
