import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstSleDealerGroupsForViewDto, MstSleDealerGroupsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstSleDealerGroupsModal',
    templateUrl: './view-mstSleDealerGroups.component.html'
})
export class ViewMstSleDealerGroupsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstSleDealerGroupsForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstSleDealerGroupsForViewDto();
        this.item.mstSleDealerGroups = new MstSleDealerGroupsDto();
    }

    show(item: GetMstSleDealerGroupsForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
