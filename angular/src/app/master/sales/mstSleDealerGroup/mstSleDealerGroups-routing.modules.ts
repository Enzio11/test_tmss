import { Routes, RouterModule } from '@angular/router';
import { MstSleDealerGroupsComponent } from './mstSleDealerGroups.component';
import {CreateOrEditMstSleDealerGroupsModalComponent} from './create-or-edit-mstSleDealerGroups.component'
import {ViewMstSleDealerGroupsModalComponent} from './view-mstSleDealerGroups.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstSleDealerGroupsComponent,
        data: { permission: null },
    }
    
];

export const MstSleDealerGroupsRoutingRoute = RouterModule.forChild(routes);
