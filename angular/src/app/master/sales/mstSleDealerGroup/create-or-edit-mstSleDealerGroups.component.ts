import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleDealerGroupsServiceProxy, CreateOrEditMstSleDealerGroupsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMstSleDealerGroupsModal',
    templateUrl: './create-or-edit-mstSleDealerGroups.component.html',
    styleUrls:["./create-or-edit-mstSleDealerGroups.component.less"]
})
export class CreateOrEditMstSleDealerGroupsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstSleDealerGroups: CreateOrEditMstSleDealerGroupsDto = new CreateOrEditMstSleDealerGroupsDto();

    lastModificationtTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstSleDealerGroupsServiceProxy: MstSleDealerGroupsServiceProxy
    ) {
        super(injector);
    }

    show(mstSleDealerGroupsId?: number): void {
    //this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstSleDealerGroupsId) {
            this.mstSleDealerGroups = new CreateOrEditMstSleDealerGroupsDto();
            this.mstSleDealerGroups.id = mstSleDealerGroupsId;
            //this.mstSleDealerGroups.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleDealerGroupsServiceProxy.getMstSleDealerGroupsForEdit(mstSleDealerGroupsId).subscribe(result => {
                this.mstSleDealerGroups = result.mstSleDealerGroups;

                // if (this.mstSleDealerGroups.lastModificationtTime) {
				// 	this.lastModificationtTime = this.mstSleDealerGroups.lastModificationtTime.toDate();
                // }
                // if (this.mstSleDealerGroups.deletionTime) {
				// 	this.deletionTime = this.mstSleDealerGroups.deletionTime.toDate();
                // }

                this.active = true;
                // this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

            this._mstSleDealerGroupsServiceProxy.createOrEdit(this.mstSleDealerGroups)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
