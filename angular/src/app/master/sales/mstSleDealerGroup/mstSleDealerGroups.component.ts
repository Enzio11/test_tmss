import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstSleDealerGroupsServiceProxy, MstSleDealerGroupsDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstSleDealerGroupsModalComponent } from './create-or-edit-mstSleDealerGroups.component';
import { ViewMstSleDealerGroupsModalComponent } from './view-mstSleDealerGroups.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './mstSleDealerGroups.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MstSleDealerGroupsComponent extends AppComponentBase {

    @ViewChild('createOrEditMstSleDealerGroupsModal', { static: true }) createOrEditMstSleDealerGroupsModal: CreateOrEditMstSleDealerGroupsModalComponent;
    @ViewChild('viewMstSleDealerGroupsModalComponent', { static: true }) viewMstSleDealerGroupsModal: ViewMstSleDealerGroupsModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    groupNameFilter = '';
    descriptionFilter = '';
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationtTimeFilter : moment.Moment;
		minLastModificationtTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
		minDeletionTimeFilter : moment.Moment;
    statusFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;




    constructor(
        injector: Injector,
        private _mstSleDealerGroupsServiceProxy: MstSleDealerGroupsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstSleDealerGroups(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstSleDealerGroupsServiceProxy.getAll(
            this.filterText,
            this.groupNameFilter,
            this.descriptionFilter,
            this.maxCreationTimeFilter,
            this.minCreationTimeFilter,
            this.maxLastModificationtTimeFilter,
            this.minLastModificationtTimeFilter,
            this.isDeletedFilter,
            this.maxDeletionTimeFilter,
            this.minDeletionTimeFilter,
            this.statusFilter,
            this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
            this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleDealerGroups(): void {
        this.createOrEditMstSleDealerGroupsModal.show();
    }

    deleteMstSleDealerGroups(mstSleDealerGroups: MstSleDealerGroupsDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstSleDealerGroupsServiceProxy.delete(mstSleDealerGroups.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
      
    }
}
