import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstGenDealersForViewDto, MstGenDealersDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstGenDealersModal',
    templateUrl: './view-mst-gen-dealer.component.html'
})
export class ViewMstGenDealersModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstGenDealersForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstGenDealersForViewDto();
        this.item.mstGenDealers = new MstGenDealersDto();
    }

    show(item: GetMstGenDealersForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
