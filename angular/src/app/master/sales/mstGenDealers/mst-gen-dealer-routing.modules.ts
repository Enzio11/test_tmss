import { Routes, RouterModule } from '@angular/router';
import { MstGenDealersComponent } from './mst-gen-dealers.component';
import {CreateOrEditMstGenDealersModalComponent} from './create-mst-gen-dealer.component'
import {ViewMstGenDealersModalComponent} from './view-mst-gen-dealer.component'
import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstGenDealersComponent,
        data: { permission: 'Pages.Master.Sales.MstGenDealers' },
    },
];

export const MstGenDealerRoutingRoute = RouterModule.forChild(routes);
