import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstGenDealersServiceProxy, CreateOrEditMstGenDealersDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMstGenDealersModal',
    templateUrl: './creat-mst-gen-dealer.component.html',
    styleUrls:["./create-mst-gen-dealer.component.less"]
})
export class CreateOrEditMstGenDealersModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstGenDealers: CreateOrEditMstGenDealersDto = new CreateOrEditMstGenDealersDto();

    lastModificationTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstGenDealersServiceProxy: MstGenDealersServiceProxy
    ) {
        super(injector);
    }

    show(mstGenDealersId?: number): void {
    this.lastModificationTime = null;
    this.deletionTime = null;

        if (!mstGenDealersId) {
            this.mstGenDealers = new CreateOrEditMstGenDealersDto();
            this.mstGenDealers.id = mstGenDealersId;
            this.mstGenDealers.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstGenDealersServiceProxy.getMstGenDealersForEdit(mstGenDealersId).subscribe(result => {
                this.mstGenDealers = result.mstGenDealers;

                if (this.mstGenDealers.lastModificationTime) {
					this.lastModificationTime = this.mstGenDealers.lastModificationTime.toDate();
                }
                if (this.mstGenDealers.deletionTime) {
					this.deletionTime = this.mstGenDealers.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationTime) {
            if (!this.mstGenDealers.lastModificationTime) {
                this.mstGenDealers.lastModificationTime = moment(this.lastModificationTime).startOf('day');
            }
            else {
                this.mstGenDealers.lastModificationTime = moment(this.lastModificationTime);
            }
        }
        else {
            this.mstGenDealers.lastModificationTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstGenDealers.deletionTime) {
                this.mstGenDealers.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstGenDealers.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstGenDealers.deletionTime = null;
        }
            this._mstGenDealersServiceProxy.createOrEdit(this.mstGenDealers)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
