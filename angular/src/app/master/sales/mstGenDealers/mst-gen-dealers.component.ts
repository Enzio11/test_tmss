import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstGenDealersServiceProxy, MstGenDealersDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstGenDealersModalComponent } from './create-mst-gen-dealer.component';
import { ViewMstGenDealersModalComponent } from './view-mst-gen-dealer.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';


@Component({
    templateUrl: './mst-gen-dealers.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls:["./mst-gen-dealer.component.less"],
    animations: [appModuleAnimation()]
    
})
export class MstGenDealersComponent extends AppComponentBase {

    @ViewChild('createOrEditMstGenDealersModal', { static: true }) createOrEditMstGenDealersModal: CreateOrEditMstGenDealersModalComponent;
    @ViewChild('viewMstGenDealersModalComponent', { static: true }) viewMstGenDealersModal: ViewMstGenDealersModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('dt') dt;



    advancedFiltersAreShown = false;
    filterText = '';
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationTimeFilter : moment.Moment;
		minLastModificationTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
		minDeletionTimeFilter : moment.Moment;
    codeFilter = '';
    accountNoFilter = '';
    bankFilter = '';
    bankAddressFilter = '';
    vnNameFilter = '';
    enNameFilter = '';
    addressFilter = '';
    abbreviationFilter = '';
    contactPersonFilter = '';
    phoneFilter = '';
    statusFilter = '';
    faxFilter = '';
    descriptionFilter = '';
    isSpecialFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;
    taxCodeFilter = '';
    isSumDealerFilter = '';
    biServerFilter = '';
    tfsAmountFilter = '';
    partLeadtimeFilter = '';
    ipAddressFilter = '';
    islexusFilter = '';
    isSellLexusPartFilter = '';
    isDlrSalesFilter = '';
    recievingAddressFilter = '';
    dlrFooterFilter = '';
    isPrintFilter = '';
    passwordSearchVinFilter = '';
    maxPortRegionFilter : number;
		maxPortRegionFilterEmpty : number;
		minPortRegionFilter : number;
        minPortRegionFilterEmpty : number;
        

    mstGenDealer: MstGenDealersDto[];




    constructor(
        injector: Injector,
        private _mstGenDealersServiceProxy: MstGenDealersServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstGenDealers(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstGenDealersServiceProxy.getAll(
            this.filterText,
            this.maxCreationTimeFilter,
            this.minCreationTimeFilter,
            this.maxLastModificationTimeFilter,
            this.minLastModificationTimeFilter,
            this.isDeletedFilter,
            this.maxDeletionTimeFilter,
            this.minDeletionTimeFilter,
            this.codeFilter,
            this.accountNoFilter,
            this.bankFilter,
            this.bankAddressFilter,
            this.vnNameFilter,
            this.enNameFilter,
            this.addressFilter,
            this.abbreviationFilter,
            this.contactPersonFilter,
            this.phoneFilter,
            this.statusFilter,
            this.faxFilter,
            this.descriptionFilter,
            this.isSpecialFilter,
            this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
            this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
            this.taxCodeFilter,
            this.isSumDealerFilter,
            this.biServerFilter,
            this.tfsAmountFilter,
            this.partLeadtimeFilter,
            this.ipAddressFilter,
            this.islexusFilter,
            this.isSellLexusPartFilter,
            this.isDlrSalesFilter,
            this.recievingAddressFilter,
            this.dlrFooterFilter,
            this.isPrintFilter,
            this.passwordSearchVinFilter,
            this.maxPortRegionFilter == null ? this.maxPortRegionFilterEmpty: this.maxPortRegionFilter,
            this.minPortRegionFilter == null ? this.minPortRegionFilterEmpty: this.minPortRegionFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstGenDealers(): void {
        this.createOrEditMstGenDealersModal.show();
    }

    deleteMstGenDealers(mstGenDealers: MstGenDealersDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstGenDealersServiceProxy.delete(mstGenDealers.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
       
    }
}
