import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
    MstSleColorsServiceProxy,
    MstSleColorsDto,
} from "@shared/service-proxies/service-proxies";
import { NotifyService } from "abp-ng2-module";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TokenAuthServiceProxy } from "@shared/service-proxies/service-proxies";
import { CreateOrEditMstSleColorsModalComponent } from "./create-or-edit-mstSleColors-modal.component";
import { ViewMstSleColorsModalComponent } from "./view-mstSleColors-modal.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/public_api";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import * as moment from "moment";

@Component({
    templateUrl: "./mstSleColors.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ["./mstSleColors.component.less"],
    animations: [appModuleAnimation()],
})
export class MstSleColorsComponent extends AppComponentBase {
    @ViewChild("createOrEditMstSleColorsModal", { static: true })
    createOrEditMstSleColorsModal: CreateOrEditMstSleColorsModalComponent;
    @ViewChild("viewMstSleColorsModalComponent", { static: true })
    viewMstSleColorsModal: ViewMstSleColorsModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    codeFilter = "";
    vnNameFilter = "";
    enNameFilter = "";
    descriptionFilter = "";
    statusFilter = "";
    maxOrderingFilter: number;
    maxOrderingFilterEmpty: number;
    minOrderingFilter: number;
    minOrderingFilterEmpty: number;
    maxOrderingRptFilter: number;
    maxOrderingRptFilterEmpty: number;
    minOrderingRptFilter: number;
    minOrderingRptFilterEmpty: number;
    maxCreationTimeFilter: moment.Moment;
    minCreationTimeFilter: moment.Moment;
    maxLastModificationtTimeFilter: moment.Moment;
    minLastModificationtTimeFilter: moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter: moment.Moment;
    minDeletionTimeFilter: moment.Moment;

    constructor(
        injector: Injector,
        private _mstSleColorsServiceProxy: MstSleColorsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstSleColors(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstSleColorsServiceProxy
            .getAll(
                this.filterText,
                this.codeFilter,
                this.vnNameFilter,
                this.enNameFilter,
                this.descriptionFilter,
                this.statusFilter,
                this.maxOrderingFilter == null
                    ? this.maxOrderingFilterEmpty
                    : this.maxOrderingFilter,
                this.minOrderingFilter == null
                    ? this.minOrderingFilterEmpty
                    : this.minOrderingFilter,
                this.maxOrderingRptFilter == null
                    ? this.maxOrderingRptFilterEmpty
                    : this.maxOrderingRptFilter,
                this.minOrderingRptFilter == null
                    ? this.minOrderingRptFilterEmpty
                    : this.minOrderingRptFilter,
                this.maxCreationTimeFilter,
                this.minCreationTimeFilter,
                this.maxLastModificationtTimeFilter,
                this.minLastModificationtTimeFilter,
                this.isDeletedFilter,
                this.maxDeletionTimeFilter,
                this.minDeletionTimeFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleColors(): void {
        this.createOrEditMstSleColorsModal.show();
    }

    deleteMstSleColors(mstSleColors: MstSleColorsDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleColorsServiceProxy
                    .delete(mstSleColors.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    exportToExcel(): void {}
}
