import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleColorsServiceProxy, CreateOrEditMstSleColorsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMstSleColorsModal',
    templateUrl: './create-or-edit-mstSleColors-modal.component.html',
    styleUrls:["./create-or-edit-mstSleColors-modal.component.less"]
})
export class CreateOrEditMstSleColorsModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstSleColors: CreateOrEditMstSleColorsDto = new CreateOrEditMstSleColorsDto();

    lastModificationtTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstSleColorsServiceProxy: MstSleColorsServiceProxy
    ) {
        super(injector);
    }

    show(mstSleColorsId?: number): void {
    this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstSleColorsId) {
            this.mstSleColors = new CreateOrEditMstSleColorsDto();
            this.mstSleColors.id = mstSleColorsId;
            this.mstSleColors.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleColorsServiceProxy.getMstSleColorsForEdit(mstSleColorsId).subscribe(result => {
                this.mstSleColors = result.mstSleColors;

                if (this.mstSleColors.lastModificationtTime) {
					this.lastModificationtTime = this.mstSleColors.lastModificationtTime.toDate();
                }
                if (this.mstSleColors.deletionTime) {
					this.deletionTime = this.mstSleColors.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationtTime) {
            if (!this.mstSleColors.lastModificationtTime) {
                this.mstSleColors.lastModificationtTime = moment(this.lastModificationtTime).startOf('day');
            }
            else {
                this.mstSleColors.lastModificationtTime = moment(this.lastModificationtTime);
            }
        }
        else {
            this.mstSleColors.lastModificationtTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstSleColors.deletionTime) {
                this.mstSleColors.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstSleColors.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstSleColors.deletionTime = null;
        }
            this._mstSleColorsServiceProxy.createOrEdit(this.mstSleColors)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
