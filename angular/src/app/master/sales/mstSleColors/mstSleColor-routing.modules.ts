import { Routes, RouterModule } from '@angular/router';
import { MstSleColorsComponent } from './mstSleColors.component';
import {CreateOrEditMstSleColorsModalComponent} from './create-or-edit-mstSleColors-modal.component'
import {ViewMstSleColorsModalComponent} from './view-mstSleColors-modal.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstSleColorsComponent,
        data: { permission: 'Pages.MstSleColors' },
    },
    {
        path: '',
        component: CreateOrEditMstSleColorsModalComponent,
        data: { permission: 'Pages.MstSleColors.Create' },
    },
    {
        path: '',
        component: ViewMstSleColorsModalComponent,
        data: { permission: 'Pages.Pages.MstSleColors' },
    },
    
];

export const MstSleColorsRoutingRoute = RouterModule.forChild(routes);
