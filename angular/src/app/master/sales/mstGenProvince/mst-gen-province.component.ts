import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstGenProvincesServiceProxy, MstGenProvinceDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstGenProvincesModalComponent } from './create-or-edit-mstGenProvinces-modal.component';
import { ViewMstGenProvincesModalComponent } from './view-mstGenProvinces-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './mst-gen-province.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MstGenProvincesComponent extends AppComponentBase {

    @ViewChild('createOrEditMstGenProvincesModal', { static: true }) createOrEditMstGenProvincesModal: CreateOrEditMstGenProvincesModalComponent;
    @ViewChild('viewMstGenProvincesModalComponent', { static: true }) viewMstGenProvincesModal: ViewMstGenProvincesModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    codeFilter = '';
    nameFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;
    statusFilter = '';
    maxPopulationAmountFilter : number;
		maxPopulationAmountFilterEmpty : number;
		minPopulationAmountFilter : number;
		minPopulationAmountFilterEmpty : number;
    maxSquareAmountFilter : number;
		maxSquareAmountFilterEmpty : number;
		minSquareAmountFilter : number;
		minSquareAmountFilterEmpty : number;
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationtTimeFilter : moment.Moment;
		minLastModificationtTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
		minDeletionTimeFilter : moment.Moment;
        subRegionIdFilter:number;



    constructor(
        injector: Injector,
        private _mstGenProvincesServiceProxy: MstGenProvincesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstGenProvinces(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstGenProvincesServiceProxy.getAll(
            this.filterText,
            this.codeFilter,
            this.nameFilter,
            this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
            this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
            this.statusFilter,
            this.subRegionIdFilter,
            this.maxPopulationAmountFilter == null ? this.maxPopulationAmountFilterEmpty: this.maxPopulationAmountFilter,
            this.minPopulationAmountFilter == null ? this.minPopulationAmountFilterEmpty: this.minPopulationAmountFilter,
            this.maxSquareAmountFilter == null ? this.maxSquareAmountFilterEmpty: this.maxSquareAmountFilter,
            this.minSquareAmountFilter == null ? this.minSquareAmountFilterEmpty: this.minSquareAmountFilter,
            this.maxCreationTimeFilter,
            this.minCreationTimeFilter,
            this.maxLastModificationtTimeFilter,
            this.minLastModificationtTimeFilter,
            this.isDeletedFilter,
            this.maxDeletionTimeFilter,
            this.minDeletionTimeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstGenProvinces(): void {
        this.createOrEditMstGenProvincesModal.show();
    }

    deleteMstGenProvinces(mstGenProvinces: MstGenProvinceDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstGenProvincesServiceProxy.delete(mstGenProvinces.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
    }
}
