import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstGenProvincesForViewDto, MstGenProvinceDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstGenProvincesModal',
    templateUrl: './view-mstGenProvinces-modal.component.html'
})
export class ViewMstGenProvincesModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstGenProvincesForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstGenProvincesForViewDto();
        this.item.mstGenProvinces = new MstGenProvinceDto();
    }

    show(item: GetMstGenProvincesForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
