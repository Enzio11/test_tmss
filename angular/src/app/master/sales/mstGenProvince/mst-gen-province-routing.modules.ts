import { Routes, RouterModule } from '@angular/router';
import { MstGenProvincesComponent } from './mst-gen-province.component';
import {CreateOrEditMstGenProvincesModalComponent} from './create-or-edit-mstGenProvinces-modal.component'
import {ViewMstGenProvincesModalComponent} from './view-mstGenProvinces-modal.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstGenProvincesComponent,
        data: { permission: 'Pages.MstGenProvinces' },
    },
    {
        path: '',
        component: CreateOrEditMstGenProvincesModalComponent,
        data: { permission: 'Pages.MstGenProvinces.Create' },
    },
    {
        path: '',
        component: ViewMstGenProvincesModalComponent,
        data: { permission: 'Pages.MstGenProvinces' },
    },
    
];

export const MstGenProvinceRoutingRoute = RouterModule.forChild(routes);
