import { Component, ViewChild, Injector, Output, EventEmitter,OnInit} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstGenProvincesServiceProxy, CreateOrEditMstGenProvinceDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';



@Component({
    selector: 'createOrEditMstGenProvincesModal',
    templateUrl: './create-or-edit-mstGenProvinces-modal.component.html',
    styleUrls:['./create-or-edit-mstGenprovinces-modal.component.less']
})
export class CreateOrEditMstGenProvincesModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstGenProvinces: CreateOrEditMstGenProvinceDto = new CreateOrEditMstGenProvinceDto();

    lastModificationtTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstGenProvincesServiceProxy: MstGenProvincesServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(){

    }

    show(mstGenProvincesId?: number): void {
    this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstGenProvincesId) {
            this.mstGenProvinces = new CreateOrEditMstGenProvinceDto();
            this.mstGenProvinces.id = mstGenProvincesId;
            this.mstGenProvinces.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstGenProvincesServiceProxy.getMstGenProvincesForEdit(mstGenProvincesId).subscribe(result => {
                this.mstGenProvinces = result.mstGenProvinces;

                if (this.mstGenProvinces.lastModificationtTime) {
					this.lastModificationtTime = this.mstGenProvinces.lastModificationtTime.toDate();
                }
                if (this.mstGenProvinces.deletionTime) {
					this.deletionTime = this.mstGenProvinces.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationtTime) {
            if (!this.mstGenProvinces.lastModificationtTime) {
                this.mstGenProvinces.lastModificationtTime = moment(this.lastModificationtTime).startOf('day');
            }
            else {
                this.mstGenProvinces.lastModificationtTime = moment(this.lastModificationtTime);
            }
        }
        else {
            this.mstGenProvinces.lastModificationtTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstGenProvinces.deletionTime) {
                this.mstGenProvinces.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstGenProvinces.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstGenProvinces.deletionTime = null;
        }
            this._mstGenProvincesServiceProxy.createOrEdit(this.mstGenProvinces)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
