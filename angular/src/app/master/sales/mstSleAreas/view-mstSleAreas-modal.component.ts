import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstSleAreasForViewDto, MstSleAreasDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstSleAreasModal',
    templateUrl: './view-mstSleAreas-modal.component.html'
})
export class ViewMstSleAreasModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstSleAreasForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstSleAreasForViewDto();
        this.item.mstSleAreas = new MstSleAreasDto();
    }

    show(item: GetMstSleAreasForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
