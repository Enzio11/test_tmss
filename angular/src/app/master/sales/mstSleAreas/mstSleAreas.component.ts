import { Component, Injector, ViewEncapsulation, ViewChild,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstSleAreasServiceProxy, MstSleAreasDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstSleAreasModalComponent } from './create-or-edit-mstSleAreas-modal.component';
import { ViewMstSleAreasModalComponent } from './view-mstSleAreas-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { MstSleYardsServiceProxy, MstSleYardsDto  } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './mstSleAreas.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls:["./mstSleAreas.component.less"],
    animations: [appModuleAnimation()],
    template:' <createOrEditMstSleAreasModal #createOrEditMstSleAreasModal [yardId]="yardId" ></createOrEditMstSleAreasModal>',
})
export class MstSleAreasComponent extends AppComponentBase {

    @ViewChild('createOrEditMstSleAreasModal', { static: true }) createOrEditMstSleAreasModal: CreateOrEditMstSleAreasModalComponent;
    @ViewChild('viewMstSleAreasModalComponent', { static: true }) viewMstSleAreasModal: ViewMstSleAreasModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    statusFilter = '';
    descriptionFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;
    isNoneAssignmentFilter = '';
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationtTimeFilter : moment.Moment;
		minLastModificationtTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
        minDeletionTimeFilter : moment.Moment;
        yardId:number;
        

        // Yard
        advancedFiltersYardShown = false;
    filterYardText = '';
    codeYardFilter = '';
    nameYardFilter = '';
    addressYardFilter = '';
    statusYardFilter = '';
    maxOrderingYardFilter : number;
		maxOrderingYardFilterEmpty : number;
		minOrderingYardFilter : number;
		minOrderingYardFilterEmpty : number;
    descriptionYardFilter = '';
    maxCreationTimeYardFilter : moment.Moment;
		minCreationTimeYardFilter : moment.Moment;
    maxLastModificationtTimeYardFilter : moment.Moment;
		minLastModificationtTimeYardFilter : moment.Moment;
    isDeletedYardFilter = -1;
    maxDeletionTimeYardFilter : moment.Moment;
        minDeletionTimeYardFilter : moment.Moment;
        
    selectYard: MstSleYardsDto = new MstSleYardsDto();

        totalRecordsYardCount =0;
        recordsYard;

        totalRecordsCount =0;
        records:any[];





    constructor(
        injector: Injector,
        private _mstSleAreasServiceProxy: MstSleAreasServiceProxy,
        private _mstSleYardsServiceProxy: MstSleYardsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }
    
    ngOnInit(){
        this.getMstSleYards();
    }

    getMstSleYards() {
        this._mstSleYardsServiceProxy.getAllYard(
        ).subscribe(resultYard => {
            this.recordsYard = resultYard;
        });
    }

    // getMstSleAreas(event?: LazyLoadEvent) {
    //     this._mstSleAreasServiceProxy.getAll(
    //         this.filterText,
    //         this.nameFilter,
    //         this.statusFilter,
    //         this.descriptionFilter,
    //         this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
    //         this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
    //         this.yardId,
    //         this.isNoneAssignmentFilter,
    //         this.maxCreationTimeFilter,
    //         this.minCreationTimeFilter,
    //         this.maxLastModificationtTimeFilter,
    //         this.minLastModificationtTimeFilter,
    //         this.isDeletedFilter,
    //         this.maxDeletionTimeFilter,
    //         this.minDeletionTimeFilter,
    //         '',
    //         0,
    //         10
    //     ).subscribe(result => {
    //         this.totalRecordsCount = result.totalCount;
    //         this.records = result.items;
    //     });
    // }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleAreas(): void {
        this.createOrEditMstSleAreasModal.show();
    }

    deleteMstSleAreas(mstSleAreas: MstSleAreasDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstSleAreasServiceProxy.delete(mstSleAreas.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
    getAreasByYardId(selectYard : number){
        this._mstSleAreasServiceProxy.getAreasByYardId( selectYard).subscribe(result => this.records = result.items);
        this.yardId = this.selectYard.id;
    }
    onRowSelected(event) {
        this.selectYard = event.data;

        this.getAreasByYardId(this.selectYard.id);
      }
    

    exportToExcel(): void {
      
    }
}
