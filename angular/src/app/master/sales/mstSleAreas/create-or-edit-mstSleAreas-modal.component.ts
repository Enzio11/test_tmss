import { Component, ViewChild, Injector, Output, EventEmitter, Input} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleAreasServiceProxy, CreateOrEditMstSleAreasDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { colorSets } from '@swimlane/ngx-charts';

@Component({
    selector: 'createOrEditMstSleAreasModal',
    templateUrl: './create-or-edit-mstSleAreas-modal.component.html',
    styleUrls:["./create-or-edit-mstSleAreas-modal.component.less"],
})
export class CreateOrEditMstSleAreasModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() yardID :number;

    active = false;
    saving = false;

    mstSleAreas: CreateOrEditMstSleAreasDto = new CreateOrEditMstSleAreasDto();

    lastModificationtTime: Date;
    deletionTime: Date;

    

    constructor(
        injector: Injector,
        private _mstSleAreasServiceProxy: MstSleAreasServiceProxy
    ) {
        super(injector);
    }

    show(mstSleAreasId?: number): void {
    this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstSleAreasId) {
            this.mstSleAreas = new CreateOrEditMstSleAreasDto();
            this.mstSleAreas.id = mstSleAreasId;
            this.mstSleAreas.creationTime = moment().startOf('day');
            this.mstSleAreas.yardId = this.yardID;

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleAreasServiceProxy.getMstSleAreasForEdit(mstSleAreasId).subscribe(result => {
                this.mstSleAreas = result.mstSleAreas;

                if (this.mstSleAreas.lastModificationtTime) {
					this.lastModificationtTime = this.mstSleAreas.lastModificationtTime.toDate();
                }
                if (this.mstSleAreas.deletionTime) {
					this.deletionTime = this.mstSleAreas.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationtTime) {
            if (!this.mstSleAreas.lastModificationtTime) {
                this.mstSleAreas.lastModificationtTime = moment(this.lastModificationtTime).startOf('day');
            }
            else {
                this.mstSleAreas.lastModificationtTime = moment(this.lastModificationtTime);
            }
        }
        else {
            this.mstSleAreas.lastModificationtTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstSleAreas.deletionTime) {
                this.mstSleAreas.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstSleAreas.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstSleAreas.deletionTime = null;
        }
            this._mstSleAreasServiceProxy.createOrEdit(this.mstSleAreas)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
