import { Routes, RouterModule } from '@angular/router';
import { MstSleAreasComponent } from './mstSleAreas.component';
import {CreateOrEditMstSleAreasModalComponent} from './create-or-edit-mstSleAreas-modal.component'
import {ViewMstSleAreasModalComponent} from './view-mstSleAreas-modal.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstSleAreasComponent,
        data: { permission: 'Pages.MstSleAreas' },
    },
    {
        path: '',
        component: CreateOrEditMstSleAreasModalComponent,
        data: { permission: 'Pages.MstSleAreas.Create' },
    },
    {
        path: '',
        component: ViewMstSleAreasModalComponent,
        data: { permission: 'Pages.MstSleAreas' },
    },
    
];

export const MstSleAreasRoutingRoute = RouterModule.forChild(routes);
