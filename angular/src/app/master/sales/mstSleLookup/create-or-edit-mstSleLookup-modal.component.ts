import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleLookupServiceProxy, CreateOrEditMstSleLookupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditMstSleLookupModal',
    templateUrl: './create-or-edit-mstSleLookup-modal.component.html',
    styleUrls:["./create-or-edit-mstSleLookup-modal.component.less"]
})
export class CreateOrEditMstSleLookupModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    mstSleLookup: CreateOrEditMstSleLookupDto = new CreateOrEditMstSleLookupDto();

    lastModificationtTime: Date;
    deletionTime: Date;


    constructor(
        injector: Injector,
        private _mstSleLookupServiceProxy: MstSleLookupServiceProxy
    ) {
        super(injector);
    }

    show(mstSleLookupId?: number): void {
    this.lastModificationtTime = null;
    this.deletionTime = null;

        if (!mstSleLookupId) {
            this.mstSleLookup = new CreateOrEditMstSleLookupDto();
            this.mstSleLookup.id = mstSleLookupId;
            this.mstSleLookup.creationTime = moment().startOf('day');

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleLookupServiceProxy.getMstSleLookupForEdit(mstSleLookupId).subscribe(result => {
                this.mstSleLookup = result.mstSleLookup;

                if (this.mstSleLookup.lastModificationtTime) {
					this.lastModificationtTime = this.mstSleLookup.lastModificationtTime.toDate();
                }
                if (this.mstSleLookup.deletionTime) {
					this.deletionTime = this.mstSleLookup.deletionTime.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastModificationtTime) {
            if (!this.mstSleLookup.lastModificationtTime) {
                this.mstSleLookup.lastModificationtTime = moment(this.lastModificationtTime).startOf('day');
            }
            else {
                this.mstSleLookup.lastModificationtTime = moment(this.lastModificationtTime);
            }
        }
        else {
            this.mstSleLookup.lastModificationtTime = null;
        }
        if (this.deletionTime) {
            if (!this.mstSleLookup.deletionTime) {
                this.mstSleLookup.deletionTime = moment(this.deletionTime).startOf('day');
            }
            else {
                this.mstSleLookup.deletionTime = moment(this.deletionTime);
            }
        }
        else {
            this.mstSleLookup.deletionTime = null;
        }
            this._mstSleLookupServiceProxy.createOrEdit(this.mstSleLookup)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
