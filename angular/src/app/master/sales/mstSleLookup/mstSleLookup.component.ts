import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MstSleLookupServiceProxy, MstSleLookupDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditMstSleLookupModalComponent } from './create-or-edit-mstSleLookup-modal.component';
import { ViewMstSleLookupModalComponent } from './view-mstSleLookup-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './mstSleLookup.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls:["./mstSleLookup.component.less"],
    animations: [appModuleAnimation()]
})
export class MstSleLookupComponent extends AppComponentBase {

    @ViewChild('createOrEditMstSleLookupModal', { static: true }) createOrEditMstSleLookupModal: CreateOrEditMstSleLookupModalComponent;
    @ViewChild('viewMstSleLookupModalComponent', { static: true }) viewMstSleLookupModal: ViewMstSleLookupModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    codeFilter = '';
    nameFilter = '';
    statusFilter = '';
    descriptionFilter = '';
    maxOrderingFilter : number;
		maxOrderingFilterEmpty : number;
		minOrderingFilter : number;
		minOrderingFilterEmpty : number;
    valueFilter = '';
    maxCreationTimeFilter : moment.Moment;
		minCreationTimeFilter : moment.Moment;
    maxLastModificationtTimeFilter : moment.Moment;
		minLastModificationtTimeFilter : moment.Moment;
    isDeletedFilter = -1;
    maxDeletionTimeFilter : moment.Moment;
		minDeletionTimeFilter : moment.Moment;




    constructor(
        injector: Injector,
        private _mstSleLookupServiceProxy: MstSleLookupServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getMstSleLookup(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._mstSleLookupServiceProxy.getAll(
            this.filterText,
            this.codeFilter,
            this.nameFilter,
            this.statusFilter,
            this.descriptionFilter,
            this.maxOrderingFilter == null ? this.maxOrderingFilterEmpty: this.maxOrderingFilter,
            this.minOrderingFilter == null ? this.minOrderingFilterEmpty: this.minOrderingFilter,
            this.valueFilter,
            this.maxCreationTimeFilter,
            this.minCreationTimeFilter,
            this.maxLastModificationtTimeFilter,
            this.minLastModificationtTimeFilter,
            this.isDeletedFilter,
            this.maxDeletionTimeFilter,
            this.minDeletionTimeFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createMstSleLookup(): void {
        this.createOrEditMstSleLookupModal.show();
    }

    deleteMstSleLookup(mstSleLookup: MstSleLookupDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._mstSleLookupServiceProxy.delete(mstSleLookup.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
       
    }
}
