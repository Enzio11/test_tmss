import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ProgressBarModule } from 'primeng/progressbar';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { UtilsModule } from '@shared/utils/utils.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { MstSleLookupComponent } from './mstSleLookup.component';
import {MstSleLookupRoutingRoute} from './mstSleLookup-routing.modules';
import {CreateOrEditMstSleLookupModalComponent} from './create-or-edit-mstSleLookup-modal.component';
import {ViewMstSleLookupModalComponent} from './view-mstSleLookup-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    PaginatorModule,
    ProgressBarModule,
    HttpClientModule,
    HttpClientJsonpModule,
    MstSleLookupRoutingRoute,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    BsDatepickerModule.forRoot(),
    // AbpModule,
    UtilsModule,
    AppCommonModule
  ],
  declarations: [
   MstSleLookupComponent,
   CreateOrEditMstSleLookupModalComponent,
   ViewMstSleLookupModalComponent
  ]
})
export class MstSleLookupModule { }
