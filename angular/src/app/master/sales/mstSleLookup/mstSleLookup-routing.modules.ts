import { Routes, RouterModule } from '@angular/router';
import { MstSleLookupComponent } from './mstSleLookup.component';
import {CreateOrEditMstSleLookupModalComponent} from './create-or-edit-mstSleLookup-modal.component'
import {ViewMstSleLookupModalComponent} from './view-mstSleLookup-modal.component'


import { from } from 'rxjs';

const routes: Routes = [
    {
        path: '',
        component: MstSleLookupComponent,
        data: { permission: 'Pages.MstSleLookup' },
    },
    {
        path: '',
        component: CreateOrEditMstSleLookupModalComponent,
        data: { permission: 'Pages.MstSleLookup.Create' },
    },
    {
        path: '',
        component: ViewMstSleLookupModalComponent,
        data: { permission: 'Pages.MstSleLookup' },
    },
    
];

export const MstSleLookupRoutingRoute = RouterModule.forChild(routes);
