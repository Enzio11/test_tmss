import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetMstSleLookupForViewDto, MstSleLookupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewMstSleLookupModal',
    templateUrl: './view-mstSleLookup-modal.component.html'
})
export class ViewMstSleLookupModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetMstSleLookupForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetMstSleLookupForViewDto();
        this.item.mstSleLookup = new MstSleLookupDto();
    }

    show(item: GetMstSleLookupForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
