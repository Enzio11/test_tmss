import { SupplierComponent } from './supplier.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: SupplierComponent,
        data: { permission: 'Pages.Master.Services.Supplier' },
    },
];

export const SupplierRoutingRoutes = RouterModule.forChild(routes);
