import { SupplierDto, MstSrvSupplierServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import { ViewSupplierModalComponent } from './view-supplier-modal.component';
import { CreateOrEditSupplierModalComponent } from './create-or-edit-supplier-modal.component';

@Component({
  templateUrl: './supplier.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true })
  createOrEditModal: CreateOrEditSupplierModalComponent;
  @ViewChild('viewModal', { static: true })
  viewModal: ViewSupplierModalComponent;
  @ViewChild('entityTypeHistoryModal', { static: true })
  entityTypeHistoryModal: EntityTypeHistoryModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.Supplier';
  entityHistoryEnabled = false;

  advancedFiltersAreShown = false;
  filterText = '';

  constructor(
    injector: Injector,
    private _serviceProxy: MstSrvSupplierServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
  }

  private setIsEntityHistoryEnabled(): boolean {
    let customSettings = (abp as any).custom;
    return (
      customSettings.EntityHistory &&
      customSettings.EntityHistory.isEnabled &&
      _.filter(
        customSettings.EntityHistory.enabledEntities,
        (entityType) => entityType === this._entityTypeFullName
      ).length === 1
    );
  }

  getSuppliers(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();

    this._serviceProxy
      .getAll(
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  createSupplier(): void {
    this.createOrEditModal.show();
  }

  showHistory(bank: SupplierDto): void {
    this.entityTypeHistoryModal.show({
      entityId: bank.id.toString(),
      entityTypeFullName: this._entityTypeFullName,
      entityTypeDescription: '',
    });
  }

  deleteSupplier(supplier: SupplierDto): void {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
      if (isConfirmed) {
        this._serviceProxy
          .delete(supplier.id)
          .subscribe(() => {
            this.reloadPage();
            this.notify.success(this.l('SuccessfullyDeleted'));
          });
      }
    });
  }
}
