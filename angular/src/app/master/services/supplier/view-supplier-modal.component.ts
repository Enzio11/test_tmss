import { GetSupplierForViewDto, SupplierDto } from './../../../../shared/service-proxies/service-proxies';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
// import { ModalDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewSupplierModal',
    templateUrl: './view-supplier-modal.component.html'
})

export class ViewSupplierModalComponent extends AppComponentBase {

    @ViewChild('viewModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetSupplierForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetSupplierForViewDto();
        this.item.supplier = new SupplierDto();
    }

    show(item: GetSupplierForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
