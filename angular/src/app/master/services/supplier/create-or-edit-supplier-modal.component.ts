import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from '@angular/core';
// import { ModalDirective } from "ngx-bootstrap";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    CreateOrEditSupplierDto,
    MstSrvSupplierServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditSupplierModal',
    templateUrl: './create-or-edit-supplier-modal.component.html',
})

export class CreateOrEditSupplierModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    supplier: CreateOrEditSupplierDto = new CreateOrEditSupplierDto();

    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvSupplierServiceProxy
    ) {
        super(injector);
    }

    show(supplierId?: number): void {
        if (!supplierId) {
            this.supplier = new CreateOrEditSupplierDto();
            this.supplier.id = supplierId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getBankForEdit(supplierId)
                .subscribe((result) => {
                    this.supplier = result.supplier;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._serviceProxy
            .createOrEdit(this.supplier)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
