import { FloorDto } from './../../../../shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ViewFloorModalComponent } from './view-floor-modal.component';
import { CreateOrEditFloorModalComponent } from './create-or-edit-floor-modal.component';
import { Component, ViewChild, Injector } from '@angular/core';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { MstSrvFloorServiceProxy, TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { LazyLoadEvent } from 'primeng/public_api';

@Component({
  selector: 'app-floor',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.less']
})
export class FloorComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true })
  createOrEditModal: CreateOrEditFloorModalComponent;
  @ViewChild('viewModal', { static: true })
  viewModal: ViewFloorModalComponent;
  @ViewChild('entityTypeHistoryModal', { static: true })
  entityTypeHistoryModal: EntityTypeHistoryModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.Floor';
  entityHistoryEnabled = false;

  constructor(
    injector: Injector,
    private _serviceProxy: MstSrvFloorServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
  }

  private setIsEntityHistoryEnabled(): boolean {
    let customSettings = (abp as any).custom;
    return (
        customSettings.EntityHistory &&
        customSettings.EntityHistory.isEnabled &&
        _.filter(
            customSettings.EntityHistory.enabledEntities,
            (entityType) => entityType === this._entityTypeFullName
        ).length === 1
    );
  }

  getFloors(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;    
    }

  this.primengTableHelper.showLoadingIndicator();

  this._serviceProxy
      .getAll(
          this.primengTableHelper.getSorting(this.dataTable),
          this.primengTableHelper.getSkipCount(this.paginator, event),
          this.primengTableHelper.getMaxResultCount(this.paginator, event)
      )
      .subscribe((result) => {
          this.primengTableHelper.totalRecordsCount = result.totalCount;
          this.primengTableHelper.records = result.items;
          this.primengTableHelper.hideLoadingIndicator();
      });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  createFloor(): void {
    this.createOrEditModal.show();
  }

  showHistory(floor: FloorDto): void {
    this.entityTypeHistoryModal.show({
        entityId: floor.id.toString(),
        entityTypeFullName: this._entityTypeFullName,
        entityTypeDescription: '',
    });
  }
}
