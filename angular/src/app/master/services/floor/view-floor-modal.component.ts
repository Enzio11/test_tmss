import { GetFloorForViewDto, FloorDto } from './../../../../shared/service-proxies/service-proxies';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
// import { ModalDirective } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewFloorModal',
    templateUrl: './view-floor-modal.component.html'
})
export class ViewFloorModalComponent extends AppComponentBase {

    @ViewChild('viewModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetFloorForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetFloorForViewDto();
        this.item.floor = new FloorDto();
    }

    show(item: GetFloorForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
