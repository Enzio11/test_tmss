import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from '@angular/core';
// import { ModalDirective } from "ngx-bootstrap";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    CreateOrEditFloorDto,
    MstSrvFloorServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditFloorModal',
    templateUrl: './create-or-edit-floor-modal.component.html',
    styleUrls: ['./floor.component.less']
})
export class CreateOrEditFloorModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    typeJobsList = [
        { id: '1', value: 'GJ + MA' },
        { id: '2', value: 'BP' }
    ];

    statusList = [
        { id: 'Y', value: 'Còn hiệu lực' },
        { id: 'N', value: 'Hết hiệu lực' },
    ];

    active = false;
    saving = false;

    floor: CreateOrEditFloorDto = new CreateOrEditFloorDto();

    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvFloorServiceProxy
    ) {
        super(injector);
    }

    show(floorId?: number): void {
        if (!floorId) {
            this.floor = new CreateOrEditFloorDto();
            this.floor.id = floorId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getFloorForEdit(floorId)
                .subscribe((result) => {
                    this.floor = result.floor;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._serviceProxy
            .createOrEdit(this.floor)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
 