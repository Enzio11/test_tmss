import { FloorComponent } from './floor.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: FloorComponent,
        data: { permission: 'Pages.Master.Services.Floor' },
    },
];

export const FloorRoutingRoutes = RouterModule.forChild(routes);
