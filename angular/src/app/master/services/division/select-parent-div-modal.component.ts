import { Table } from 'primeng/table';
import { DivisionDto, CreateOrEditDivisionDto } from './../../../../shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng/api';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { MstSrvDivisionServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'selectParentDivModal',
    templateUrl: './select-parent-div-modal.component.html',
    styleUrls: ['./division.component.less']
})

export class SelectParentDivModalComponent extends AppComponentBase {
    @ViewChild('selectModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    saving;
    active;
    division;
    selectedDivs;

    divisions: DivisionDto[];
    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvDivisionServiceProxy
    ) {
        super(injector);
    }

    show(divisionList: DivisionDto[], divId?: number, ): void {
        this.divisions = divisionList;
        if (!divId) {
            this.division = new CreateOrEditDivisionDto();
            this.division.id = divId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getDivisionForEdit(divId)
                .subscribe((result) => {
                    this.division = result.division;
                    this.selectedDivs = result.division;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this.division.parentDivId = this.selectedDivs.id;
        this._serviceProxy
            .createOrEdit(this.division)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    delete(divId) {
        this._serviceProxy.delete(divId);
    }
}