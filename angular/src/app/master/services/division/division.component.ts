import { SelectParentDivModalComponent } from './select-parent-div-modal.component';
import { DivisionDto } from './../../../../shared/service-proxies/service-proxies';
import { TreeNode } from 'primeng/api';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, ViewEncapsulation, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { MstSrvDivisionServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './division.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
  styleUrls: ['./division.component.less']
})
export class DivisionComponent extends AppComponentBase {

  lazyFiles: [];
  saving;

  division: DivisionDto = new DivisionDto();
  divisions: TreeNode[];
  divisionList: DivisionDto[];
  advancedFiltersAreShown = true;
  @ViewChild('selectModal', { static: true })
  selectModal: SelectParentDivModalComponent;
  @ViewChild('divisionTree', {static: true}) divisionTree;

  _entityTypeFullName = 'tmss.Core.Master.Services.Division';
  entityHistoryEnabled = false;

  constructor(
    injector: Injector,
    private _serviceProxy: MstSrvDivisionServiceProxy,
  ) { 
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
    this.getDivList();
    console.log(this.division);
  }

  getDivList() {
    this.division = new DivisionDto();
    this._serviceProxy.getAll(
      
    ).subscribe(res => {
      this.divisionList = res;
      this.divisions =  this.setChildren(res || []);
    });
  }

  setChildren(sourceData, parentDivId?) {
    return (parentDivId
      ? sourceData.filter(res => res.parentDivId === parentDivId)
      : sourceData.filter(res => !res.parentDivId))
      .map(res => {
        res.label = res['divName'];
        return Object.assign({}, res, {
          children: this.setChildren(sourceData, res.id),
        });
      });
  }

  selectParentDiv(divId) {
    this.selectModal.show(this.divisionList, divId);
  }

  onNodeSelect(event) {
    this.advancedFiltersAreShown = true;
    this.division = event.node;
  }

  save() {
    this.saving = true;
    this._serviceProxy
            .createOrEdit(this.division)
            .pipe(
                finalize(() => {
                    this.saving = false;
                    this.getDivList();
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('DeletedSuccessfully'));
                this.division = new DivisionDto();
            });
  }

  cancel() {
    this.division = new DivisionDto();
  }

  addNew() {
    this.division = new DivisionDto();
  }

  delete(divId) {
    this._serviceProxy.delete(divId).subscribe(res => {
      this.notify.info(this.l('DeleteSuccessfully'));
      this.division = new DivisionDto();
      this.getDivList();
    });
  }
}
