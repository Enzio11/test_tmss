import { DivisionComponent } from './division.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: DivisionComponent,
        data: { permission: 'Pages.Master.Services.Division' },
    },
];

export const DivisionRoutingRoutes = RouterModule.forChild(routes);
