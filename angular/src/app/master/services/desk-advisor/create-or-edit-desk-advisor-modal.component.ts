
import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    CreateOrEditDeskAdvisorDto,
    MstSrvDeskAdvisorServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditDeskAdvisorModal',
    templateUrl: './create-or-edit-desk-advisor-modal.component.html',
    styleUrls: ['./desk-advisor.component.css']
})
export class CreateOrEditDeskAdvisorModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    statusList = [
        { id: 'Y', name: 'Còn hiệu lực' },
        { id: 'N', name: 'Hết hiệu lực' }
    ];

    advisors = [];

    deskAdvisor: CreateOrEditDeskAdvisorDto = new CreateOrEditDeskAdvisorDto();

    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvDeskAdvisorServiceProxy
    ) {
        super(injector);
    }

    show(daId?: number): void {
        this.getAllAdvisors();
        console.log(this.advisors);
        if (!daId) {
            this.deskAdvisor = new CreateOrEditDeskAdvisorDto();
            this.deskAdvisor.id = daId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getDeskAdvisorForEdit(daId)
                .subscribe((result) => {
                    this.deskAdvisor = result.deskAdvisor;
                    this.deskAdvisor.advisorCode = this.advisors.filter(e => e.id === result.deskAdvisor.advisorId)[0] ?
                    this.advisors.filter(e => e.id === result.deskAdvisor.advisorId)[0].empCode : 0;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._serviceProxy
            .createOrEdit(this.deskAdvisor)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    onChangeSelectedAdvisor(event) {
        console.log(this.advisors, event.target.value);
        this.deskAdvisor.advisorCode = this.advisors.filter(e => e.id === parseInt(event.target.value))[0] ? 
        this.advisors.filter(e => e.id === parseInt(event.target.value))[0].empCode
        : 0;

        console.log(this.advisors.filter(e => e.id === parseInt(event.target.value)));
    }

    getAllAdvisors() {
        this._serviceProxy.getAllAdvisorsByTenantId().subscribe(res => this.advisors = res);
    }
}
