import { GetDeskAdvisorForViewDto, DeskAdvisorDto } from './../../../../shared/service-proxies/service-proxies';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewDeskAdvisorModal',
    templateUrl: './view-desk-advisor-modal.component.html'
})
export class ViewDeskAdvisorModalComponent extends AppComponentBase {

    @ViewChild('viewModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetDeskAdvisorForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetDeskAdvisorForViewDto();
        this.item.deskAdvisor = new DeskAdvisorDto();
    }

    show(item: GetDeskAdvisorForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
