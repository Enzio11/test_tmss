
import { ViewDeskAdvisorModalComponent } from './view-desk-advisor-modal.component';
import { CreateOrEditDeskAdvisorModalComponent } from './create-or-edit-desk-advisor-modal.component';
import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  MstSrvDeskAdvisorServiceProxy,
  DeskAdvisorDto
} from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-desk-advisor',
  templateUrl: './desk-advisor.component.html'
})
export class DeskAdvisorComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true })
  createOrEditModal: CreateOrEditDeskAdvisorModalComponent;
  @ViewChild('viewModal', { static: true })
  viewModal: ViewDeskAdvisorModalComponent;
  @ViewChild('entityTypeHistoryModal', { static: true })
  entityTypeHistoryModal: EntityTypeHistoryModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.DeskAdvisor';
  entityHistoryEnabled = false;

  advancedFiltersAreShown = false;
  filterText = '';

  constructor(
    injector: Injector,
    private _serviceProxy: MstSrvDeskAdvisorServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
  }

  private setIsEntityHistoryEnabled(): boolean {
    let customSettings = (abp as any).custom;
    return (
        customSettings.EntityHistory &&
        customSettings.EntityHistory.isEnabled &&
        _.filter(
            customSettings.EntityHistory.enabledEntities,
            (entityType) => entityType === this._entityTypeFullName
        ).length === 1
    );
  }

  getDeskAdvisors(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();

  this._serviceProxy
    .getAll(
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
    )
    .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  createDeskAdvisor(): void {
    this.createOrEditModal.show();
  }

  showHistory(deskAdvisor: DeskAdvisorDto): void {
    this.entityTypeHistoryModal.show({
        entityId: deskAdvisor.id.toString(),
        entityTypeFullName: this._entityTypeFullName,
        entityTypeDescription: '',
    });
  }

  deleteDeskAdvisor(deskAdvisor: DeskAdvisorDto): void {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
        if (isConfirmed) {
            this._serviceProxy
                .delete(deskAdvisor.id)
                .subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
        }
    });
  }

}
