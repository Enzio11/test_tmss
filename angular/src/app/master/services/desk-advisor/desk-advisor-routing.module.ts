
import { Routes, RouterModule } from '@angular/router';
import { DeskAdvisorComponent } from './desk-advisor.component';

const routes: Routes = [
    {
        path: '',
        component: DeskAdvisorComponent,
        data: { permission: 'Pages.Master.Services.DeskAdvisor' },
    },
];

export const DeskAdvisorRoutingRoutes = RouterModule.forChild(routes);
