import { MessageService } from 'abp-ng2-module';
import { LazyLoadEvent } from 'primeng/api';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FooterDto, MstSrvFooterServiceProxy, CreateOrEditFooterDto } from '../../../../shared/service-proxies/service-proxies';
import { Component, OnInit, ViewChild, Injector, ViewEncapsulation } from '@angular/core';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
  templateUrl: './footer-category.component.html',
  styleUrls: ['./footer-category.component.less']
})
export class FooterCategoryComponent extends AppComponentBase {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.Footer';
  entityHistoryEnabled = false;
  saving;
  footers: FooterDto[];
  footerForUpdate: CreateOrEditFooterDto = new CreateOrEditFooterDto();
  advancedFiltersAreShown = false;
  tenantIdFilter;
  footer: FooterDto = new FooterDto();

  constructor(private messageService: MessageService,
    injector: Injector,
    private _serviceProxy: MstSrvFooterServiceProxy) {
    super(injector);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
  }

  onRowSelect(event: any) {
    this.footer = event.data.footer !== null ? event.data.footer : new FooterDto();
  }

  getFooters(event?: LazyLoadEvent) {
    this._serviceProxy.getAll(
      this.tenantIdFilter,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe((result) => {
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  saveFooter() {
    this.saving = true;
    this._serviceProxy.createOrEdit(this.footerForUpdate)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
      });
  }

}
