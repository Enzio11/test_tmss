import { Routes, RouterModule } from '@angular/router';
import { FooterCategoryComponent } from './footer-category.component';

const routes: Routes = [
    {
        path: '',
        component: FooterCategoryComponent,
        data: { permission: 'Pages.Master.Services.Footer' },
    },
];

export const FooterRoutingRoutes = RouterModule.forChild(routes);
