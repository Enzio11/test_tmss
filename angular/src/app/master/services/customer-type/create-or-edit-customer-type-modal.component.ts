import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    CreateOrEditCustomerTypeDto,
    MstSrvCustomerTypeServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditCustomerTypeModal',
    templateUrl: './create-or-edit-customer-type-modal.component.html',
})
export class CreateOrEditCustomerTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    customerType: CreateOrEditCustomerTypeDto = new CreateOrEditCustomerTypeDto();

    constructor(
        injector: Injector,
        private _serviceProxy: MstSrvCustomerTypeServiceProxy
    ) {
        super(injector);
    }

    show(cusTypeId?: number): void {
        if (!cusTypeId) {
            this.customerType = new CreateOrEditCustomerTypeDto();
            this.customerType.id = cusTypeId;

            this.active = true;
            this.modal.show();
        } else {
            this._serviceProxy
                .getCustomerTypeForEdit(cusTypeId)
                .subscribe((result) => {
                    this.customerType = result.customerType;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._serviceProxy
            .createOrEdit(this.customerType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('Saved Successfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
