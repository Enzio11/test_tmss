
import { Routes, RouterModule } from '@angular/router';
import { CustomerTypeComponent } from './customer-type.component';

const routes: Routes = [
    {
        path: '',
        component: CustomerTypeComponent,
        data: { permission: 'Pages.Master.Services.CustomerType' },
    },
];

export const CustomerTypeRoutingRoutes = RouterModule.forChild(routes);
