
import { Routes, RouterModule } from '@angular/router';
import { BankComponent } from './bank.component';

const routes: Routes = [
    {
        path: '',
        component: BankComponent,
        data: { permission: 'Page.Master.Sales' },
    },
];

export const BankRoutingRoutes = RouterModule.forChild(routes);
