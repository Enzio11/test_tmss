
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee.component';

const routes: Routes = [
    {
        path: '',
        component: EmployeeComponent,
        data: { permission: 'Pages.Master.Services.Employee' },
    },
];

export const EmployeeRoutingRoutes = RouterModule.forChild(routes);
