import { EmployeeDto, DivisionDto, CreateOrEditEmployeeDto, TitleDto } from './../../../../shared/service-proxies/service-proxies';
import { MessageService } from 'abp-ng2-module';

import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
} from '@angular/core';
import {
  MstSrvEmployeeServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import { TreeNode } from 'primeng/api/treenode';

@Component({
  templateUrl: './employee.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
  styleUrls: ['./employee.component.less']
})
export class EmployeeComponent extends AppComponentBase {

  lazyFiles: [];
  @ViewChild('entityTypeHistoryModal', { static: true })
  entityTypeHistoryModal: EntityTypeHistoryModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  _entityTypeFullName = 'tmss.Core.Master.Services.Employee';
  entityHistoryEnabled = false;
  division: DivisionDto;
  employee: CreateOrEditEmployeeDto = new CreateOrEditEmployeeDto();
  divs: TreeNode[];
  divList: DivisionDto[];
  advancedFiltersAreShown = false;
  filterText = '';
  empCode: '';
  empName: '';
  titles: TitleDto[];
  sexList = [
    { id: 0, value: 'Nam' },
    { id: 1, value: 'Nữ' }
  ];

  constructor(
    private messageService: MessageService,
    injector: Injector,
    private _serviceProxy: MstSrvEmployeeServiceProxy,
  ) {
    super(injector);
   }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.getDivList();
    this.getTitleList();
  }

  getEmployeesByDiv(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;    
    }

  this.primengTableHelper.showLoadingIndicator();

  this._serviceProxy
      .getAll(
          this.empCode,
          this.empName,
          this.primengTableHelper.getSorting(this.dataTable),
          this.primengTableHelper.getSkipCount(this.paginator, event),
          this.primengTableHelper.getMaxResultCount(this.paginator, event),
          this.division  ? this.division.id : (this.divs ? this.divs[0]['id'] : 0)
      )
      .subscribe((result) => {
          this.primengTableHelper.totalRecordsCount = result.totalCount;
          this.primengTableHelper.records = result.items;
          this.primengTableHelper.hideLoadingIndicator();
      });
  }

  reloadPage(): void {  
    this.paginator.changePage(this.paginator.getPage());
  }

  showHistory(emp: EmployeeDto): void {
    this.entityTypeHistoryModal.show({
        entityId: emp.id.toString(),
        entityTypeFullName: this._entityTypeFullName,
        entityTypeDescription: '',
    });
  }

  deleteEmployee(emp: EmployeeDto): void {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
        if (isConfirmed) {
            this._serviceProxy
                .delete(emp.id)
                .subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
        }
    });
  }

  getDivList() {
    this._serviceProxy.getAllDivision(
    ).subscribe(res => {
      this.divs =  this.setChildren(res || []);
      this.divList = res;
    }, err => console.log(err),
    () => {
      this.getEmployeesByDiv();
    });
  }

  getTitleList() {
    this._serviceProxy.getAllTitle().subscribe(res => this.titles = res);
  }

  setChildren(sourceData, parentDivId?) {
    return (parentDivId
      ? sourceData.filter(res => res.parentDivId === parentDivId)
      : sourceData.filter(res => !res.parentDivId))
      .map(res => {
        res.label = res['divName'];
        return Object.assign({}, res, {
          children: this.setChildren(sourceData, res.id),
        });
      });
  }

   onNodeSelect(event) {
    console.log(event.node);
    this.division = event.node;
    this.getEmployeesByDiv();
  }

}
