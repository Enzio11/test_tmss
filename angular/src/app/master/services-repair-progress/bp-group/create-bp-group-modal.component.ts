import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MstSrvBPGroupServiceProxy, CreateOrUpdateBPGroupInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createBPGroupModal',
  templateUrl: './create-bp-group-modal.component.html'
})
export class CreateBPGroupModalComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal' , { static: false }) modal: ModalDirective;

  BPGroup: CreateOrUpdateBPGroupInput = new CreateOrUpdateBPGroupInput();

  active: boolean = false;
  saving: boolean = false;

  constructor(
    injector: Injector,
    private _BPGroupService: MstSrvBPGroupServiceProxy
  ) {
    super(injector);
  }

  show(): void {
    this.active = true;
    this.BPGroup = new CreateOrUpdateBPGroupInput();
    this.BPGroup.isActive = true;
    this._BPGroupService.getMaxOrdering().subscribe(result => {
      this.BPGroup.ordering = result;
    });
    this.modal.show();
  }

  onShown(): void {
    document.getElementById('GroupName').focus();
  }

  save(): void {
    this.saving = true;
    this._BPGroupService.createOrUpdateBPGroup(this.BPGroup)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(this.BPGroup);
      });
  }

  close(): void {
    this.modal.hide();
    this.active = false;
  }
}