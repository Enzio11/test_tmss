import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BPGroupListDto, MstSrvBPGroupServiceProxy } from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng/public_api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-bp-group',
  templateUrl: './bp-group.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class BpGroupComponent extends AppComponentBase implements OnInit {
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filters: {
    filterText: string;
    isActive: boolean;
  } = <any>{};

  constructor(
    injector: Injector,
    private _BPGroupService: MstSrvBPGroupServiceProxy,
    private _activatedRoute: ActivatedRoute,
  ) { 
    super(injector);
    this.setFiltersFromRoute();
  }

  ngOnInit(): void {
    this.filters.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  setFiltersFromRoute(): void {
    this.filters.isActive = true;
  }

  getBPGroups(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);

      return;
    }

    this.primengTableHelper.showLoadingIndicator();

    this._BPGroupService.getBPGroups(
      this.filters.filterText,
      this.filters.isActive,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getMaxResultCount(this.paginator, event),
      this.primengTableHelper.getSkipCount(this.paginator, event)
    ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }

  deleteBPGroup(BPgroup: BPGroupListDto): void {
    this.message.confirm(
      this.l('BPGroupDeleteWarningMessage', BPgroup.groupName),
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this._BPGroupService.deleteBPGroup(BPgroup.id).subscribe(() => {
            this.reloadPage();
            this.notify.success(this.l('SuccessfullyDeleted'));
          });
        }
      }
    );
  }
}