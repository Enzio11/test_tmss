import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrUpdateBPGroupInput, MstSrvBPGroupServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'editBPGroupModal',
  templateUrl: './edit-bp-group-modal.component.html'
})
export class EditBPGroupModalComponent extends AppComponentBase {
  @ViewChild('nameInput', { static: true }) nameInput: ElementRef;
  @ViewChild('editModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;
  BPGroup: CreateOrUpdateBPGroupInput = new CreateOrUpdateBPGroupInput();

  constructor(
    injector: Injector,
    private _BPGroupService: MstSrvBPGroupServiceProxy
  ) {
    super(injector);
  }

  show(BPGroupId: number): void {
    this.active = true;
    this._BPGroupService.getBPGroupForEdit(BPGroupId).subscribe((result) => {
      this.BPGroup = result;
      this.modal.show();
    });
  }

  onShown(): void {
    document.getElementById('GroupName').focus();
  }

  save(): void {
    this.saving = true;

    this._BPGroupService.createOrUpdateBPGroup(this.BPGroup)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}