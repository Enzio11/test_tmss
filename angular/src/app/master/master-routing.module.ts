import { SupplierModule } from "./services/supplier/supplier.module";
import { FloorModule } from './services/floor/floor.module';
import { DivisionModule } from './services/division/division.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BpGroupComponent } from "./services-repair-progress/bp-group/bp-group.component";
import { truncate } from "fs";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "dealer",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstGenDealers/mst-gen-dealers.modules"
                            ).then((m) => m.MstGenDealersModule),
                        data: { preload: true },
                    },
                    {
                        path: "province",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstGenProvince/mst-gen-province.modules"
                            ).then((m) => m.MstGenProvinceModule),
                        data: { preload: true },
                    },
                    {
                        path: "colors",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleColors/mstSleColors.module"
                            ).then((m) => m.MstSleColorsModule),
                        data: { preload: true },
                    },
                    {
                        path: "yards",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleYards/mstSleYards.modules"
                            ).then((m) => m.MstSleYardsModule),
                        data: { preload: true },
                    },
                    {
                        path: "areas",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleAreas/mst-sle-areas.modules"
                            ).then((m) => m.MstSleAreasModule),
                        data: { preload: true },
					},
					{
                        path: "lookup",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleLookup/mstSleLookup.modules"
                            ).then((m) => m.MstSleLookupModule),
                        data: { preload: true },
                    },
                    {
                        path: "groupdealer",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleDealerGroup/mstSleDealerGroups.modules"
                            ).then((m) => m.MstSleDealerGroupsModule),
                        data: { preload: true },
                    },
                    {
                        path: "models",
                        loadChildren: () =>
                            import(
                                "app/master/sales/mstSleModels/mstSleModels.module"
                            ).then((m) => m.MstSleModelsModule),
                        data: { preload: true }
                    },

                    {
                        path: "bp-group",
                        component: BpGroupComponent,
                        data: {
                            permission:
                                "Pages.Master.ServicesRepairProgress.BPGroup",
                        },
                    },
                    {
                        path: "bank-service",
                        loadChildren: () =>
                            import("app/master/services/bank/bank.module").then(
                                (m) => m.BankModule
                            ), //Lazy load module
                        data: { preload: true },
                    },
                    {
                        path: "parts-type",
                        loadChildren: () =>
                            import(
                                "app/master/services-part/parts-type/parts-type.module"
                            ).then((m) => m.PartsTypeModule), //Lazy load module
                        data: { preload: true },
                    },
                    {
                        path: "supplier",
                        loadChildren: () =>
                            import(
                                "app/master/services/supplier/supplier.module"
                            ).then((m) => m.SupplierModule), //Lazy load module
                        data: { preload: true },
                    },
                    {
                        path: "customer-type",
                        loadChildren: () =>
                            import(
                                "app/master/services/customer-type/customer-type.module"
                            ).then((m) => m.CustomerTypeModule), //Lazy load module
                        data: { preload: true },
                    },
                    {
                        path: "desk-advisor",
                        loadChildren: () =>
                            import(
                                "app/master/services/desk-advisor/desk-advisor.module"
                            ).then((m) => m.DeskAdvisorModule), //Lazy load module
                        data: { preload: true },
                    },
                    {   
						path: 'customer-type',
						loadChildren: () => import('app/master/services/customer-type/customer-type.module').then(m => m.CustomerTypeModule), //Lazy load module
						data: { preload: true }
					},
					{
						path: 'desk-advisor',
						loadChildren: () => import('app/master/services/desk-advisor/desk-advisor.module').then(m => m.DeskAdvisorModule), //Lazy load module
						data: { preload: true }
					},
					{
						path: 'employee',
						loadChildren: () => import('app/master/services/employee/employee.module').then(m => m.EmployeeModule), //Lazy load module
						data: { preload: true }
					},
					{
						path: 'division',
						loadChildren: () => import('app/master/services/division/division.module').then(m => m.DivisionModule), //Lazy load module
						data: { preload: true }
					},
					{
						path: 'floor',
						loadChildren: () => import('app/master/services/floor/floor.module').then(m => m.FloorModule), //Lazy load module
						data: { preload: true }
					},
					{
						path: 'footer-catgegory',
						loadChildren: () => import('@app/master/services/footer-category/footer-category.module').then(m => m.FooterCategoryModule), //Lazy load module
						data: { preload: true }
					},
                    { path: "", redirectTo: "dashboard", pathMatch: "full" },
                    { path: "**", redirectTo: "dashboard" },
                ],
            },
        ]),
    ],
    exports: [RouterModule
    ]
})
export class MasterRoutingModule {}
