default:
  # cache:
  #   paths: [angular/node_modules]
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure

stages:
  - build-dotnet
  - test-dotnet
  - deploy-dotnet
  - build-angular
  - test-angular
  - deploy-angular

variables:
  JOB_PUBLISH_PROFILE_NAME: "" #Staging, Production
  JOB_PUBLISH_MODE: "" #staging, publish

# -----------------------------------------------------------------------------
# Templates
# -----------------------------------------------------------------------------

# dotnet
.build_dotnet_job_template: &build_dotnet_job_definition
  stage: build-dotnet
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  before_script:
    - cd aspnet-core
    - dotnet restore tmss.Web.sln
  script:
    - dotnet build tmss.Web.sln

.test_dotnet_job_template: &test_dotnet_job_definition
  stage: test-dotnet
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  before_script:
    - cd aspnet-core
    - dotnet restore tmss.Web.sln
  script:
    - cd test/tmss.Tests
    - dotnet test
  allow_failure: false

.deploy_dotnet_job_template: &deploy_dotnet_job_definition
  stage: deploy-dotnet
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass
    - cd aspnet-core
    - dotnet restore tmss.Web.sln

.deploy_dotnet_script_template: &deploy_dotnet_script_definition
    - cd src/tmss.Web.Host
    - dotnet build -c Release /p:DeployOnBuild=true /p:PublishProfile=$JOB_PUBLISH_PROFILE_NAME
    - cd obj/Release/netcoreapp3.1/PubTmp/Out/
    - cat web.config
    - export SSHPASS=$USER_PASS

# angular
.build_angular_job_template: &build_angular_job_definition
  stage: build-angular
  image: trion/ng-cli
  cache:
    key:
      files:
        - angular/package.json
    paths:
      - angular/node_modules
  before_script:
    - cd angular
    - yarn
  script:
    - npm run publish
  after_script:
    - ls -la angular/dist

.test_angular_ut_it_job_template: &test_angular_ut_it_job_definition
  stage: test-angular
  image: trion/ng-cli-karma
  cache:
    key:
      files:
        - angular/package.json
    paths:
      - angular/node_modules
    policy: pull
  before_script:
    - cd angular
    - yarn
  # temporary
  allow_failure: true
  script:
    - npm run test-ci

.test_angular_e2e_job_template: &test_angular_e2e_job_definition
  stage: test-angular
  image: trion/ng-cli-e2e
  cache:
    key:
      files:
        - angular/package.json
    paths:
      - angular/node_modules
    policy: pull
  before_script:
    - cd angular
    - yarn
  # temporary
  allow_failure: true
  script:
    - npm run test-ci-e2e

.deploy_angular_job_template: &deploy_angular_job_definition
  stage: deploy-angular
  image: trion/ng-cli-karma
  cache:
    key:
      files:
        - angular/package.json
    paths:
      - angular/node_modules
    policy: pull
  before_script:
    - apt-get update -qq && apt-get install -y -qq sshpass
    - cd angular
    - yarn

.deploy_angular_script_template: &deploy_angular_script_definition
    - npm run $JOB_PUBLISH_MODE
    - cd dist/
    - export SSHPASS=$USER_PASS

# -----------------------------------------------------------------------------
# Dev Branch - Only Build
# -----------------------------------------------------------------------------

dev:build:dotnet:
  <<: *build_dotnet_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "dev"'
      when: always

dev:build:angular:
  <<: *build_angular_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "dev"'
      when: always

# -----------------------------------------------------------------------------
# Master Branch - Staging - Build,Test,Deploy to Staging
# -----------------------------------------------------------------------------

# dotnet
stage:build:dotnet:
  <<: *build_dotnet_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always

stage:test:dotnet:
  <<: *test_dotnet_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always

stage:deploy:dotnet:
  <<: *deploy_dotnet_job_definition
  variables:
    JOB_PUBLISH_PROFILE_NAME: "FolderProfileStaging"
  script:
    - *deploy_dotnet_script_definition
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./stop-website-tmss-be-stag"
    # Keep App_Data
    - sshpass -v -e scp -o stricthostkeychecking=no -r . Administrator@14.225.19.84:C:/inetpub/wwwroot/tmss-be-stag
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./start-website-tmss-be-stag"
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always
  environment:
    name: staging-be
    url: http://tmss-be-stag.ntsc.com.vn

# angular
stage:build:angular:
  <<: *build_angular_job_definition
  variables:
    JOB_PUBLISH_MODE: "staging"
  script:
    # override staging
    - npm run $JOB_PUBLISH_MODE
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always
  # Should only make artifacts for Tags Release only
  # artifacts:
  #   name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  #   expire_in: 1 day
  #   expose_as: 'dist'
  #   paths:
  #     - angular/dist/
  #   dependencies:
  #     - previous stage job (eg build:osx)

stage:test-ut-it:angular:
  <<: *test_angular_ut_it_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always

stage:test-e2e:angular:
  <<: *test_angular_e2e_job_definition
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always

stage:deploy:angular:
  <<: *deploy_angular_job_definition
  variables:
    JOB_PUBLISH_MODE: "staging"
  script:
    - *deploy_angular_script_definition
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./stop-website-tmss-fe-stag"
    # Keep web.config
    - sshpass -v -e scp -o stricthostkeychecking=no -r . Administrator@14.225.19.84:C:/inetpub/wwwroot/tmss-fe-stag
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./start-website-tmss-fe-stag"
  rules:
    - if: '$CI_BUILD_REF_NAME == "master"'
      when: always
    - if: '$CI_BUILD_REF_NAME =~ /^demo-.*$/'
      when: always
  environment:
    name: staging-fe
    url: http://tmss-fe-stag.ntsc.com.vn

# -----------------------------------------------------------------------------
# Master Branch - Production - Build,Test,Deploy to Release
# -----------------------------------------------------------------------------

# dotnet
prod:build:dotnet:
  <<: *build_dotnet_job_definition
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always

prod:test:dotnet:
  <<: *test_dotnet_job_definition
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always

prod:deploy:dotnet:
  <<: *deploy_dotnet_job_definition
  variables:
    JOB_PUBLISH_PROFILE_NAME: "FolderProfileProduction"
  script:
    - *deploy_dotnet_script_definition
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./stop-website-tmss-be"
    # Keep App_Data
    - sshpass -v -e scp -o stricthostkeychecking=no -r . Administrator@14.225.19.84:C:/inetpub/wwwroot/tmss-be
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./start-website-tmss-be"
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always
  environment:
    name: production-be
    url: http://tmss-be.ntsc.com.vn

# angular
prod:build:angular:
  <<: *build_angular_job_definition
  variables:
    JOB_PUBLISH_MODE: "publish"
  script:
    # override staging
    - npm run $JOB_PUBLISH_MODE
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always

prod:test-ut-it:angular:
  <<: *test_angular_ut_it_job_definition
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always

prod:test-e2e:angular:
  <<: *test_angular_e2e_job_definition
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always

prod:deploy:angular:
  <<: *deploy_angular_job_definition
  variables:
    JOB_PUBLISH_MODE: "publish"
  script:
    - *deploy_angular_script_definition
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./stop-website-tmss-fe"
    # Keep web.config
    - sshpass -v -e scp -o stricthostkeychecking=no -r . Administrator@14.225.19.84:C:/inetpub/wwwroot/tmss-fe
    - sshpass -v -e ssh -o StrictHostKeyChecking=no Administrator@14.225.19.84 "Set-Location C:\Scripts; ./start-website-tmss-fe"
  rules:
    - if: '$CI_BUILD_TAG =~ /^v/'
      when: always
  environment:
    name: production-fe
    url: http://tmss-fe.ntsc.com.vn
