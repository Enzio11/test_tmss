﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Mst_Srv_Dlr_Config_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstSrvDlrConfig",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WkAmFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    WkAmTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    WkPmFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    WkPmTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    RestAmFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    RestAmTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    RestPmFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    RestPmTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    Cost = table.Column<double>(nullable: true),
                    CoEfficientB = table.Column<double>(nullable: true),
                    CoEfficientJg = table.Column<double>(nullable: true),
                    CoEfficientP = table.Column<double>(nullable: true),
                    EffDateFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffDateTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    Logo = table.Column<byte[]>(nullable: true),
                    FooterDoc = table.Column<string>(maxLength: 2000, nullable: true),
                    TaxJobs = table.Column<long>(nullable: true),
                    LogLv = table.Column<long>(nullable: true),
                    UseProgress = table.Column<string>(maxLength: 1, nullable: false),
                    WashTime = table.Column<int>(nullable: false),
                    WaitTime = table.Column<int>(nullable: false),
                    ReceptionTimeBp = table.Column<int>(nullable: false),
                    ReceptionTimeScc = table.Column<int>(nullable: false),
                    WarningTime = table.Column<int>(nullable: false),
                    WshopGjChangeTime = table.Column<int>(nullable: false),
                    WshopBpChangeTime = table.Column<int>(nullable: false),
                    LateAppointmentTime = table.Column<int>(nullable: false),
                    LateSuggested = table.Column<int>(nullable: true),
                    ReservePartsPct = table.Column<int>(nullable: false),
                    HolidayDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    GuessReceptionOrder = table.Column<string>(maxLength: 1, nullable: false),
                    Reception = table.Column<int>(nullable: true),
                    UseProgressDs = table.Column<string>(maxLength: 1, nullable: false),
                    SplitTimeScc = table.Column<int>(nullable: true),
                    SplitTimeDs = table.Column<int>(nullable: true),
                    SplitTimeRx = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSrvDlrConfig", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MstSrvDlrConfig");
        }
    }
}
