﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_Customer_Details_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SrvQuoCustomerDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CusId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    IdNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Tel = table.Column<string>(maxLength: 40, nullable: true),
                    Mobil = table.Column<string>(maxLength: 20, nullable: false),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    Type = table.Column<string>(maxLength: 1, nullable: true),
                    CallTime = table.Column<string>(maxLength: 1, nullable: true),
                    CusVerify = table.Column<string>(maxLength: 1, nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    Ordering = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoCustomerDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SrvQuoCustomerDetails_SrvQuoCustomers_CusId",
                        column: x => x.CusId,
                        principalTable: "SrvQuoCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SrvQuoCusVisit_SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit",
                column: "SrvQuoCustomerDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_SrvQuoCustomerDetails_CusId",
                table: "SrvQuoCustomerDetails",
                column: "CusId");

            migrationBuilder.AddForeignKey(
                name: "FK_SrvQuoCusVisit_SrvQuoCustomerDetails_SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit",
                column: "SrvQuoCustomerDetailId",
                principalTable: "SrvQuoCustomerDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SrvQuoCusVisit_SrvQuoCustomerDetails_SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropTable(
                name: "SrvQuoCustomerDetails");

            migrationBuilder.DropIndex(
                name: "IX_SrvQuoCusVisit_SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropColumn(
                name: "SrvQuoCustomerDetailId",
                table: "SrvQuoCusVisit");
        }
    }
}
