﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Mst_Srv_Division_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstSrvDivision",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ParentDivId = table.Column<long>(nullable: true),
                    DivCode = table.Column<string>(maxLength: 20, nullable: false),
                    DivName = table.Column<string>(maxLength: 50, nullable: false),
                    Des = table.Column<string>(maxLength: 200, nullable: true),
                    ManagerId = table.Column<long>(nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstSrvDivision", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MstSrvDivision_MstSrvDivision_ParentDivId",
                        column: x => x.ParentDivId,
                        principalTable: "MstSrvDivision",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MstSrvEmployee_DivId",
                table: "MstSrvEmployee",
                column: "DivId");

            migrationBuilder.CreateIndex(
                name: "IX_MstSrvDivision_ParentDivId",
                table: "MstSrvDivision",
                column: "ParentDivId");

            migrationBuilder.AddForeignKey(
                name: "FK_MstSrvEmployee_MstSrvDivision_DivId",
                table: "MstSrvEmployee",
                column: "DivId",
                principalTable: "MstSrvDivision",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MstSrvEmployee_MstSrvDivision_DivId",
                table: "MstSrvEmployee");

            migrationBuilder.DropTable(
                name: "MstSrvDivision");

            migrationBuilder.DropIndex(
                name: "IX_MstSrvEmployee_DivId",
                table: "MstSrvEmployee");
        }
    }
}
