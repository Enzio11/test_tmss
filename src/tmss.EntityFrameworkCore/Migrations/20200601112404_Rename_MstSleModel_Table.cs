﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Rename_MstSleModel_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MstSleModels",
                table: "MstSleModels");

            migrationBuilder.RenameTable(
                name: "MstSleModels",
                newName: "MstSleModel");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MstSleModel",
                table: "MstSleModel",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MstSleModel",
                table: "MstSleModel");

            migrationBuilder.RenameTable(
                name: "MstSleModel",
                newName: "MstSleModels");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MstSleModels",
                table: "MstSleModels",
                column: "Id");
        }
    }
}
