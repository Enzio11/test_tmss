﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_fix_tb_MstGenDealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DlrId",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropColumn(
                name: "DealerGroupId",
                table: "MstGenDealers");

            migrationBuilder.DropColumn(
                name: "DealerId",
                table: "MstGenDealers");

            migrationBuilder.DropColumn(
                name: "DealerTypeId",
                table: "MstGenDealers");

            migrationBuilder.DropColumn(
                name: "ProvinceId",
                table: "MstGenDealers");

            migrationBuilder.AddColumn<long>(
                name: "SubRegionId",
                table: "MstGenProvinces",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "MstGenDealers",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubRegionId",
                table: "MstGenProvinces");

            migrationBuilder.AddColumn<long>(
                name: "DlrId",
                table: "SrvQuoCusVisit",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "MstGenDealers",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DealerGroupId",
                table: "MstGenDealers",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "DealerId",
                table: "MstGenDealers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DealerTypeId",
                table: "MstGenDealers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProvinceId",
                table: "MstGenDealers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
