﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Drop_FK_Customer_Customer_Detail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SrvQuoCustomerDetails_SrvQuoCustomers_CusId",
                table: "SrvQuoCustomerDetails");

            migrationBuilder.DropIndex(
                name: "IX_SrvQuoCustomerDetails_CusId",
                table: "SrvQuoCustomerDetails");

            migrationBuilder.AddColumn<long>(
                name: "srvQuoCustomerId",
                table: "SrvQuoCustomerDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SrvQuoCustomerDetails_srvQuoCustomerId",
                table: "SrvQuoCustomerDetails",
                column: "srvQuoCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_SrvQuoCustomerDetails_SrvQuoCustomers_srvQuoCustomerId",
                table: "SrvQuoCustomerDetails",
                column: "srvQuoCustomerId",
                principalTable: "SrvQuoCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SrvQuoCustomerDetails_SrvQuoCustomers_srvQuoCustomerId",
                table: "SrvQuoCustomerDetails");

            migrationBuilder.DropIndex(
                name: "IX_SrvQuoCustomerDetails_srvQuoCustomerId",
                table: "SrvQuoCustomerDetails");

            migrationBuilder.DropColumn(
                name: "srvQuoCustomerId",
                table: "SrvQuoCustomerDetails");

            migrationBuilder.CreateIndex(
                name: "IX_SrvQuoCustomerDetails_CusId",
                table: "SrvQuoCustomerDetails",
                column: "CusId");

            migrationBuilder.AddForeignKey(
                name: "FK_SrvQuoCustomerDetails_SrvQuoCustomers_CusId",
                table: "SrvQuoCustomerDetails",
                column: "CusId",
                principalTable: "SrvQuoCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
