﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Remove_Tenant_Id_Mst_Srv_Footer_Report_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "MstSrvFooterReportType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "MstSrvFooterReportType",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
