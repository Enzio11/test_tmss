﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Relationship_Mst_Srv_Footer_Report_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_MstSrvFooterTmv_TypeId",
                table: "MstSrvFooterTmv",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MstSrvFooter_TypeId",
                table: "MstSrvFooter",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_MstSrvFooter_MstSrvFooterReportType_TypeId",
                table: "MstSrvFooter",
                column: "TypeId",
                principalTable: "MstSrvFooterReportType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MstSrvFooterTmv_MstSrvFooterReportType_TypeId",
                table: "MstSrvFooterTmv",
                column: "TypeId",
                principalTable: "MstSrvFooterReportType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MstSrvFooter_MstSrvFooterReportType_TypeId",
                table: "MstSrvFooter");

            migrationBuilder.DropForeignKey(
                name: "FK_MstSrvFooterTmv_MstSrvFooterReportType_TypeId",
                table: "MstSrvFooterTmv");

            migrationBuilder.DropIndex(
                name: "IX_MstSrvFooterTmv_TypeId",
                table: "MstSrvFooterTmv");

            migrationBuilder.DropIndex(
                name: "IX_MstSrvFooter_TypeId",
                table: "MstSrvFooter");
        }
    }
}
