﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Update_Srv_Quo_cusvisit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CusDid",
                table: "SrvQuoCusVisit",
                newName: "CusDId");

            migrationBuilder.AddColumn<string>(
                name: "ContactAddress",
                table: "SrvQuoCusVisit",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "SrvQuoCusVisit",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerTel",
                table: "SrvQuoCusVisit",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactAddress",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropColumn(
                name: "CustomerTel",
                table: "SrvQuoCusVisit");

            migrationBuilder.RenameColumn(
                name: "CusDId",
                table: "SrvQuoCusVisit",
                newName: "CusDid");
        }
    }
}
