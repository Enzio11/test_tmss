﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_Appointment_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoAppointment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CusVsId = table.Column<decimal>(type: "numeric(38, 0)", nullable: true),
                    ReqDesc = table.Column<string>(maxLength: 1000, nullable: true),
                    Sponsor = table.Column<string>(maxLength: 50, nullable: true),
                    Deposits = table.Column<decimal>(type: "decimal(30, 0)", nullable: true),
                    Depositsstatus = table.Column<string>(maxLength: 1, nullable: true),
                    SaId = table.Column<decimal>(type: "numeric(38, 0)", nullable: true),
                    Safrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    Sato = table.Column<DateTime>(type: "datetime", nullable: false),
                    WShopId = table.Column<decimal>(type: "numeric(38, 0)", nullable: true),
                    WShopfrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    WShopto = table.Column<DateTime>(type: "datetime", nullable: true),
                    CarDelivery = table.Column<DateTime>(type: "datetime", nullable: true),
                    AppStatus = table.Column<string>(maxLength: 1, nullable: true),
                    CancelReason = table.Column<string>(maxLength: 1, nullable: true),
                    AppChangerCount = table.Column<decimal>(type: "numeric(38, 0)", nullable: true),
                    AppType = table.Column<string>(maxLength: 1, nullable: true),
                    CusArr = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateConfirm = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateContact = table.Column<DateTime>(type: "datetime", nullable: true),
                    ContactCount = table.Column<decimal>(type: "numeric(38, 0)", nullable: true),
                    TechnicalId = table.Column<long>(nullable: true),
                    EstimateMoney = table.Column<decimal>(type: "decimal(15, 0)", nullable: true),
                    EstimateTime = table.Column<long>(nullable: false),
                    Notes = table.Column<string>(maxLength: 500, nullable: true),
                    RoId = table.Column<long>(nullable: true),
                    Lh1 = table.Column<DateTime>(type: "datetime", nullable: true),
                    Lh2 = table.Column<DateTime>(type: "datetime", nullable: true),
                    Lh3 = table.Column<DateTime>(type: "datetime", nullable: true),
                    Lh1Note = table.Column<string>(maxLength: 200, nullable: true),
                    Lh2Note = table.Column<string>(maxLength: 200, nullable: true),
                    Lh3Note = table.Column<string>(maxLength: 200, nullable: true),
                    KqNoShow = table.Column<string>(maxLength: 200, nullable: true),
                    AppointmentNo = table.Column<string>(maxLength: 20, nullable: true),
                    IsDone = table.Column<string>(maxLength: 1, nullable: true),
                    JobsMoney = table.Column<decimal>(type: "decimal(15, 0)", nullable: true),
                    PartsMoney = table.Column<decimal>(type: "decimal(15, 0)", nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoAppointment", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoAppointment");
        }
    }
}
