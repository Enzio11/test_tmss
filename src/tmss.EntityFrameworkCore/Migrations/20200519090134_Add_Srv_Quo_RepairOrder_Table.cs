﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_RepairOrder_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoRepairOrder",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CusVsId = table.Column<long>(nullable: false),
                    RepairOrderNo = table.Column<string>(nullable: true),
                    RoType = table.Column<string>(nullable: true),
                    RoState = table.Column<string>(nullable: true),
                    ReqDesc = table.Column<string>(nullable: true),
                    InrState = table.Column<string>(nullable: true),
                    TestPaperPrint = table.Column<double>(nullable: true),
                    QuotationPrint = table.Column<double>(nullable: true),
                    ReadJustRoId = table.Column<long>(nullable: true),
                    RcTypeId = table.Column<double>(nullable: true),
                    CusWait = table.Column<string>(nullable: true),
                    ReadJustFixNote = table.Column<string>(nullable: true),
                    ReadJustReason = table.Column<string>(nullable: true),
                    Attribute1 = table.Column<string>(nullable: true),
                    IsCarWash = table.Column<string>(nullable: true),
                    GetOldParts = table.Column<string>(nullable: true),
                    RoChange = table.Column<string>(nullable: true),
                    CheckTlccpt = table.Column<long>(nullable: true),
                    OpenRoDate = table.Column<DateTime>(nullable: true),
                    Kcalc = table.Column<string>(nullable: true),
                    CloseRoDate = table.Column<DateTime>(nullable: true),
                    Attribute2 = table.Column<string>(nullable: true),
                    InsertQVersion = table.Column<string>(nullable: true),
                    UseProgress = table.Column<string>(nullable: true),
                    FreePm = table.Column<string>(nullable: true),
                    Km = table.Column<long>(nullable: true),
                    QcLevel = table.Column<long>(nullable: true),
                    TotalAmount = table.Column<long>(nullable: true),
                    TotalDiscount = table.Column<long>(nullable: true),
                    TotalTaxAmount = table.Column<long>(nullable: true),
                    Gid = table.Column<long>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    IsPriority = table.Column<string>(nullable: true),
                    EstimateTime = table.Column<long>(nullable: true),
                    CarDeliveryTime = table.Column<DateTime>(nullable: true),
                    CampaignId = table.Column<long>(nullable: true),
                    IsCusWait = table.Column<string>(nullable: true),
                    IsTakeParts = table.Column<string>(nullable: true),
                    StartRepairTime = table.Column<DateTime>(nullable: true),
                    StartCarWashTime = table.Column<DateTime>(nullable: true),
                    PrintLog = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoRepairOrder", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoRepairOrder");
        }
    }
}
