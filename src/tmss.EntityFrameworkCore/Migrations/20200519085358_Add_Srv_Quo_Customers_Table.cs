﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_Srv_Quo_Customers_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SrvQuoCustomers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CusTypeId = table.Column<long>(nullable: true),
                    CusNo = table.Column<string>(maxLength: 20, nullable: true),
                    CarOwnerName = table.Column<string>(maxLength: 300, nullable: false),
                    CarOwnerIdNum = table.Column<string>(maxLength: 100, nullable: true),
                    CarOwnerAdd = table.Column<string>(maxLength: 300, nullable: false),
                    CarOwnerTel = table.Column<string>(maxLength: 25, nullable: false),
                    CarOwnerMobile = table.Column<string>(maxLength: 25, nullable: true),
                    CarOwnerFax = table.Column<string>(maxLength: 20, nullable: true),
                    CarOwnerEmail = table.Column<string>(maxLength: 100, nullable: true),
                    TaxCode = table.Column<string>(maxLength: 50, nullable: true),
                    BankId = table.Column<long>(nullable: true),
                    AccNo = table.Column<string>(maxLength: 30, nullable: true),
                    CusVerify = table.Column<string>(maxLength: 1, nullable: true),
                    CusMrs = table.Column<string>(maxLength: 1, nullable: true),
                    OrgName = table.Column<string>(maxLength: 2000, nullable: true),
                    CallTime = table.Column<string>(maxLength: 1, nullable: true),
                    CurFir = table.Column<string>(maxLength: 1, nullable: true),
                    CusContact = table.Column<long>(nullable: true),
                    ProvinceId = table.Column<long>(nullable: true),
                    DistrictId = table.Column<long>(nullable: true),
                    SalesCustormerId = table.Column<long>(nullable: true),
                    RelativesName = table.Column<string>(maxLength: 255, nullable: true),
                    RelativesAddress = table.Column<string>(maxLength: 255, nullable: true),
                    RelationshipId = table.Column<string>(maxLength: 50, nullable: true),
                    RelativesPrOId = table.Column<long>(maxLength: 255, nullable: true),
                    RelativesPhone = table.Column<string>(maxLength: 255, nullable: true),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    Ordering = table.Column<long>(nullable: true),
                    RefId = table.Column<long>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SrvQuoCustomers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SrvQuoCustomers");
        }
    }
}
