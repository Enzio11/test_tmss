﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tmss.Migrations
{
    public partial class Add_PK_Srv_Quo_Vehicle_Cus_Visit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SrvQuoVehicleId",
                table: "SrvQuoCusVisit",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SrvQuoCusVisit_SrvQuoVehicleId",
                table: "SrvQuoCusVisit",
                column: "SrvQuoVehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_SrvQuoCusVisit_SrvQuoVehicles_SrvQuoVehicleId",
                table: "SrvQuoCusVisit",
                column: "SrvQuoVehicleId",
                principalTable: "SrvQuoVehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SrvQuoCusVisit_SrvQuoVehicles_SrvQuoVehicleId",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropIndex(
                name: "IX_SrvQuoCusVisit_SrvQuoVehicleId",
                table: "SrvQuoCusVisit");

            migrationBuilder.DropColumn(
                name: "SrvQuoVehicleId",
                table: "SrvQuoCusVisit");
        }
    }
}
