﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.RepairProgress
{
  class SrvPrgActualConfiguration : IEntityTypeConfiguration<SrvPrgActual>
  {
    // [Table("SRV_PRG_ACTUAL")] // ~ SRV_B_RO_WSHOP_ACTUAL
    public void Configure(EntityTypeBuilder<SrvPrgActual> builder)
    {
      builder.ToTable("SRV_PRG_ACTUAL");
      builder.Property(t => t.JobType).HasColumnName("RO_TYPE").IsRequired(true);
      builder.Property(t => t.QCLevel).HasColumnName("QC_LEVEL").IsRequired(true);
      builder.Property(t => t.IsCustomerWait).HasColumnName("IS_CUS_WAIT").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsTakeParts).HasColumnName("IS_TAKE_PARTS").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsPriority).HasColumnName("IS_PRIORITY").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.IsCarWash).HasColumnName("IS_CAR_WASH").IsRequired(false).HasDefaultValue(false);
      builder.Property(t => t.ActualFromTime).HasColumnName("FROM_TIME").IsRequired(false);
      builder.Property(t => t.ActualToTime).HasColumnName("TO_TIME").IsRequired(false);
      builder.Property(t => t.ActualCalcTime).HasColumnName("CALC_TIME").IsRequired(false);
      builder.Property(t => t.ActualState).HasColumnName("STATE").IsRequired(false);
      builder.Property(t => t.PendingNote).HasColumnName("PENDING_NOTE").IsRequired(false).HasMaxLength(Constants.MaxDescLength);

      // TODO: chuyen thanh bang tuong ung
      builder.Property(t => t.PendingId).HasColumnName("PENDING_ID");
      //builder.HasOne(t => t.LOOKUP_TABLE).WithMany().HasForeignKey("PENDING_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.CustomerVisitId).HasColumnName("CUS_VS_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("CUS_VS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.ROId).HasColumnName("RO_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("RO_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.AppointmentId).HasColumnName("APP_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("APP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.VehicleId).HasColumnName("VEH_ID");
      //builder.HasOne(t => t.DEALER_TABLE).WithMany().HasForeignKey("VEH_ID").OnDelete(DeleteBehavior.Restrict);
      builder.Property(t => t.CustomerId).HasColumnName("CUS_ID");

      builder.HasOne(t => t.MstSrvWorkshop).WithMany().HasForeignKey("WS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPProcess).WithMany().HasForeignKey("BP_PROCESS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPGroup).WithMany().HasForeignKey("BP_GROUP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.SrvPrgPlan).WithMany().HasForeignKey("PLAN_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}