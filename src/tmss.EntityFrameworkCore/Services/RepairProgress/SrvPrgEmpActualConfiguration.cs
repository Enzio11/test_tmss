﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.RepairProgress
{
  public class SrvPrgEmpActualConfiguration : IEntityTypeConfiguration<SrvPrgEmpActual>
  {
    // [Table("SRV_PRG_EMP_ACTUAL")] // ~ SRV_B_RO_WSHOP_EMP_ACTUAL
    public void Configure(EntityTypeBuilder<SrvPrgEmpActual> builder)
    {
      builder.ToTable("SRV_PRG_EMP_ACTUAL");

      builder.Property(t => t.JobType).HasColumnName("RO_TYPE").IsRequired(true);
      builder.Property(t => t.EmployeeActualFromTime).HasColumnName("FROM_TIME").IsRequired(false);
      builder.Property(t => t.EmployeeActualToTime).HasColumnName("TO_TIME").IsRequired(false);
      builder.Property(t => t.EmployeeActualCalcTime).HasColumnName("CALC_TIME").IsRequired(false);
      builder.Property(t => t.EmployeeActualState).HasColumnName("STATE").IsRequired(false);

      // TODO: chuyen thanh bang tuong ung
      builder.Property(t => t.EmployeeId).HasColumnName("EMP_ID").IsRequired(true);
      //builder.HasOne(t => t.EMPLOYEE_TABLE).WithMany().HasForeignKey("DLR_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvWorkshop).WithMany().HasForeignKey("WS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPProcess).WithMany().HasForeignKey("BP_PROCESS_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.MstSrvBPGroup).WithMany().HasForeignKey("BP_GROUP_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.SrvPrgPlan).WithMany().HasForeignKey("PLAN_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.SrvPrgActual).WithMany().HasForeignKey("ACTUAL_ID").OnDelete(DeleteBehavior.Restrict);
      builder.HasOne(t => t.SrvPrgEmpPlan).WithMany().HasForeignKey("EMP_PLAN_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}