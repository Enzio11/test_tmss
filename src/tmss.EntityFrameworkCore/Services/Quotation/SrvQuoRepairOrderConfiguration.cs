﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Services.Quotation
{
    // [Table("SRV_B_REPAIRORDER")] // ~ SrvQuoRepairOrder
    public class SrvQuoRepairOrderConfiguration : IEntityTypeConfiguration<SrvQuoRepairOrder>
    {
        public void Configure(EntityTypeBuilder<SrvQuoRepairOrder> builder)
        {
            // Columns
            builder.ToTable("SrvQuoRepairOrder");
            builder.Property(t => t.CusVsId).IsRequired(true);
            builder.Property(t => t.RepairOrderNo).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.RoType).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.RoState).IsRequired(false).HasMaxLength(50).HasDefaultValue('0');
            builder.Property(t => t.ReqDesc).IsRequired(false).HasMaxLength(255);
            builder.Property(t => t.InrState).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.Km).IsRequired(false);
            builder.Property(t => t.TestPaperPrint).IsRequired(false);
            builder.Property(t => t.QuotationPrint).IsRequired(false);
            builder.Property(t => t.ReadJustRoId).IsRequired(false);
            builder.Property(t => t.RcTypeId).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.CusWait).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.ReadJustFixNote).IsRequired(false).HasMaxLength(500);
            builder.Property(t => t.ReadJustReason).IsRequired(false).HasMaxLength(500);
            builder.Property(t => t.Attribute1).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.IsCarWash).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.GetOldParts).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.RoChange).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.CheckTlccpt).IsRequired(false);
            builder.Property(t => t.OpenRoDate).IsRequired(false);
            builder.Property(t => t.Kcalc).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.CloseRoDate).IsRequired(false);
            builder.Property(t => t.Attribute2).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.InsertQVersion).IsRequired(false).HasMaxLength(20);
            builder.Property(t => t.UseProgress).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.FreePm).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.Km).IsRequired(false);
            builder.Property(t => t.QcLevel).IsRequired(false);
            builder.Property(t => t.TotalAmount).IsRequired(false);
            builder.Property(t => t.TotalDiscount).IsRequired(false);
            builder.Property(t => t.TotalTaxAmount).IsRequired(false);
            builder.Property(t => t.Gid).IsRequired(false);
            builder.Property(t => t.Notes).IsRequired(false).HasMaxLength(500);
            builder.Property(t => t.IsPriority).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.EstimateTime).IsRequired(false);
            builder.Property(t => t.CarDeliveryTime).IsRequired(false);
            builder.Property(t => t.CampaignId).IsRequired(false);
            builder.Property(t => t.IsCusWait).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.IsTakeParts).IsRequired(false).HasMaxLength(1);
            builder.Property(t => t.StartRepairTime).IsRequired(false);
            builder.Property(t => t.StartCarWashTime).IsRequired(false);
            builder.Property(t => t.PrintLog).IsRequired(false).HasMaxLength(500);
        }
    }
}
