using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace tmss.EntityFrameworkCore
{
    public static class tmssDbContextConfigurer
    {
        private static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => {
                // builder.AddFilter((category, level) =>
                //     category == DbLoggerCategory.Database.Command.Name
                //     && level == LogLevel.Information
                //     );
                builder.AddConsole();
                builder.AddDebug();
                // builder.AddEventLog(); // Only run on Windows OS not macOS
                builder.AddEventSourceLogger();
            });

        public static void Configure(DbContextOptionsBuilder<tmssDbContext> builder, string connectionString)
        {
            builder.UseLoggerFactory(MyLoggerFactory).UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<tmssDbContext> builder, DbConnection connection)
        {
            builder.UseLoggerFactory(MyLoggerFactory).UseSqlServer(connection);
        }
    }
}
