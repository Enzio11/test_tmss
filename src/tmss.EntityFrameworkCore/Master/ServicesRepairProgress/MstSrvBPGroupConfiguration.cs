﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvBPGroupConfiguration : IEntityTypeConfiguration<MstSrvBPGroup>
  {
    // [Table("MST_SRV_BP_GROUP")] // ~ SRV_M_WSHOP_BP_GROUP
    public void Configure(EntityTypeBuilder<MstSrvBPGroup> builder)
    {
      builder.ToTable("MST_SRV_BP_GROUP"); 
      builder.Property(t => t.GroupName).HasColumnName("GROUP_NAME").IsRequired(true).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.GroupDesc).HasColumnName("GROUP_DESC").IsRequired(false).HasMaxLength(Constants.MaxDescLength);
      builder.Property(t => t.Ordering).HasColumnName("ORDERING").IsRequired(true);
      builder.Property(t => t.IsActive).HasColumnName("IS_ACTIVE").HasDefaultValue(true);
    }
  }
}