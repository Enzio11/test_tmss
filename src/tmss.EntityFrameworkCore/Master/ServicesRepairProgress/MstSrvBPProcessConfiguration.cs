﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvBPProcessConfiguration : IEntityTypeConfiguration<MstSrvBPProcess>
  {
    // [Table("MST_SRV_BP_PROCESS")] // ~ SRV_M_BPCONFIG
    public void Configure(EntityTypeBuilder<MstSrvBPProcess> builder)
    {
      builder.ToTable("MST_SRV_BP_PROCESS");
      builder.Property(t => t.ProcessName).HasColumnName("PROCESS_NAME").IsRequired(true).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.ProcessDesc).HasColumnName("PROCESS_DESC").IsRequired(false).HasMaxLength(Constants.MaxDescLength);
      builder.Property(t => t.ColorCode).HasColumnName("COLOR_CODE").IsRequired(false).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.Ordering).HasColumnName("ORDERING").IsRequired(true);
      builder.Property(t => t.IsActive).HasColumnName("IS_ACTIVE").HasDefaultValue(true);
    }
  }
}