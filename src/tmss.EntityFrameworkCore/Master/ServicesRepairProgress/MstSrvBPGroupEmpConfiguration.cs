﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvBPGroupEmpConfiguration : IEntityTypeConfiguration<MstSrvBPGroupEmp>
  {
    // [Table("MST_SRV_BP_GROUP_EMP")] // ~ SRV_M_WSHOP_BP_GROUP_EMP
    public void Configure(EntityTypeBuilder<MstSrvBPGroupEmp> builder)
    {
      builder.ToTable("MST_SRV_BP_GROUP_EMP");
      builder.HasOne(t => t.MstSrvBPGroup).WithMany().HasForeignKey("BP_GROUP_ID").OnDelete(DeleteBehavior.Restrict);

      // TODO: chuyen thanh bang EMPLOYEE
      builder.Property(t => t.EmployeeId).HasColumnName("EMP_ID").IsRequired(true);
      //builder.HasOne(t => t.EMPLOYEE_TABLE).WithMany().HasForeignKey("EMP_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}