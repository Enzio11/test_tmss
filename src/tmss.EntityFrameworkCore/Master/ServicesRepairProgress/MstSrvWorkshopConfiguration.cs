﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress
{
  public class MstSrvWorkshopConfiguration : IEntityTypeConfiguration<MstSrvWorkshop>
  {
    // [Table("MST_SRV_WORKSHOP")] // ~ SRV_M_WSHOP
    public void Configure(EntityTypeBuilder<MstSrvWorkshop> builder)
    {
      builder.ToTable("MST_SRV_WORKSHOP");
      builder.Property(t => t.WorkshopCode).HasColumnName("WS_CODE").IsRequired(true).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.WorkshopName).HasColumnName("WS_NAME").IsRequired(false).HasMaxLength(Constants.MaxNameLength);
      builder.Property(t => t.WorkshopDesc).HasColumnName("WS_DESC").IsRequired(false).HasMaxLength(Constants.MaxDescLength);
      builder.Property(t => t.Ordering).HasColumnName("ORDERING").IsRequired(true);
      builder.Property(t => t.IsActive).HasColumnName("IS_ACTIVE").HasDefaultValue(true);

      builder.HasOne(t => t.MstSrvWorkshopType).WithMany().HasForeignKey("WS_TYPE_ID").OnDelete(DeleteBehavior.Restrict);
    }
  }
}