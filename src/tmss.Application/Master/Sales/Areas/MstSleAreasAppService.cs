﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstSleAreas)]
	public class MstSleAreasAppService : tmssAppServiceBase, IMstSleAreasAppService
	{
		private readonly IRepository<MstSleAreas, long> _mstSleAreasRepository;

		public MstSleAreasAppService(IRepository<MstSleAreas, long> mstSleAreasRepository)
		{
			_mstSleAreasRepository = mstSleAreasRepository;
		}

		public async Task<PagedResultDto<GetMstSleAreasForViewDto>> GetAll(GetAllMstSleAreasInput input)
		{

			var filteredMstSleAreas = _mstSleAreasRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Status.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.IsNoneAssignment.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsNoneAssignmentFilter), e => e.IsNoneAssignment == input.IsNoneAssignmentFilter)
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

			var pagedAndFilteredMstSleAreas = filteredMstSleAreas
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstSleAreas = from o in pagedAndFilteredMstSleAreas
							  select new GetMstSleAreasForViewDto()
							  {
								  MstSleAreas = new MstSleAreasDto
								  {
									  Name = o.Name,
									  Status = o.Status,
									  Description = o.Description,
									  Ordering = o.Ordering,
									  IsNoneAssignment = o.IsNoneAssignment,
									  CreationTime = o.CreationTime,
									  IsDeleted = o.IsDeleted,
									  DeletionTime = o.DeletionTime,
									  YardId = o.YardId,
									  Id = o.Id
								  }
							  };

			var totalCount = await filteredMstSleAreas.CountAsync();

			return new PagedResultDto<GetMstSleAreasForViewDto>(
				totalCount,
				await mstSleAreas.ToListAsync()
			);
		}

		public async Task<PagedResultDto<GetMstSleAreasForViewDto>> GetAreasByYardId(long Id)
		{
				var filteredMstSleAreas = _mstSleAreasRepository.GetAll().Where(r => r.YardId == Id);
				var pagedAndFilteredMstSleAreas = filteredMstSleAreas;
				var mstSleAreas = from o in pagedAndFilteredMstSleAreas
								  select new GetMstSleAreasForViewDto()
								  {
									  MstSleAreas = new MstSleAreasDto
									  {
										  Name = o.Name,
										  Status = o.Status,
										  Description = o.Description,
										  Ordering = o.Ordering,
										  IsNoneAssignment = o.IsNoneAssignment,
										  CreationTime = o.CreationTime,
										  IsDeleted = o.IsDeleted,
										  DeletionTime = o.DeletionTime,
										  YardId = o.YardId,
										  Id = o.Id
									  }
								  };

				 var totalCount = await filteredMstSleAreas.CountAsync();

				return new PagedResultDto<GetMstSleAreasForViewDto>(
					totalCount,
					await mstSleAreas.ToListAsync()
				);

		}

		public async Task<GetMstSleAreasForViewDto> GetMstSleAreasForView(long id)
		{
			var mstSleAreas = await _mstSleAreasRepository.GetAsync(id);

			var output = new GetMstSleAreasForViewDto { MstSleAreas = ObjectMapper.Map<MstSleAreasDto>(mstSleAreas) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleAreas_Edit)]
		public async Task<GetMstSleAreasForEditOutput> GetMstSleAreasForEdit(EntityDto<long> input)
		{
			var mstSleAreas = await _mstSleAreasRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstSleAreasForEditOutput { MstSleAreas = ObjectMapper.Map<CreateOrEditMstSleAreasDto>(mstSleAreas) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstSleAreasDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleAreas_Create)]
		protected virtual async Task Create(CreateOrEditMstSleAreasDto input)
		{
			var mstSleAreas = ObjectMapper.Map<MstSleAreas>(input);


			if (AbpSession.TenantId != null)
			{
				mstSleAreas.TenantId = (int)AbpSession.TenantId;
			}


			await _mstSleAreasRepository.InsertAsync(mstSleAreas);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleAreas_Edit)]
		protected virtual async Task Update(CreateOrEditMstSleAreasDto input)
		{
			var mstSleAreas = await _mstSleAreasRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstSleAreas);
		}

		[AbpAuthorize(AppPermissions.Pages_MstSleAreas_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstSleAreasRepository.DeleteAsync(input.Id);
		}
	}
}