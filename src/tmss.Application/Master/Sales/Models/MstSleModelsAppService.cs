﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Sales.Models;
using tmss.Master.Sales.Models.Dto;
using Task = System.Threading.Tasks.Task;

namespace tmss.Master.Sales
{
    [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model)]
    public class MstSleModelsAppService : tmssAppServiceBase, IMstSleModelsAppService
    {
        private readonly IRepository<MstSleModels, long> _mstSleModelsRepository;
        public MstSleModelsAppService(IRepository<MstSleModels, long> mstSleModelsRepository)
        {
            _mstSleModelsRepository = mstSleModelsRepository;
        }
        public async Task<PagedResultDto<GetMstSleModelsForViewDto>> GetAll(GetAllMstSleModelsInput input)
        {
            var filteredMstSleModels = _mstSleModelsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.MarketingCode.Contains(input.Filter) || e.ProductionCode.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Status.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MarketingCodeFilter), e => e.MarketingCode == input.MarketingCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductionCodeFilter), e => e.ProductionCode == input.ProductionCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName == input.VnNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName == input.EnNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                        .WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
                        .WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
                        .WhereIf(input.MinOrderingRptFilter != null, e => e.OrderingRpt >= input.MinOrderingRptFilter)
                        .WhereIf(input.MaxOrderingRptFilter != null, e => e.OrderingRpt <= input.MaxOrderingRptFilter)
                        .WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
                        .WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
                        .WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
                        .WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
                        .WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter);

            var pagedAndFilteredMstSleModels = filteredMstSleModels
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var mstSleModels = from o in pagedAndFilteredMstSleModels
                               select new GetMstSleModelsForViewDto()
                               {
                                   MstSleModels = new MstSleModelsDto
                                   {
                                       ProductionCode = o.ProductionCode,
                                       MarketingCode = o.MarketingCode,
                                       VnName = o.VnName,
                                       EnName = o.EnName,
                                       Description = o.Description,
                                       Status = o.Status,
                                       Ordering = o.Ordering,
                                       OrderingRpt = o.OrderingRpt,
                                       CreationTime = o.CreationTime,
                                       DeletionTime = o.DeletionTime,
                                       LastModificationtTime = o.LastModificationTime,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredMstSleModels.CountAsync();

            return new PagedResultDto<GetMstSleModelsForViewDto>(
                totalCount,
                await mstSleModels.ToListAsync()
            );
        }
        public async Task CreateOrEdit(CreateOrEditMstSleModelsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_CreateEdit)]
        protected virtual async Task Create(CreateOrEditMstSleModelsDto input)
        {
            var mstSleModels = ObjectMapper.Map<MstSleModels>(input);

                mstSleModels.TenantId = (int)AbpSession.TenantId;
            await _mstSleModelsRepository.InsertAsync(mstSleModels);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_CreateEdit)]
        protected virtual async Task Update(CreateOrEditMstSleModelsDto input)
        {
            var mstSleModels = await _mstSleModelsRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleModels);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            await _mstSleModelsRepository.DeleteAsync(input.Id);
        }

        public async Task<GetMstSleModelsForEditOutput> GetMstSleModelsForEdit(EntityDto<long> input)
        {
            var mstSleModels = await _mstSleModelsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleModelsForEditOutput { MstSleModels = ObjectMapper.Map<CreateOrEditMstSleModelsDto>(mstSleModels) };

            return output;
        }

        public async Task<GetMstSleModelsForViewDto> GetMstSleModelsForView(long id)
        {
            var mstSleModels = await _mstSleModelsRepository.GetAsync(id);

            var output = new GetMstSleModelsForViewDto { MstSleModels = ObjectMapper.Map<MstSleModelsDto>(mstSleModels) };

            return output;
        }
    }
}
