﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

using tmss.Master.Sales.Dtos;
using tmss.Dto;
using Abp.Application.Services.Dto;
using tmss.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace tmss.Master.Sales
{
	[AbpAuthorize(AppPermissions.Pages_MstGenDealers)]
	public class MstGenDealersAppService : tmssAppServiceBase, IMstGenDealersAppService
	{
		private readonly IRepository<MstGenDealers, long> _mstGenDealersRepository;


		public MstGenDealersAppService(IRepository<MstGenDealers, long> mstGenDealersRepository)
		{
			_mstGenDealersRepository = mstGenDealersRepository;

		}

		public async Task<PagedResultDto<GetMstGenDealersForViewDto>> GetAll(GetAllMstGenDealersInput input)
		{

			var filteredMstGenDealers = _mstGenDealersRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.AccountNo.Contains(input.Filter) || e.Bank.Contains(input.Filter) || e.BankAddress.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Abbreviation.Contains(input.Filter) || e.ContactPerson.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Status.Contains(input.Filter) || e.Fax.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.IsSpecial.Contains(input.Filter) || e.TaxCode.Contains(input.Filter) || e.IsSumDealer.Contains(input.Filter) || e.BiServer.Contains(input.Filter) || e.TfsAmount.Contains(input.Filter) || e.PartLeadtime.Contains(input.Filter) || e.IpAddress.Contains(input.Filter) || e.Islexus.Contains(input.Filter) || e.IsSellLexusPart.Contains(input.Filter) || e.IsDlrSales.Contains(input.Filter) || e.RecievingAddress.Contains(input.Filter) || e.DlrFooter.Contains(input.Filter) || e.IsPrint.Contains(input.Filter) || e.PasswordSearchVin.Contains(input.Filter))
						.WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
						.WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
						.WhereIf(input.MinLastModificationTimeFilter != null, e => e.LastModificationTime >= input.MinLastModificationTimeFilter)
						.WhereIf(input.MaxLastModificationTimeFilter != null, e => e.LastModificationTime <= input.MaxLastModificationTimeFilter)
						.WhereIf(input.IsDeletedFilter > -1, e => (input.IsDeletedFilter == 1 && e.IsDeleted) || (input.IsDeletedFilter == 0 && !e.IsDeleted))
						.WhereIf(input.MinDeletionTimeFilter != null, e => e.DeletionTime >= input.MinDeletionTimeFilter)
						.WhereIf(input.MaxDeletionTimeFilter != null, e => e.DeletionTime <= input.MaxDeletionTimeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AccountNoFilter), e => e.AccountNo == input.AccountNoFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.BankFilter), e => e.Bank == input.BankFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.BankAddressFilter), e => e.BankAddress == input.BankAddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName == input.VnNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName == input.EnNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AbbreviationFilter), e => e.Abbreviation == input.AbbreviationFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ContactPersonFilter), e => e.ContactPerson == input.ContactPersonFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.FaxFilter), e => e.Fax == input.FaxFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsSpecialFilter), e => e.IsSpecial == input.IsSpecialFilter)
						.WhereIf(input.MinOrderingFilter != null, e => e.Ordering >= input.MinOrderingFilter)
						.WhereIf(input.MaxOrderingFilter != null, e => e.Ordering <= input.MaxOrderingFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.TaxCodeFilter), e => e.TaxCode == input.TaxCodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsSumDealerFilter), e => e.IsSumDealer == input.IsSumDealerFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.BiServerFilter), e => e.BiServer == input.BiServerFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.TfsAmountFilter), e => e.TfsAmount == input.TfsAmountFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PartLeadtimeFilter), e => e.PartLeadtime == input.PartLeadtimeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IpAddressFilter), e => e.IpAddress == input.IpAddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IslexusFilter), e => e.Islexus == input.IslexusFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsSellLexusPartFilter), e => e.IsSellLexusPart == input.IsSellLexusPartFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsDlrSalesFilter), e => e.IsDlrSales == input.IsDlrSalesFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.RecievingAddressFilter), e => e.RecievingAddress == input.RecievingAddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DlrFooterFilter), e => e.DlrFooter == input.DlrFooterFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.IsPrintFilter), e => e.IsPrint == input.IsPrintFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PasswordSearchVinFilter), e => e.PasswordSearchVin == input.PasswordSearchVinFilter)
						.WhereIf(input.MinPortRegionFilter != null, e => e.PortRegion >= input.MinPortRegionFilter)
						.WhereIf(input.MaxPortRegionFilter != null, e => e.PortRegion <= input.MaxPortRegionFilter);

			var pagedAndFilteredMstGenDealers = filteredMstGenDealers
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var mstGenDealers = from o in pagedAndFilteredMstGenDealers
								select new GetMstGenDealersForViewDto()
								{
									MstGenDealers = new MstGenDealersDto
									{
										CreationTime = o.CreationTime,
										LastModificationTime = o.LastModificationTime,
										IsDeleted = o.IsDeleted,
										DeletionTime = o.DeletionTime,
										Code = o.Code,
										AccountNo = o.AccountNo,
										Bank = o.Bank,
										BankAddress = o.BankAddress,
										VnName = o.VnName,
										EnName = o.EnName,
										Address = o.Address,
										Abbreviation = o.Abbreviation,
										ContactPerson = o.ContactPerson,
										Phone = o.Phone,
										Status = o.Status,
										Fax = o.Fax,
										Description = o.Description,
										IsSpecial = o.IsSpecial,
										Ordering = o.Ordering,
										TaxCode = o.TaxCode,
										IsSumDealer = o.IsSumDealer,
										BiServer = o.BiServer,
										TfsAmount = o.TfsAmount,
										PartLeadtime = o.PartLeadtime,
										IpAddress = o.IpAddress,
										Islexus = o.Islexus,
										IsSellLexusPart = o.IsSellLexusPart,
										IsDlrSales = o.IsDlrSales,
										RecievingAddress = o.RecievingAddress,
										DlrFooter = o.DlrFooter,
										IsPrint = o.IsPrint,
										PasswordSearchVin = o.PasswordSearchVin,
										PortRegion = o.PortRegion,
										Id = o.Id
									}
								};

			var totalCount = await filteredMstGenDealers.CountAsync();

			return new PagedResultDto<GetMstGenDealersForViewDto>(
				totalCount,
				await mstGenDealers.ToListAsync()
			);
		}

		public async Task<GetMstGenDealersForViewDto> GetMstGenDealersForView(long id)
		{
			var mstGenDealers = await _mstGenDealersRepository.GetAsync(id);

			var output = new GetMstGenDealersForViewDto { MstGenDealers = ObjectMapper.Map<MstGenDealersDto>(mstGenDealers) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenDealers_Edit)]
		public async Task<GetMstGenDealersForEditOutput> GetMstGenDealersForEdit(EntityDto<long> input)
		{
			var mstGenDealers = await _mstGenDealersRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMstGenDealersForEditOutput { MstGenDealers = ObjectMapper.Map<CreateOrEditMstGenDealersDto>(mstGenDealers) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMstGenDealersDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenDealers_Create)]
		protected virtual async Task Create(CreateOrEditMstGenDealersDto input)
		{
			var mstGenDealers = ObjectMapper.Map<MstGenDealers>(input);


			if (AbpSession.TenantId != null)
			{
				mstGenDealers.TenantId = (int)AbpSession.TenantId;
			}


			await _mstGenDealersRepository.InsertAsync(mstGenDealers);
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenDealers_Edit)]
		protected virtual async Task Update(CreateOrEditMstGenDealersDto input)
		{
			var mstGenDealers = await _mstGenDealersRepository.FirstOrDefaultAsync((long)input.Id);
			ObjectMapper.Map(input, mstGenDealers);
		}

		[AbpAuthorize(AppPermissions.Pages_MstGenDealers_Delete)]
		public async Task Delete(EntityDto<long> input)
		{
			await _mstGenDealersRepository.DeleteAsync(input.Id);
		}

	}
}