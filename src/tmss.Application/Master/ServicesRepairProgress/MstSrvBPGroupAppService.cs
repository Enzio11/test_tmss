﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using tmss.Authorization;
using Abp.Authorization;
using Abp.UI;
using tmss.Master.ServicesRepairProgress.BPGroup;
using tmss.Master.ServicesRepairProgress.BPGroup.Dto;

namespace tmss.Master.ServicesRepairProgress
{
  [AbpAuthorize(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup)]
  public class MstSrvBPGroupAppService : tmssAppServiceBase, IMstSrvBPGroupAppService
  {
    private readonly IRepository<MstSrvBPGroup> _BPGroupRepository;
    public MstSrvBPGroupAppService(IRepository<MstSrvBPGroup> BPGroupRepository)
    {
      _BPGroupRepository = BPGroupRepository;
    }

    [AbpAuthorize(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_List)]
    public async Task<PagedResultDto<BPGroupListDto>> GetBPGroups(GetBPGroupsInput input)
    {
      var query = _BPGroupRepository
                  .GetAll()
                  .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.GroupName.Contains(input.Filter))
                  .Where(p => p.IsActive == input.IsActive);
      var BPGroupCount = await query.CountAsync();
      var BPGroups = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

      return new PagedResultDto<BPGroupListDto>(
        BPGroupCount,
        ObjectMapper.Map<List<BPGroupListDto>>(BPGroups)
      );
    }

    [AbpAuthorize(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_CreateEdit)]
    public async Task CreateOrUpdateBPGroup(CreateOrUpdateBPGroupInput input)
    {
      if (input.Id.HasValue)
      {
        var existingOrdering = await _BPGroupRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering && x.Id != input.Id);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var BPGroup = await _BPGroupRepository.GetAsync(input.Id.Value);
        BPGroup.GroupName = input.GroupName;
        BPGroup.GroupDesc = input.GroupDesc;
        BPGroup.Ordering = input.Ordering;
        BPGroup.IsActive = input.IsActive;
        await _BPGroupRepository.UpdateAsync(BPGroup);
      }
      else
      {
        var existingOrdering = await _BPGroupRepository.FirstOrDefaultAsync(x => x.Ordering == input.Ordering);
        if (existingOrdering != null)
        {
          throw new UserFriendlyException(L("MstExistingOrdering"));
        }

        var BPGroup = ObjectMapper.Map<MstSrvBPGroup>(input);
        await _BPGroupRepository.InsertAsync(BPGroup);
      }
    }

    [AbpAuthorize(AppPermissions.Pages_Master_ServicesRepairProgress_BPGroup_Delete)]
    public async Task DeleteBPGroup(EntityDto<int> input)
    {
      await _BPGroupRepository.DeleteAsync(input.Id);
    }

    public async Task<GetBPGroupForEditOutput> GetBPGroupForEdit(NullableIdDto<int> input)
    {
      var BPGroup = await _BPGroupRepository.GetAsync(input.Id.Value);
      return ObjectMapper.Map<GetBPGroupForEditOutput>(BPGroup);
    }

    public async Task<int> GetMaxOrdering()
    {
      var maxOrdering = await _BPGroupRepository.GetAll().OrderByDescending(x => x.Ordering).FirstOrDefaultAsync();
      if (maxOrdering != null)
        return maxOrdering.Ordering + 1;
      else
        return 1;
    }
  }
}