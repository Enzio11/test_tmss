﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Services.DeskAdvisor;
using tmss.Master.Services.DeskAdvisor.Dto;
using tmss.Master.Services.Employee.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_DeskAdvisor)]
    public class MstSrvDeskAdvisorAppService : tmssAppServiceBase, IMstSrvDeskAdvisorAppService
    {
        private readonly IRepository<MstSrvDeskAdvisor, long> _repo;
        private readonly IRepository<MstSrvEmployee, long> _empRepo;

        public MstSrvDeskAdvisorAppService(IRepository<MstSrvDeskAdvisor, long> repo, IRepository<MstSrvEmployee, long> empRepo)
        {
            _repo = repo;
            _empRepo = empRepo;
        }
        public async Task CreateOrEdit(CreateOrEditDeskAdvisorDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_DeskAdvisor_CreateEdit)]
        private async Task Update(CreateOrEditDeskAdvisorDto input)
        {
            //Check BankCode is exists or not
            var deskAdvisor = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, deskAdvisor);
            await _repo.UpdateAsync(deskAdvisor);
        }

        //CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_DeskAdvisor_CreateEdit)]
        private async Task Create(CreateOrEditDeskAdvisorDto input)
        {

            var deskAdvisorExists = _repo.FirstOrDefaultAsync(o => o.DeskName == input.DeskName && o.Status == "Y");
            if (deskAdvisorExists.Result != null)
            {
                throw new UserFriendlyException(L("DataIsAlreadyExisted"));
            }

            var deskAdvisor = ObjectMapper.Map<MstSrvDeskAdvisor>(input);
            deskAdvisor.TenantId = AbpSession.TenantId.Value;
            deskAdvisor.Advisor = null;
                await _repo.InsertAsync(deskAdvisor);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Services_DeskAdvisor_Delete)]
        public async Task Delete(EntityDto input)
        {
            var deskAdvisor = await _repo.FirstOrDefaultAsync((long)input.Id);
            await _repo.DeleteAsync(deskAdvisor);
            // await _repo.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetDeskAdvisorForViewDto>> GetAll(GetAllDeskAdvisorInput input)
        {

            var filteredDeskAdvisors = _repo.GetAll();

            var pageAndFilteredDeskAdvisors = filteredDeskAdvisors.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var deskAdvisors = from o in pageAndFilteredDeskAdvisors
                        select new GetDeskAdvisorForViewDto()
                        {
                            DeskAdvisor = new DeskAdvisorDto
                            {
                                Id = o.Id,
                                DeskName = o.DeskName,
                                AdvisorId = o.AdvisorId,
                                AdvisorCode = o.Advisor.EmpCode,
                                AdvisorName = o.Advisor.EmpName,
                                Description = o.Description,
                                Status = o.Status
                            }
                        };

            var totalCount = await filteredDeskAdvisors.CountAsync();

            return new PagedResultDto<GetDeskAdvisorForViewDto>(
                totalCount,
                await deskAdvisors.ToListAsync()
                );
        }

        //Get By Id or Edit
        [AbpAuthorize(AppPermissions.Pages_Master_Services_DeskAdvisor_CreateEdit)]
        public async Task<GetDeskAdvisorForEditOutput> GetDeskAdvisorForEdit(EntityDto input)
        {
           var deskAdvisor = await _repo.FirstOrDefaultAsync((long)input.Id);

            var output = new GetDeskAdvisorForEditOutput { DeskAdvisor = ObjectMapper.Map<CreateOrEditDeskAdvisorDto>(deskAdvisor) };
            return output;
        }

        public async Task<GetDeskAdvisorForViewDto> GetDeskAdvisorForView(int id)
        {
            var deskAdvisor = await _repo.GetAsync(id);
            var output = new GetDeskAdvisorForViewDto { DeskAdvisor = ObjectMapper.Map<DeskAdvisorDto>(deskAdvisor) };
            return output;
        }

        public Task<List<EmployeeDto>> getAllAdvisorsByTenantId()
        {
            var emps = _empRepo.GetAll();

            var employees = from o in emps
                            select new EmployeeDto
                            {
                                Id = o.Id,
                                EmpCode = o.EmpCode,
                                EmpName = o.EmpName
                            };

            return employees.ToListAsync();
        }
    }
}
