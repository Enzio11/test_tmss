﻿
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Services.Division;
using tmss.Master.Services.Division.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Division)]
    public class MstSrvDivisionAppService : tmssAppServiceBase, IMstSrvDivisionAppService
    {
        private readonly IRepository<MstSrvDivision, long> _repo;
        public MstSrvDivisionAppService(IRepository<MstSrvDivision, long> repo) {
            _repo = repo;
        }
        public async Task CreateOrEdit(CreateOrEditDivisionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //Edit
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Division_CreateEdit)]
        private async Task Update(CreateOrEditDivisionDto input)
        {

            var div = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, div);
            await _repo.UpdateAsync(div);
        }

        //Create
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Division_CreateEdit)]
        private async Task Create(CreateOrEditDivisionDto input)
        {
            var divExist = _repo.FirstOrDefaultAsync(o => o.DivCode == input.DivCode);
            if (divExist.Result != null)
            {
                throw new UserFriendlyException(L("DataIsAlreadyExisted"));
            }
            //Check DivCode is exists or not
            var div = ObjectMapper.Map<MstSrvDivision>(input);
            div.TenantId = AbpSession.TenantId.Value;
            div.Status = "Y";

            await _repo.InsertAsync(div);
        }

        public async Task Delete(EntityDto input)
        {
            var div = await _repo.FirstOrDefaultAsync((long)input.Id);
            await _repo.DeleteAsync(div);
        }

        //Get All division
        public Task<List<DivisionDto>> GetAll()
        {
         
            var divs = _repo.GetAll();

            var divisions = from o in divs
                            select new DivisionDto
                            {
                                Id = o.Id,
                                DivCode = o.DivCode,
                                DivName = o.DivName,
                                ParentDivId = o.ParentDiv.Id,
                                ParentDivName = o.ParentDiv.DivName
                            };

            return divisions.ToListAsync();

        }

        //Create
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Division_CreateEdit)]
        public async Task<GetDivisionForEditOutput> GetDivisionForEdit(EntityDto input)
        {
            var div = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetDivisionForEditOutput { Division = ObjectMapper.Map<CreateOrEditDivisionDto>(div) };

            return output;
        }

        public async Task<GetDivisionForViewDto> GetDivisionForView(int id)
        {
            var div = await _repo.GetAsync(id);
            var output = new GetDivisionForViewDto { Division = ObjectMapper.Map<DivisionDto>(div) };

            return output;
        }
    }
}
