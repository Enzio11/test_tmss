﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Services.Floor;
using tmss.Master.Services.Floor.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Floor)]
    public class MstSrvFloorAppService : tmssAppServiceBase, IMstSrvFloorAppService
    {
        private readonly IRepository<MstSrvFloor, long> _repo;
        public MstSrvFloorAppService(IRepository<MstSrvFloor, long> repo)
        {
            _repo = repo;
        }
        public async Task CreateOrEdit(CreateOrEditFloorDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Floor_CreateEdit)]
        private async Task Create(CreateOrEditFloorDto input)
        {
            //Check floor name exists or not
            var floorExists = _repo.FirstOrDefaultAsync(o => o.FloorName == input.FloorName);
            if (floorExists.Result != null)
            {
                throw new UserFriendlyException(L("DataIsAlreadyExisted"));
            }
            var floor = ObjectMapper.Map<MstSrvFloor>(input);
            floor.TenantId = AbpSession.TenantId.Value;
            floor.Status = "Y";

            await _repo.InsertAsync(floor);
        }

        //EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Floor_CreateEdit)]
        private async Task Update(CreateOrEditFloorDto input)
        {
            var floor = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, floor);
            await _repo.UpdateAsync(floor);
        }

        public async Task<PagedResultDto<GetFloorForViewDto>> GetAll(GetAllFloorInput input)
        {
            var filteredFlrs = _repo.GetAll();

            var pageAndFilteredFlrs = filteredFlrs.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var floors = from o in pageAndFilteredFlrs
                        select new GetFloorForViewDto()
                        {
                            Floor = new FloorDto
                            {
                                Id = o.Id,
                                FloorName = o.FloorName,
                                Description = o.Description,
                                Status = o.Status,
                                Type = o.Type
                            }
                        };

            var totalCount = await filteredFlrs.CountAsync();

            return new PagedResultDto<GetFloorForViewDto>(
                totalCount,
                await floors.ToListAsync()
                );
        }

        // Get by ID for EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Floor_CreateEdit)]
        public async Task<GetFloorForEditOuput> GetFloorForEdit(EntityDto input)
        {
            var floor = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetFloorForEditOuput { Floor = ObjectMapper.Map<CreateOrEditFloorDto>(floor) };

            return output;
        }

        public async Task<GetFloorForViewDto> GetFloorForView(int id)
        {
            var floor = await _repo.FirstOrDefaultAsync((long)id);

            var output = new GetFloorForViewDto { Floor = ObjectMapper.Map<FloorDto>(floor) };

            return output;
        }
    }
}
