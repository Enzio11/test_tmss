﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master.Services;
using tmss.Master.Services.Supplier;
using tmss.Master.Services.Supplier.Dto;

namespace tmss.Master.Common
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Supplier )]
    public class MstSrvSupplierAppService : tmssAppServiceBase, IMstSrvSupplierAppService
    {
        private readonly IRepository<MstSrvSupplier, long> _repo;
        public MstSrvSupplierAppService(IRepository<MstSrvSupplier, long> repo)
        {
            _repo = repo;
        }
        public async Task CreateOrEdit(CreateOrEditSupplierDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Supplier_CreateEdit)]
        private async Task Update(CreateOrEditSupplierDto input)
        {
            var supplier = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, supplier);

            await _repo.UpdateAsync(supplier);
        }

        //CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Supplier_CreateEdit)]
        private async Task Create(CreateOrEditSupplierDto input)
        {
            //Check SupplierCode is exists or not
            var supIsExist = _repo.FirstOrDefaultAsync(e => e.SupplierCode == input.SupplierCode && e.Status == "Y");
            if (supIsExist.Result != null)
            {
                throw new UserFriendlyException(L("SupplierCodeIsExisted"));
            }
            var supplier = ObjectMapper.Map<MstSrvSupplier>(input);
            supplier.Status = "Y";
            await _repo.InsertAsync(supplier);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Services_Supplier_Delete)]
        public async Task Delete(EntityDto input)
        {
            var supplier = await _repo.FirstOrDefaultAsync((long)input.Id);
            await _repo.DeleteAsync(supplier);
        }

        public async Task<PagedResultDto<GetSupplierForViewDto>> GetAll(GetAllSupplierInput input)
        {
            var filteredSuppliers = _repo.GetAll()
                .Where(e => e.Status.Contains("Y") && e.TenantId == AbpSession.TenantId.Value);

            var pageAndFilteredSuppliers = filteredSuppliers.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var suppliers = from o in pageAndFilteredSuppliers
                            select new GetSupplierForViewDto()
                            {
                                Supplier = new SupplierDto
                                {
                                    Id = o.Id,
                                    SupplierCode = o.SupplierCode,
                                    Address = o.Address,
                                    SupplierName = o.SupplierName,
                                    Tel = o.Tel,
                                    Fax = o.Fax,
                                    LeadTime = o.LeadTime,
                                    Taxcode = o.Taxcode,
                                    CountryId = o.CountryId,
                                    Email = o.Email,
                                    AccNo = o.AccNo,
                                    BankId = o.BankId,
                                    Pic = o.Pic,
                                    PicMobi = o.PicMobi,
                                    PicTel = o.PicTel
                                }
                            };

            var totalCount = await filteredSuppliers.CountAsync();

            return new PagedResultDto<GetSupplierForViewDto>(
                totalCount,
                await suppliers.ToListAsync()
                );
        }
        // Get by ID for EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Supplier )]
        public async Task<GetSupplierForEditOutputDto> GetBankForEdit(EntityDto input)
        {
            var supplier = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetSupplierForEditOutputDto { Supplier = ObjectMapper.Map<CreateOrEditSupplierDto>(supplier) };

            return output;
        }

        public async Task<GetSupplierForViewDto> GetBankForView(int id)
        {
            var supplier = await _repo.GetAsync(id);
            var output = new GetSupplierForViewDto { Supplier = ObjectMapper.Map<SupplierDto>(supplier) };

            return output;
        }
    }
}
