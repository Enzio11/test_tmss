﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Core.Master.Services;
using tmss.Master.Services.Bank;
using tmss.Master.Services.Bank.Dto;

namespace tmss.Master.Services
{
    [AbpAuthorize(AppPermissions.Pages_Master_Services_Bank)]
    public class MstSrvBankAppService : tmssAppServiceBase, IMstSrvBankAppService
    {
        private readonly IRepository<MstSrvBank, long> _repo;

        public MstSrvBankAppService(IRepository<MstSrvBank, long> repo)
        {
            _repo = repo;
        }
        public async Task CreateOrEdit(CreateOrEditBankDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Bank_CreateEdit)]
        private async Task Update(CreateOrEditBankDto input)
        {
            var bank = await _repo.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, bank);
            await _repo.UpdateAsync(bank);
        }

        // CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Bank_CreateEdit)]
        private async Task Create(CreateOrEditBankDto input)
        {
            var bankExists = _repo.FirstOrDefaultAsync(o => o.BankCode == input.BankCode && o.Status == "Y");
            if (bankExists.Result != null)
            {
                throw new UserFriendlyException(L("DataIsAlreadyExisted"));
            }
            //Check BankCode is exists or not
            var bank = ObjectMapper.Map<MstSrvBank>(input);
            bank.TenantId = AbpSession.TenantId.Value;
            bank.Status = "Y";

            await _repo.InsertAsync(bank);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Services_Bank_Delete)]
        public async Task Delete(EntityDto input)
        {
            var bank = await _repo.FirstOrDefaultAsync((long)input.Id);
            bank.Status = "N";
            await _repo.UpdateAsync(bank);
            // await _repo.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetBankForViewDto>> GetAll(GetAllBankInput input)
        {

            var filteredBanks = _repo.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.BankCode.Contains(input.Filter) || e.BankName.Contains(input.Filter))
                .Where(e => e.Status.Contains("Y"));

            var pageAndFilteredBanks = filteredBanks.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var banks = from o in pageAndFilteredBanks
                        select new GetBankForViewDto()
                        {
                            Bank = new MstSrvBankDto
                            {
                                Id = o.Id,
                                BankCode = o.BankCode,
                                BankName = o.BankName,
                                BankAdd = o.BankAdd,
                                Tel = o.Tel,
                                Fax = o.Fax,
                                Email = o.Email
                            }
                        };

            var totalCount = await filteredBanks.CountAsync();

            return new PagedResultDto<GetBankForViewDto>(
                totalCount,
                await banks.ToListAsync()
                );
        }

        // Get by ID for EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_Services_Bank_CreateEdit)]
        public async Task<GetBankForEditOutput> GetBankForEdit(EntityDto input)
        {
            var bank = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetBankForEditOutput { Bank = ObjectMapper.Map<CreateOrEditBankDto>(bank) };

            return output;
        }

        public async Task<GetBankForViewDto> GetBankForView(int id)
        {
            var floor = await _repo.GetAsync(id);
            var output = new GetBankForViewDto { Bank = ObjectMapper.Map<MstSrvBankDto>(floor) };

            return output;
        }
    }
}
