﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Core.Master.ServicesPart;
using tmss.Master.ServicesPart.PartsType;
using tmss.Master.ServicesPart.PartsType.Dto;

namespace tmss.Master.ServicesPart
{
    [AbpAuthorize(AppPermissions.Pages_Master_ServicesPart_PartsType)]
    public class MstSrvPartsTypeAppService : tmssAppServiceBase, IMstSrvPartsTypeAppService
    {
        private readonly IRepository<MstSrvPartsType, long> _repo;
        public MstSrvPartsTypeAppService(IRepository<MstSrvPartsType, long> repo)
        {
            _repo = repo;
        }
        public async Task CreateOrEdit(CreateOrEditPartsTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // CREATE
        [AbpAuthorize(AppPermissions.Pages_Master_ServicesPart_PartsType_CreateEdit)]
        private async Task Create(CreateOrEditPartsTypeDto input)
        {
            //Check GTypeCode is exists or not
            var prtTypeExists = _repo.FirstOrDefaultAsync(e => e.GTypeCode == input.GTypeCode && e.Status == "Y");
            if (prtTypeExists.Result != null)
            {
                throw new UserFriendlyException(L("TypeCodeIsExisted"));
            }
            var partsType = ObjectMapper.Map<MstSrvPartsType>(input);
            partsType.Status = "Y";
            await _repo.InsertAsync(partsType);
        }

        // EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_ServicesPart_PartsType_CreateEdit)]
        private async Task Update(CreateOrEditPartsTypeDto input)
        {
            //Check GTypeCode is exists or not
            var prtTypeExists = _repo.FirstOrDefaultAsync(e => e.GTypeCode == input.GTypeCode && e.Status == "Y");
            if (prtTypeExists.Result != null)
            {
                throw new UserFriendlyException(L("TypeCodeIsExisted"));
            }
            var partsType = ObjectMapper.Map<MstSrvPartsType>(input);
            partsType.Status = "Y";
            await _repo.UpdateAsync(partsType);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_ServicesPart_PartsType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var partType = await _repo.FirstOrDefaultAsync((long)input.Id);
            await _repo.DeleteAsync(partType);
        }

        public async Task<PagedResultDto<GetPartsTypeForViewDto>> GetAll(GetAllPartsTypeInput input)
        {


            var filteredPartTypes = _repo.GetAll();

            var pageAndFilteredPrtTypes = filteredPartTypes.OrderBy(input.Sorting ?? "id asc")
                                                                      .PageBy(input);

            var partTypes = from o in pageAndFilteredPrtTypes
                            select new GetPartsTypeForViewDto()
                            {
                                PartsType = new PartsTypeDto
                                {
                                    Id = o.Id,
                                    GTypeCode = o.GTypeCode,
                                    GTypeName = o.GTypeName,
                                    Remark = o.Remark
                                }
                            };

            var totalCount = await filteredPartTypes.CountAsync();

            return new PagedResultDto<GetPartsTypeForViewDto>(
                totalCount,
                await partTypes.ToListAsync()
                );
        }

        // Get by ID for EDIT
        [AbpAuthorize(AppPermissions.Pages_Master_ServicesPart_PartsType_CreateEdit)]
        public async Task<GetPartsTypeForEditOutput> GetPartsTypeForEdit(EntityDto input)
        {
            var partType = await _repo.FirstOrDefaultAsync(input.Id);

            var output = new GetPartsTypeForEditOutput { PartsType = ObjectMapper.Map<CreateOrEditPartsTypeDto>(partType) };

            return output;
        }

        public async Task<GetPartsTypeForViewDto> GetPartsTypeForView(int id)
        {
            var partType = await _repo.GetAsync(id);
            var output = new GetPartsTypeForViewDto { PartsType = ObjectMapper.Map<PartsTypeDto>(partType) };

            return output;
        }
    }
}
