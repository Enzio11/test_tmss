﻿using System.Collections.Generic;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using tmss.Dto;
using tmss.DataExporting.Excel.NPOI;
using Abp.Timing.Timezone;
using Abp.Runtime.Session;
using tmss.Storage;

namespace tmss.Services.Master.OrderMethod
{
    public class OrderMethodExcelExporter : NpoiExcelExporterBase, IOrderMethodExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public OrderMethodExcelExporter(ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetOrderMethodForViewDto> orderMethods)
        {
            return null;
            //return CreateExcelPackage(
            //    "OrderMethods.xlsx",
            //    excelPackage =>
            //    {
            //        var sheet = excelPackage.Workbook.Worksheets.Add(L("OrderMethods"));
            //        sheet.OutLineApplyStyle = true;

            //        AddHeader(
            //            sheet,
            //            L("Code"),
            //            L("Name")
            //            );

            //        AddObjects(
            //            sheet, 2, orderMethods,
            //            _ => _.OrderMethod.Code,
            //            _ => _.OrderMethod.Name
            //            );



            //    });
        }
    }
}
