using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityParameters;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using tmss.Auditing.Dto;
using tmss.Authorization.Accounts.Dto;
using tmss.Authorization.Delegation;
using tmss.Authorization.Permissions.Dto;
using tmss.Authorization.Roles;
using tmss.Authorization.Roles.Dto;
using tmss.Authorization.Users;
using tmss.Authorization.Users.Delegation.Dto;
using tmss.Authorization.Users.Dto;
using tmss.Authorization.Users.Importing.Dto;
using tmss.Authorization.Users.Profile.Dto;
using tmss.Chat;
using tmss.Chat.Dto;
using tmss.Core.Master.Services;
using tmss.Core.Master.ServicesPart;
using tmss.Core.Services.Master.OrderMethod;
using tmss.DynamicEntityParameters.Dto;
using tmss.Editions;
using tmss.Editions.Dto;
using tmss.Friendships;
using tmss.Friendships.Cache;
using tmss.Friendships.Dto;
using tmss.Localization.Dto;
using tmss.Master;
using tmss.Master.Sale;
using tmss.Master.Sales;
using tmss.Master.Sales.DealerGroup.Dto;
using tmss.Master.Sales.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Master.Sales.LogisticCompany.Dto;
using tmss.Master.Sales.Models.Dto;
using tmss.Master.Sales.Provinces.Dto;
using tmss.Master.Services;
using tmss.Master.Services.Bank.Dto;
using tmss.Master.Services.CustomerType.Dto;
using tmss.Master.Services.DeskAdvisor.Dto;
using tmss.Master.Services.Division.Dto;
using tmss.Master.Services.Employee.Dto;
using tmss.Master.Services.Floor.Dto;
using tmss.Master.Services.Footer.Dto;
using tmss.Master.Services.Supplier.Dto;
using tmss.Master.Services.Title;
using tmss.Master.ServicesPart.PartsType.Dto;
using tmss.MultiTenancy;
using tmss.MultiTenancy.Dto;
using tmss.MultiTenancy.HostDashboard.Dto;
using tmss.MultiTenancy.Payments;
using tmss.MultiTenancy.Payments.Dto;
using tmss.Notifications.Dto;
using tmss.Organizations.Dto;
using tmss.Sessions.Dto;
using tmss.WebHooks.Dto;

namespace tmss
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();


            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicParameter, DynamicParameterDto>().ReverseMap();
            configuration.CreateMap<DynamicParameterValue, DynamicParameterValueDto>().ReverseMap();
            configuration.CreateMap<EntityDynamicParameter, EntityDynamicParameterDto>()
                .ForMember(dto => dto.DynamicParameterName,
                    options => options.MapFrom(entity => entity.DynamicParameter.ParameterName));
            configuration.CreateMap<EntityDynamicParameterDto, EntityDynamicParameter>();

            configuration.CreateMap<EntityDynamicParameterValue, EntityDynamicParameterValueDto>().ReverseMap();
            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();


            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
            // OrderMethod Master mapping 
            configuration.CreateMap<OrderMethodDto, OrderMethod>().ReverseMap();
            configuration.CreateMap<CreateOrEditOrderMethodDto, OrderMethod>().ReverseMap();

            // Master Service Bank
            configuration.CreateMap<MstSrvBankDto, MstSrvBank>().ReverseMap();
            configuration.CreateMap<CreateOrEditBankDto, MstSrvBank>().ReverseMap();

            // Master Service PartsType
            configuration.CreateMap<MstSrvPartsType, PartsTypeDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditPartsTypeDto, MstSrvPartsType>().ReverseMap();

            // Master Service Supplier
            configuration.CreateMap<MstSrvSupplier, SupplierDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditSupplierDto, MstSrvSupplier>().ReverseMap();

            // Master Service CustomerType
            configuration.CreateMap<MstSrvCustomerType, CustomerTypeDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditCustomerTypeDto, MstSrvCustomerType>().ReverseMap();

            // Master Service DeskAdvisor
            configuration.CreateMap<MstSrvDeskAdvisor, DeskAdvisorDto>()
                .ForMember(dto => dto.AdvisorName, opt =>
                opt.MapFrom(e => e.Advisor.EmpName))
                .ForMember(dto => dto.AdvisorCode, opt =>
                opt.MapFrom(e => e.Advisor.EmpCode)).ReverseMap();
            configuration.CreateMap<CreateOrEditDeskAdvisorDto, MstSrvDeskAdvisor>().ReverseMap();
            //Master Dealer
            configuration.CreateMap<CreateOrEditMstGenDealersDto, MstGenDealers>().ReverseMap();
            configuration.CreateMap<MstGenDealersDto, MstGenDealers>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstGenProvinceDto, MstGenProvinces>().ReverseMap();
            configuration.CreateMap<MstGenProvinceDto, MstGenProvinces>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleColorsDto, MstSleColors>().ReverseMap();
            configuration.CreateMap<MstSleColorsDto, MstSleColors>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleYardsDto, MstSleYards>().ReverseMap();
            configuration.CreateMap<MstSleYardsDto, MstSleYards>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleAreasDto, MstSleAreas>().ReverseMap();
            configuration.CreateMap<MstSleAreasDto, MstSleAreas>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleLookupDto, MstSleLookup>().ReverseMap();
            configuration.CreateMap<MstSleLookupDto, MstSleLookup>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleDealerGroupsDto, MstSleDealerGroups>().ReverseMap();
            configuration.CreateMap<MstSleDealerGroupsDto, MstSleDealerGroups>().ReverseMap();

            configuration.CreateMap<CreateOrEditMstSleLogisticCompaniesDto, MstSleLogisticCompanies>().ReverseMap();
            configuration.CreateMap<MstSleLogisticCompaniesDto, MstSleLogisticCompanies>().ReverseMap();

            configuration.CreateMap<MstSrvDeskAdvisor, CreateOrEditDeskAdvisorDto>()
                .ForMember(dto => dto.AdvisorCode, opt =>
                opt.MapFrom(e => e.Advisor.EmpCode))
                .ReverseMap();

            // Master Service Employee
            configuration.CreateMap<EmployeeDto, MstSrvEmployee>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeeDto, MstSrvEmployee>().ReverseMap();

            // Master Service Division
            configuration.CreateMap<MstSrvDivision, DivisionDto>()
                .ForMember(dto => dto.ParentDivName, opt =>
                opt.MapFrom(e => e.ParentDiv.DivName));
            configuration.CreateMap<CreateOrEditDivisionDto, MstSrvDivision>().ReverseMap();

            // Master Service Title
            configuration.CreateMap<MstSrvTitle, TitleDto>().ReverseMap();

            // Master Service Floor
            configuration.CreateMap<MstSrvFloor, FloorDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditFloorDto, MstSrvFloor>().ReverseMap();

            // Master Service Footer
            configuration.CreateMap<MstSrvFooter, FooterDto>()
                .ForMember(dto => dto.TenantFooter, opt =>
                opt.MapFrom(e => e.Footer));
            configuration.CreateMap<MstSrvFooterTmv, FooterDto>()
                .ForMember(dto => dto.FooterTmv, opt =>
                opt.MapFrom(e => e.FooterTmv));
            configuration.CreateMap<MstSrvFooterReportType, FooterDto>().ReverseMap();

            /* Master Sale */

            // Master Sale Model
            configuration.CreateMap<MstSleModels, MstSleModelsDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleModelsDto, MstSleModels>().ReverseMap();
        }
    }
}
