﻿namespace tmss.Services.Quotation.CustomerInfo.Dto
{
    public class GetCustomerInfoForViewDto
    {
        public long? VehicleId { get; set; }
        public string RegisterNo { get; set; }
        public long CustomerId { get; set; }
        public string CarOwnername { get; set; }
        public string CarOwnerMobil { get; set; }
        public string CarOwnerAdd { get; set; }
        public string VinNo { get; set; }
        public long CusVsStatus { get; set; }
        public long CusVsId { get; set; }
        public long RoId { get; set; }
        public long AppId { get; set; }
        public string CusNo { get; set; }
        public string CarOwnerTel { get; set; }
        public string CarOwnerFax { get; set; }
        public string OrgName { get; set; }
        public string TaxCode { get; set; }
        public long BankId { get; set; }
        public string BankCode { get; set; }
        public string AccNo { get; set; }
        public string IsCustomerSaler { get; set; }
    }
}
