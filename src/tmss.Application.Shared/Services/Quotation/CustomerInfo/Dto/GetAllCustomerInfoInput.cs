﻿using Abp.Application.Services.Dto;

namespace tmss.Services.Quotation.CustomerInfo.Dto
{
    public class GetAllCustomerInfoInput : PagedAndSortedResultRequestDto
    {
        public string CarOwnerNameFilter { get; set; }
        public string CarOwnerMobilFilter { get; set; }
        public string RegisterNoFilter { get; set; }
        public string VinNoFilter { get; set; }
        public string CusNoFilter { get; set; }
    }
}