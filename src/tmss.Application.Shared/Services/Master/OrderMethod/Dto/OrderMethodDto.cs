﻿using Abp.Application.Services.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class OrderMethodDto : EntityDto
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
