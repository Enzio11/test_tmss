﻿namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class GetAllOrderMethodForExcelInput
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string NameFilter { get; set; }
    }
}
