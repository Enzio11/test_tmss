﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class CreateOrEditOrderMethodDto : EntityDto<int?>
    {
        [Required]
        [MaxLength(20)]
        public string Code { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
