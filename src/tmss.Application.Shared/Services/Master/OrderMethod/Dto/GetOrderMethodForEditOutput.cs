﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace tmss.Application.Shared.Services.Master.OrderMethod.Dto
{
    public class GetOrderMethodForEditOutput
    {
        public CreateOrEditOrderMethodDto OrderMethod { get; set; }
    }
}
