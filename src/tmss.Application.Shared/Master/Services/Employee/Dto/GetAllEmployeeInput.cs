﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.Employee.Dto
{
    public class GetAllEmployeeInput : PagedAndSortedResultRequestDto
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
    }
}
