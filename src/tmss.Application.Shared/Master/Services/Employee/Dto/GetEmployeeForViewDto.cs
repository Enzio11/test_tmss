﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.Employee.Dto
{
    public class GetEmployeeForViewDto : EntityDto<long>
    {

        public EmployeeDto Employee { get; set; }
    }
}
