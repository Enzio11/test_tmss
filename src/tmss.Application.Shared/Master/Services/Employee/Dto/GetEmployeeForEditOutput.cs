﻿namespace tmss.Master.Services.Employee.Dto
{
    public class GetEmployeeForEditOutput
    {
        public CreateOrEditEmployeeDto Employee { get; set; }
    }
}
