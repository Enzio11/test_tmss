﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.Master.Services.DeskAdvisor.Dto;
using tmss.Master.Services.Employee.Dto;

namespace tmss.Master.Services.DeskAdvisor
{
    public interface IMstSrvDeskAdvisorAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeskAdvisorForViewDto>> GetAll(GetAllDeskAdvisorInput input);

        Task<GetDeskAdvisorForViewDto> GetDeskAdvisorForView(int id);

        Task<GetDeskAdvisorForEditOutput> GetDeskAdvisorForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeskAdvisorDto input);

        Task Delete(EntityDto input);

        Task<List<EmployeeDto>> getAllAdvisorsByTenantId();
    }
}
