﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.DeskAdvisor.Dto
{
    public class GetDeskAdvisorForViewDto : EntityDto<long>
    {
        public DeskAdvisorDto DeskAdvisor { get; set; } 
    }
}
