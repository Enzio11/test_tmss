﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.Footer.Dto
{
    public class GetAllFooterInput : PagedAndSortedResultRequestDto
    {
        public int TenantId { get; set; }
    }
}
