﻿namespace tmss.Master.Services.Footer.Dto
{
    public class GetFooterForEditOutputDto
    {
        public CreateOrEditFooterDto FooterReportType { get; set; }
    }
}
