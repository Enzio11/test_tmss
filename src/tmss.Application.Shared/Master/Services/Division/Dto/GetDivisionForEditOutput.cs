﻿namespace tmss.Master.Services.Division.Dto
{
    public class GetDivisionForEditOutput
    {
        public CreateOrEditDivisionDto Division { get; set; }
    }
}
