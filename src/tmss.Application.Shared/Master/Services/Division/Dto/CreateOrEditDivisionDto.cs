﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Division.Dto
{
    public class CreateOrEditDivisionDto : EntityDto<long?>
    {
        public long? ParentDivId { get; set; }
        [Required]
        [StringLength(20)]
        public string DivCode { get; set; }
        [Required]
        [StringLength(50)]
        public string DivName { get; set; }
        [StringLength(200)]
        public string Des { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
    }
}
