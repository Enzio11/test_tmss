﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.Master.Services.Division.Dto;

namespace tmss.Master.Services.Division
{
    public interface IMstSrvDivisionAppService : IApplicationService
    {
        Task<List<DivisionDto>> GetAll();

        Task<GetDivisionForViewDto> GetDivisionForView(int id);

        Task<GetDivisionForEditOutput> GetDivisionForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDivisionDto input);

        Task Delete(EntityDto input);
    }
}
