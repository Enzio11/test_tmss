﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Services.CustomerType.Dto
{
    public class GetCustomerTypeForViewDto : EntityDto<long>
    {
        public CustomerTypeDto CustomerType { get; set; }
    }
}
