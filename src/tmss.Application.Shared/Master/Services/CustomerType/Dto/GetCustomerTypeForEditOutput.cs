﻿namespace tmss.Master.Services.CustomerType.Dto
{
    public class GetCustomerTypeForEditOutput
    {
        public CreateOrEditCustomerTypeDto CustomerType { get; set; }
    }
}
