﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Services.CustomerType.Dto;

namespace tmss.Master.Services.CustomerType
{
    public interface IMstSrvCustomerTypeAppService : IApplicationService
    {
        Task<PagedResultDto<GetCustomerTypeForViewDto>> GetAll(GetAllCustomerTypeInput input);

        Task<GetCustomerTypeForViewDto> GetCustomerTypeForView(int id);

        Task<GetCustomerTypeForEditOutput> GetCustomerTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCustomerTypeDto input);

        Task Delete(EntityDto input);
    }
}
