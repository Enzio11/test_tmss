﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.Services.Floor.Dto;

namespace tmss.Master.Services.Floor
{
    public interface IMstSrvFloorAppService : IApplicationService
    {
        Task<PagedResultDto<GetFloorForViewDto>> GetAll(GetAllFloorInput input);

        Task<GetFloorForViewDto> GetFloorForView(int id);

        Task<GetFloorForEditOuput> GetFloorForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditFloorDto input);
    }
}
