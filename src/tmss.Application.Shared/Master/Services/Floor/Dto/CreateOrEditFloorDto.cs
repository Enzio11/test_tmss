﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Floor.Dto
{
    public class CreateOrEditFloorDto : EntityDto<long?>
    {
        [StringLength(200)]
        public string FloorName { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        [StringLength(1)]
        public string Type { get; set; }
    }
}
