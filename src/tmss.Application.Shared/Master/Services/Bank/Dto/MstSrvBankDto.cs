﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Services.Bank.Dto
{
    public class MstSrvBankDto : EntityDto<long>
    {
        [StringLength(20)]
        public string BankCode { get; set; }
        [StringLength(50)]
        public string BankName { get; set; }
        [StringLength(30)]
        public string Tel { get; set; }
        [StringLength(30)]
        public string Fax { get; set; }
        [StringLength(100)]
        public string BankAdd { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
    }
}
