﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.ServicesPart.PartsType.Dto
{
    public class CreateOrEditPartsTypeDto : EntityDto<long?>
    {
        [Required]
        [StringLength(50)]
        public string GTypeCode { get; set; }
        [StringLength(100)]
        public string GTypeName { get; set; }
        [StringLength(200)]
        public string Remark { get; set; }
        public long? SearchAll { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
    }
}
