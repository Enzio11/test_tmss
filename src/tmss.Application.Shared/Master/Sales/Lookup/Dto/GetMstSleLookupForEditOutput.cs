﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstSleLookupForEditOutput
    {
        public CreateOrEditMstSleLookupDto MstSleLookup { get; set; }


    }
}