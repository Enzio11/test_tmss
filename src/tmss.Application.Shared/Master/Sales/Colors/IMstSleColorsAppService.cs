﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Dto;

namespace tmss.Master.Sales
{
    public interface IMstSleColorsAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleColorsForViewDto>> GetAll(GetAllMstSleColorsInput input);

        Task<GetMstSleColorsForViewDto> GetMstSleColorsForView(long id);

        Task<GetMstSleColorsForEditOutput> GetMstSleColorsForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleColorsDto input);

        Task Delete(EntityDto<long> input);


    }
}