﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstSleColorsForEditOutput
    {
        public CreateOrEditMstSleColorsDto MstSleColors { get; set; }


    }
}