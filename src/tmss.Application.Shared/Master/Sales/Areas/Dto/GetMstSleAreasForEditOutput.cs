﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstSleAreasForEditOutput
    {
        public CreateOrEditMstSleAreasDto MstSleAreas { get; set; }


    }
}