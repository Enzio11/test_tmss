﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Master.Sales.Dtos
{
    public class GetAllMstSleAreasInput : PagedAndSortedResultRequestDto, ISortedResultRequest
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public string StatusFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public decimal? MaxOrderingFilter { get; set; }
        public decimal? MinOrderingFilter { get; set; }
        public decimal YardId { get; set; }

        public string IsNoneAssignmentFilter { get; set; }

        public DateTime? MaxCreationTimeFilter { get; set; }
        public DateTime? MinCreationTimeFilter { get; set; }

        public DateTime? MaxLastModificationtTimeFilter { get; set; }
        public DateTime? MinLastModificationtTimeFilter { get; set; }

        public int IsDeletedFilter { get; set; }

        public DateTime? MaxDeletionTimeFilter { get; set; }
        public DateTime? MinDeletionTimeFilter { get; set; }



    }
}