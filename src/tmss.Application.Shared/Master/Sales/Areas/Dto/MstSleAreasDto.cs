﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class MstSleAreasDto : EntityDto<long>
    {
        public string Name { get; set; }

        public string Status { get; set; }

        public string Description { get; set; }

        public decimal? Ordering { get; set; }
        public decimal YardId { get; set; }

        public string IsNoneAssignment { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }



    }
}