﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Dto;
using tmss.Master.Sales.Provinces.Dto;

namespace tmss.Master.Sales
{
    public interface IMstGenProvincesAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstGenProvincesForViewDto>> GetAll(GetAllMstGenProvincesInput input);

        Task<GetMstGenProvincesForViewDto> GetMstGenProvincesForView(long id);

        Task<GetMstGenProvincesForEditOutput> GetMstGenProvincesForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstGenProvinceDto input);

        Task Delete(EntityDto<long> input);



    }
}