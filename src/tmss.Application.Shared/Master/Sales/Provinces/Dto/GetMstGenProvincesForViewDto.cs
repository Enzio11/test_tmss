﻿using tmss.Master.Sales.Provinces.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstGenProvincesForViewDto
    {
        public MstGenProvinceDto MstGenProvinces { get; set; }


    }
}