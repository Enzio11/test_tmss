﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Provinces.Dto
{
    public class CreateOrEditMstGenProvinceDto : Entity<long?>
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        public decimal? Ordering { get; set; }
        [Required]
        public string Status { get; set; }
        public decimal? PopulationAmount { get; set; }
        public decimal? SquareAmount { get; set; }
        public int SubRegionId { get; set; }
        [Required]
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationtTime { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }

    }
}
