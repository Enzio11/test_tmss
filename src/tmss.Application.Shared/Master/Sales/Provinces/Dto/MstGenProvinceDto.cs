﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Sales.Provinces.Dto
{
    public class MstGenProvinceDto: EntityDto<long>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public decimal? Ordering { get; set; }

        public string Status { get; set; }

        public decimal? PopulationAmount { get; set; }

        public decimal? SquareAmount { get; set; }
        public int SubRegionId { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }
    }
}
