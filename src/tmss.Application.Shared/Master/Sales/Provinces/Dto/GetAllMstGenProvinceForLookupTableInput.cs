﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using tmss.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedInputDto, ISortedResultRequest
    {
        public string Filter { get; set; }
    }
}