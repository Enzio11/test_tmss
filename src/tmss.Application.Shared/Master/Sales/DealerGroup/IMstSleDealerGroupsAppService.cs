﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Dto;
using tmss.Master.Sales.DealerGroup.Dto;

namespace tmss.Master.Sales.DealerGroup
{
    public interface IMstSleDealerGroupsAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleDealerGroupsForViewDto>> GetAll(GetAllMstSleDealerGroupsInput input);

        Task<GetMstSleDealerGroupsForViewDto> GetMstSleDealerGroupsForView(long id);

        Task<GetMstSleDealerGroupsForEditOutput> GetMstSleDealerGroupsForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleDealerGroupsDto input);

        Task Delete(EntityDto<long> input);

    }
}