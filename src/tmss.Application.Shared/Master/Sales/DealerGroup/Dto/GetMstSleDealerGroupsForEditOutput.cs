﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.DealerGroup.Dto
{
    public class GetMstSleDealerGroupsForEditOutput
    {
        public CreateOrEditMstSleDealerGroupsDto MstSleDealerGroups { get; set; }


    }
}