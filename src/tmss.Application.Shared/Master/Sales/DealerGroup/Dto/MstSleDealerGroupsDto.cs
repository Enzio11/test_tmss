﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.DealerGroup.Dto
{
    public class MstSleDealerGroupsDto : EntityDto<long>
    {
        public string GroupName { get; set; }

        public string Description { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }
    }
}