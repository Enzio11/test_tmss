﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Sales.Models.Dto
{
    public class GetAllMstSleModelsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string MarketingCodeFilter { get; set; }
        public string ProductionCodeFilter { get; set; }
        public string VnNameFilter { get; set; }
        public string EnNameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public string StatusFilter { get; set; }
        public DateTime? MaxCreationTimeFilter { get; set; }
        public DateTime? MinCreationTimeFilter { get; set; }

        public DateTime? MaxLastModificationTimeFilter { get; set; }
        public DateTime? MinLastModificationTimeFilter { get; set; }
        public decimal? MaxOrderingFilter { get; set; }
        public decimal? MinOrderingFilter { get; set; }
        public decimal? MinOrderingRptFilter { get; set; }
        public decimal? MaxOrderingRptFilter { get; set; }
        public int IsDeletedFilter { get; set; }

        public DateTime? MaxDeletionTimeFilter { get; set; }
        public DateTime? MinDeletionTimeFilter { get; set; }
    }
}
