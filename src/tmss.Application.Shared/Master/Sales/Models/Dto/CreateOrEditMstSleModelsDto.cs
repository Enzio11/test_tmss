﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace tmss.Master.Sales.Models.Dto
{
    public class CreateOrEditMstSleModelsDto : EntityDto<long?>
    {
        [Required]
        public string EnName { get; set; }
        [Required]
        public string VnName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int Ordering { get; set; }
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        [Required]
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }

    }
}
