﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tmss.Master.Sales.Models.Dto
{
    public class GetMstSleModelsForEditOutput
    {
        public CreateOrEditMstSleModelsDto MstSleModels { get; set; }
    }
}
