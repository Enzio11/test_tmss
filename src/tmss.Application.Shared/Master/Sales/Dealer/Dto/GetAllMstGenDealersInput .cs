﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Master.Sales.Dtos
{
	public class GetAllMstGenDealersInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public DateTime? MaxCreationTimeFilter { get; set; }
		public DateTime? MinCreationTimeFilter { get; set; }

		public DateTime? MaxLastModificationTimeFilter { get; set; }
		public DateTime? MinLastModificationTimeFilter { get; set; }

		public int IsDeletedFilter { get; set; }

		public DateTime? MaxDeletionTimeFilter { get; set; }
		public DateTime? MinDeletionTimeFilter { get; set; }

		public string CodeFilter { get; set; }

		public string AccountNoFilter { get; set; }

		public string BankFilter { get; set; }

		public string BankAddressFilter { get; set; }

		public string VnNameFilter { get; set; }

		public string EnNameFilter { get; set; }

		public string AddressFilter { get; set; }

		public string AbbreviationFilter { get; set; }

		public string ContactPersonFilter { get; set; }

		public string PhoneFilter { get; set; }

		public string StatusFilter { get; set; }

		public string FaxFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string IsSpecialFilter { get; set; }

		public int? MaxOrderingFilter { get; set; }
		public int? MinOrderingFilter { get; set; }

		public string TaxCodeFilter { get; set; }

		public string IsSumDealerFilter { get; set; }

		public string BiServerFilter { get; set; }

		public string TfsAmountFilter { get; set; }

		public string PartLeadtimeFilter { get; set; }

		public string IpAddressFilter { get; set; }

		public string IslexusFilter { get; set; }

		public string IsSellLexusPartFilter { get; set; }

		public string IsDlrSalesFilter { get; set; }

		public string RecievingAddressFilter { get; set; }

		public string DlrFooterFilter { get; set; }

		public string IsPrintFilter { get; set; }

		public string PasswordSearchVinFilter { get; set; }

		public decimal? MaxPortRegionFilter { get; set; }
		public decimal? MinPortRegionFilter { get; set; }



	}
}