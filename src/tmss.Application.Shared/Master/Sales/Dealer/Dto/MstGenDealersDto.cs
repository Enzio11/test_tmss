﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.Dtos
{
	public class MstGenDealersDto : EntityDto<long>
	{
		public DateTime CreationTime { get; set; }

		public DateTime? LastModificationTime { get; set; }

		public bool IsDeleted { get; set; }

		public DateTime? DeletionTime { get; set; }

		public string Code { get; set; }

		public string AccountNo { get; set; }

		public string Bank { get; set; }

		public string BankAddress { get; set; }

		public string VnName { get; set; }

		public string EnName { get; set; }

		public string Address { get; set; }

		public string Abbreviation { get; set; }

		public string ContactPerson { get; set; }

		public string Phone { get; set; }

		public string Status { get; set; }

		public string Fax { get; set; }

		public string Description { get; set; }

		public string IsSpecial { get; set; }

		public int Ordering { get; set; }

		public string TaxCode { get; set; }

		public string IsSumDealer { get; set; }

		public string BiServer { get; set; }

		public string TfsAmount { get; set; }

		public string PartLeadtime { get; set; }

		public string IpAddress { get; set; }

		public string Islexus { get; set; }

		public string IsSellLexusPart { get; set; }

		public string IsDlrSales { get; set; }

		public string RecievingAddress { get; set; }

		public string DlrFooter { get; set; }

		public string IsPrint { get; set; }

		public string PasswordSearchVin { get; set; }

		public decimal PortRegion { get; set; }



	}
}