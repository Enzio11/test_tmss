﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.Dtos;
using tmss.Dto;

namespace tmss.Master.Sales
{
    public interface IMstGenDealersAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstGenDealersForViewDto>> GetAll(GetAllMstGenDealersInput input);

        Task<GetMstGenDealersForViewDto> GetMstGenDealersForView(long id);

        Task<GetMstGenDealersForEditOutput> GetMstGenDealersForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstGenDealersDto input);

        Task Delete(EntityDto<long> input);


    }
}