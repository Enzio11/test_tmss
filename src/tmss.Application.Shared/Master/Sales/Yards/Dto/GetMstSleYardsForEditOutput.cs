﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class GetMstSleYardsForEditOutput
    {
        public CreateOrEditMstSleYardsDto MstSleYards { get; set; }


    }
}