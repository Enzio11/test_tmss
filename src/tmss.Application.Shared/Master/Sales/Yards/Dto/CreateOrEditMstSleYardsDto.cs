﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.Master.Sales.Dtos
{
    public class CreateOrEditMstSleYardsDto : EntityDto<long?>
    {

        [Required]
        public string Code { get; set; }


        public string Name { get; set; }


        public string Address { get; set; }


        [Required]
        public string Status { get; set; }


        public decimal? Ordering { get; set; }


        public string Description { get; set; }


        [Required]
        public DateTime CreationTime { get; set; }


        public DateTime? LastModificationtTime { get; set; }


        [Required]
        public bool IsDeleted { get; set; }


        public DateTime? DeletionTime { get; set; }



    }
}