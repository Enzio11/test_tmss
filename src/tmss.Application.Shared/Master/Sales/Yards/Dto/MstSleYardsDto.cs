﻿
using System;
using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.Dtos
{
    public class MstSleYardsDto : EntityDto<long>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

        public string Description { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? LastModificationtTime { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletionTime { get; set; }



    }
}