﻿using Abp.Application.Services.Dto;

namespace tmss.Master.Sales.LogisticCompany.Dto
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
