﻿using Abp.Application.Services.Dto;
using System;
namespace tmss.Master.Sales.LogisticCompany.Dto
{
    public class GetAllMstSleLogisticCompaniesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string VnNameFilter { get; set; }

        public string EnNameFilter { get; set; }

        public string TaxCodeFilter { get; set; }

        public string BankNoFilter { get; set; }

        public string TelFilter { get; set; }

        public string FaxFilter { get; set; }

        public string AddressFilter { get; set; }

        public string ContactPersonFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public string OwnerTypeFilter { get; set; }

        public string StatusFilter { get; set; }

        public decimal? MaxOrderingFilter { get; set; }
        public decimal? MinOrderingFilter { get; set; }

        public long? MaxLogisticsNumTofsFilter { get; set; }
        public long? MinLogisticsNumTofsFilter { get; set; }

        public DateTime? MaxCreationTimeFilter { get; set; }
        public DateTime? MinCreationTimeFilter { get; set; }

        public DateTime? MaxLastModificationtTimeFilter { get; set; }
        public DateTime? MinLastModificationtTimeFilter { get; set; }

        public int IsDeletedFilter { get; set; }

        public DateTime? MaxDeletionTimeFilter { get; set; }
        public DateTime? MinDeletionTimeFilter { get; set; }



    }
}
