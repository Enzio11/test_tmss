﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using tmss.Master.Sales.LogisticCompany.Dto;

namespace tmss.Master.Sales.LogisticCompany
{
    public interface IMstSleLogisticCompaniesAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleLogisticCompaniesForViewDto>> GetAll(GetAllMstSleLogisticCompaniesInput input);

        Task<GetMstSleLogisticCompaniesForViewDto> GetMstSleLogisticCompaniesForView(long id);

        Task<GetMstSleLogisticCompaniesForEditOutput> GetMstSleLogisticCompaniesForEdit(EntityDto<long> input);

        Task CreateOrEdit(CreateOrEditMstSleLogisticCompaniesDto input);

        Task Delete(EntityDto<long> input);


    }
}
