﻿using Abp.Application.Services.Dto;

namespace tmss.Master.ServicesRepairProgress.WorshopType.Dto
{
  public class WorkshopTypeListDto : FullAuditedEntityDto
  {
    public string WorkshopTypeCode { get; set; }
    public string WorkshopTypeName { get; set; }
    public string WorkshopTypeDesc { get; set; }
    public string ColorCode { get; set; }
    public bool IsActive { get; set; }
  }
}