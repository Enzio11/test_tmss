﻿using System.ComponentModel.DataAnnotations;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress.WorshopType.Dto
{
  public class CreateOrUpdateWorkshopTypeInput
  {
    [Range(1, int.MaxValue)]
    public int? Id { get; set; }

    [Required]
    [MaxLength(Constants.MaxNameLength)]
    public string WorkshopTypeCode { get; set; }

    [MaxLength(Constants.MaxNameLength)]
    public string WorkshopTypeName { get; set; }

    [MaxLength(Constants.MaxDescLength)]
    public string WorkshopTypeDesc { get; set; }

    [MaxLength(Constants.MaxNameLength)]
    public string ColorCode { get; set; }

    [Required]
    public bool IsActive { get; set; }
  }
}