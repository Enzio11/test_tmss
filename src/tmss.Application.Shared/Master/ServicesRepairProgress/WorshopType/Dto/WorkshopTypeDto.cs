﻿namespace tmss.Master.ServicesRepairProgress.WorshopType.Dto
{
  public class WorkshopTypeDto
  {
    public int WorkshopTypeId { get; set; }
    public string WorkshopTypeCode { get; set; }
  }
}