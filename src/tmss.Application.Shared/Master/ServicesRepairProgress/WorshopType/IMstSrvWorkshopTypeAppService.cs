﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.ServicesRepairProgress.WorshopType.Dto;

namespace tmss.Master.ServicesRepairProgress.WorshopType
{
  public interface IMstSrvWorkshopTypeAppService : IApplicationService
  {
    Task<PagedResultDto<WorkshopTypeListDto>> GetvWorkshopTypes(GetWorkshopTypesInput input);
    Task<GetWorkshopTypeForEditOutput> GetWorkshopTypeForEdit(NullableIdDto<int> input);
    Task CreateOrUpdateWorkshopType(CreateOrUpdateWorkshopTypeInput input);
    Task DeleteWorkshopType(EntityDto<int> input);
  }
}