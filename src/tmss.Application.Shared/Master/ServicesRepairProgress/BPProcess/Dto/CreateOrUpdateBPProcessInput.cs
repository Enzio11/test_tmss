﻿using System.ComponentModel.DataAnnotations;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress.BPProcess.Dto
{
  public class CreateOrUpdateBPProcessInput
  {
    [Range(1, int.MaxValue)]
    public int? Id { get; set; }

    [Required]
    [MaxLength(Constants.MaxNameLength)]
    public string ProcessName { get; set; }

    [MaxLength(Constants.MaxDescLength)]
    public string ProcessDesc { get; set; }

    [Required]
    [MaxLength(Constants.MaxNameLength)]
    public string ColorCode { get; set; }

    [Required]
    public int Ordering { get; set; }

    [Required]
    public bool IsActive { get; set; }
  }
}