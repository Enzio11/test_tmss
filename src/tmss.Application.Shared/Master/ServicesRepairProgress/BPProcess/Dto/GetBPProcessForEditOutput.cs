﻿namespace tmss.Master.ServicesRepairProgress.BPProcess.Dto
{
  public class GetBPProcessForEditOutput
  {
    public string ProcessName { get; set; }
    public string ProcessDesc { get; set; }
    public string ColorCode { get; set; }
    public int Ordering { get; set; }
    public bool IsActive { get; set; }
    public int? DealerId { get; set; }
  }
}