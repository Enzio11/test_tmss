﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Master.ServicesRepairProgress.BPGroup.Dto;

namespace tmss.Master.ServicesRepairProgress.BPGroup
{
  public interface IMstSrvBPGroupAppService : IApplicationService
  {
    Task<PagedResultDto<BPGroupListDto>> GetBPGroups(GetBPGroupsInput input);
    Task<GetBPGroupForEditOutput> GetBPGroupForEdit(NullableIdDto<int> input);
    Task CreateOrUpdateBPGroup(CreateOrUpdateBPGroupInput input);
    Task DeleteBPGroup(EntityDto<int> input);
    Task<int> GetMaxOrdering();
  }
}