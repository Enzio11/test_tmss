﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using tmss.Dto;

namespace tmss.Master.ServicesRepairProgress.BPGroup.Dto
{
  public class GetBPGroupsInput : PagedAndSortedInputDto, IShouldNormalize, ISortedResultRequest
  {
    public string Filter { get; set; }
    public bool IsActive { get; set; }

    public void Normalize()
    {
      if (string.IsNullOrEmpty(Sorting))
      {
        Sorting = "Ordering,GroupName";
      }

      Filter = Filter?.Trim();
    }
  }
}