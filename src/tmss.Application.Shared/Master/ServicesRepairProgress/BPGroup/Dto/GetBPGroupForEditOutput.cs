﻿using Abp.Application.Services.Dto;

namespace tmss.Master.ServicesRepairProgress.BPGroup.Dto
{
  public class GetBPGroupForEditOutput : FullAuditedEntityDto
  {
    public string GroupName { get; set; }
    public string GroupDesc { get; set; }
    public int Ordering { get; set; }
    public bool IsActive { get; set; }
    public int? DealerId { get; set; }
  }
}