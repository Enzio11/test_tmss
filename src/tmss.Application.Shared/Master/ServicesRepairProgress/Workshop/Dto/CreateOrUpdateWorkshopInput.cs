﻿using System.ComponentModel.DataAnnotations;
using tmss.Services.RepairProgress;

namespace tmss.Master.ServicesRepairProgress.Workshop.Dto
{
  public class CreateOrUpdateWorkshopInput
  {
    [Range(1, int.MaxValue)]
    public int? Id { get; set; }

    [Required]
    [MaxLength(Constants.MaxNameLength)]
    public string WorkshopCode { get; set; }

    [MaxLength(Constants.MaxNameLength)]
    public string WorkshopName { get; set; }

    [MaxLength(Constants.MaxDescLength)]
    public string WorkshopDesc { get; set; }

    [Required]
    public int Ordering { get; set; }

    [Required]
    public bool IsActive { get; set; }

    [Required]
    public int[] WorkshopTypes { get; set; }
  }
}