﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using tmss.Master.ServicesRepairProgress.WorshopType.Dto;

namespace tmss.Master.ServicesRepairProgress.Workshop.Dto
{
  public class WorkshopListDto : FullAuditedEntityDto
  {
    public string WorkshopCode { get; set; }
    public string WorkshopName { get; set; }
    public string WorkshopDesc { get; set; }
    public int Ordering { get; set; }
    public bool IsActive { get; set; }    
    public List<WorkshopTypeDto> WorkshopTypes { get; set; }
  }
}