﻿using tmss.Master.ServicesRepairProgress.WorshopType.Dto;

namespace tmss.Master.ServicesRepairProgress.Workshop.Dto
{
  public class GetWorkshopForEditOutput
  {
    public string WorkshopCode { get; set; }
    public string WorkshopName { get; set; }
    public string WorkshopDesc { get; set; }
    public int Ordering { get; set; }
    public bool IsActive { get; set; }
    public WorkshopTypeDto[] WorkshopTypes { get; set; }
  }
}