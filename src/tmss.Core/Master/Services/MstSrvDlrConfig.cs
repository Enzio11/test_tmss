﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Services
{
    [Table("MstSrvDlrConfig")]
    public partial class MstSrvDlrConfig : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [Column(TypeName = "datetime")]
        public DateTime? WkAmFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? WkAmTo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? WkPmFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? WkPmTo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RestAmFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RestAmTo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RestPmFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RestPmTo { get; set; }
        public double? Cost { get; set; }
        public double? CoEfficientB { get; set; }
        public double? CoEfficientJg { get; set; }
        public double? CoEfficientP { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffDateFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffDateTo { get; set; }
        public byte[] Logo { get; set; }
        [StringLength(2000)]
        public string FooterDoc { get; set; }
        public long? TaxJobs { get; set; }
        public long? LogLv { get; set; }
        [Required]
        [StringLength(1)]
        public string UseProgress { get; set; }
        public int WashTime { get; set; }
        public int WaitTime { get; set; }
        public int ReceptionTimeBp { get; set; }
        public int ReceptionTimeScc { get; set; }
        public int WarningTime { get; set; }
        public int WshopGjChangeTime { get; set; }
        public int WshopBpChangeTime { get; set; }
        public int LateAppointmentTime { get; set; }
        public int? LateSuggested { get; set; }
        public int ReservePartsPct { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? HolidayDate { get; set; }
        [Required]
        [StringLength(1)]
        public string GuessReceptionOrder { get; set; }
        public int? Reception { get; set; }
        [Required]
        [StringLength(1)]
        public string UseProgressDs { get; set; }
        public int? SplitTimeScc { get; set; }
        public int? SplitTimeDs { get; set; }
        public int? SplitTimeRx { get; set; }
        public int TenantId { get; set; }
    }
}