﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Services
{
    [Table("MstSrvFooter")]
    public partial class MstSrvFooter : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [Key]
        public long Id { get; set; }
        public long? TypeId { get; set; }
        public MstSrvFooterReportType? Type { get; set; }
        [StringLength(2000)]
        public string Footer { get; set; }
        [StringLength(2000)]
        public string FooterTmv { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int TenantId { get; set; }
    }
}