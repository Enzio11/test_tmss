﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tmss.Master.Services;

namespace tmss.Master.Services
{
    [Table("MstSrvDeskAdvisor")]
    public partial class MstSrvDeskAdvisor : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [StringLength(50)]
        public string DeskName { get; set; }
        //Add Foreign Key - Begin
        public long? AdvisorId { get; set; }
        public MstSrvEmployee Advisor { get; set; }
        // Add Foreign Key - End
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        public DateTime? BusyToDate { get; set; }
        public DateTime? UpdateStatusDate { get; set; }
        public long? Ordering { get; set; }
        [StringLength(1)]
        public int TenantId { get; set; }

    }
}