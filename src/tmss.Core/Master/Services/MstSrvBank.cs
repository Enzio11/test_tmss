﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Core.Master.Services
{
    [Table("MstSrvBank")]
    public partial class MstSrvBank : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [StringLength(20)]
        public string BankCode { get; set; }
        [StringLength(50)]
        public string BankName { get; set; }
        [StringLength(30)]
        public string Tel { get; set; }
        [StringLength(30)]
        public string Fax { get; set; }
        [StringLength(100)]
        public string BankAdd { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        public int TenantId { get; set; }
    }
}