﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using System.Reflection.Emit;
using Microsoft.AspNetCore.DataProtection.XmlEncryption;

namespace tmss.Master.Sale
{
    [Table("MstSleDealerGroup")]
    public class MstSleDealerGroups : FullAuditedEntity<long>,IEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }


        public virtual string GroupName { get; set; }

        public virtual string Description { get; set; }

        public virtual string Status { get; set; }

        public virtual decimal? Ordering { get; set; }


    }
}