﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace tmss.Master.Sales
{
    [Table("MstSleModel")]
    public class MstSleModels : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual string MarketingCode { get; set; }
        public virtual string ProductionCode { get; set; }
        public virtual string EnName { get; set; }
        public virtual string VnName { get; set; }

        public virtual string Description { get; set; }

        public virtual string Status { get; set; }

        public virtual int Ordering { get; set; }
        public virtual int OrderingRpt { get; set; }
    }
}
