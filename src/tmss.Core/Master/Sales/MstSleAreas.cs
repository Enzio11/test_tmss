﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace tmss.Master.Sales
{
    [Table("MstSleArea")]
    public class MstSleAreas : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }


        [Required]
        public virtual string Name { get; set; }

        public virtual string Status { get; set; }

        public virtual string Description { get; set; }

        public virtual decimal? Ordering { get; set; }
        [Required]
        public virtual long YardId { get; set; }

        public virtual string IsNoneAssignment { get; set; }


    }
}