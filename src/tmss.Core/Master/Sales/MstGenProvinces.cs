﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Org.BouncyCastle.Math;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master.Sales
{
    public partial class MstGenProvinces : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        [Required]
        [StringLength(1)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }
        
        [Column(TypeName = "numeric(12, 2)")]
        public decimal? SquareAmount { get; set; }
        [Column(TypeName = "bigint")]
        public int SubRegionId { get; set; }

        public int TenantId { get; set; }
    }
}