﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace tmss.Master.Sales
{
    [Table("MstSleColors")]
    public class MstSleColors : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }


        [Required]
        public virtual string Code { get; set; }

        public virtual string VnName { get; set; }

        public virtual string EnName { get; set; }

        public virtual string Description { get; set; }

        public virtual string Status { get; set; }

        public virtual decimal? Ordering { get; set; }

        public virtual decimal? OrderingRpt { get; set; }

    }
}