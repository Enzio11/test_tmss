﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace tmss.Master.Sales
{
	[Table("MstGenDealers")]
	public class MstGenDealers : FullAuditedEntity<long>,IEntity<long> , IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual string Code { get; set; }

		public virtual string AccountNo { get; set; }

		public virtual string Bank { get; set; }

		public virtual string BankAddress { get; set; }

		public virtual string VnName { get; set; }

		public virtual string EnName { get; set; }

		public virtual string Address { get; set; }

		public virtual string Abbreviation { get; set; }

		public virtual string ContactPerson { get; set; }

		public virtual string Phone { get; set; }

		public virtual string Status { get; set; }

		public virtual string Fax { get; set; }

		public virtual string Description { get; set; }

		public virtual string IsSpecial { get; set; }

		[Required]
		public virtual int Ordering { get; set; }

		public virtual string TaxCode { get; set; }

		public virtual string IsSumDealer { get; set; }

		public virtual string BiServer { get; set; }

		public virtual string TfsAmount { get; set; }

		public virtual string PartLeadtime { get; set; }

		public virtual string IpAddress { get; set; }

		public virtual string Islexus { get; set; }

		public virtual string IsSellLexusPart { get; set; }

		public virtual string IsDlrSales { get; set; }

		public virtual string RecievingAddress { get; set; }

		public virtual string DlrFooter { get; set; }

		public virtual string IsPrint { get; set; }

		public virtual string PasswordSearchVin { get; set; }

		[Required]
		public virtual decimal PortRegion { get; set; }


	}
}