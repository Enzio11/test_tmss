﻿using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_WORKSHOP_TYPE")] // ~ SRV_M_WSHOP_TYPE
  public class MstSrvWorkshopType : FullAuditedEntity
  {
    public virtual string WorkshopTypeCode { get; set; }
    public virtual string WorkshopTypeName { get; set; }
    public virtual string WorkshopTypeDesc { get; set; }
    public virtual string ColorCode { get; set; }
    public virtual bool IsActive { get; set; }
  }
}