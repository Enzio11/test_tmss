﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Master.ServicesRepairProgress
{
  // [Table("MST_SRV_BP_PROCESS")] // ~ SRV_M_BPCONFIG
  public class MstSrvBPProcess : FullAuditedEntity, IMustHaveTenant
  {
    public virtual string ProcessName { get; set; }
    public virtual string ProcessDesc { get; set; }
    public virtual string ColorCode { get; set; }
    public virtual int Ordering { get; set; }
    public virtual bool IsActive { get; set; }

    public virtual int TenantId { get; set; }
  }
}