﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace tmss.Services.Quotation
{
    [Table("SrvQuoCustomerDetails")]
    public class SrvQuoCustomerDetail : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public long? CusId { get; set; }

        //[StringLength(100)]
        public string Name { get; set; }

        //[StringLength(20)]
        public string IdNumber { get; set; }

        //[StringLength(40)]
        public string Tel { get; set; }

        //[StringLength(20)]
        public string Mobil { get; set; }

        //[StringLength(200)]
        public string Address { get; set; }

        //[StringLength(50)]
        public string Email { get; set; }

        //[StringLength(1)]
        public string Type { get; set; }

        //[StringLength(1)]
        public string CallTime { get; set; }

        //[StringLength(1)]
        public string CusVerify { get; set; }

        //[StringLength(1)]
        public string Status { get; set; }

        public long? Ordering { get; set; }

        public int TenantId { get; set; }

        public SrvQuoCustomer srvQuoCustomer { get; set; }

        public IList<SrvQuoCusVisit> SrvQuoCusVisits { get; set; }
    }
}
