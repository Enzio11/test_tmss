﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using tmss.Core.Shared.Services.RepairProgress;
using tmss.Master.ServicesRepairProgress;

namespace tmss.Services.RepairProgress
{
  // [Table("SRV_PRG_ACTUAL")] // ~ SRV_B_RO_WSHOP_ACTUAL
  public class SrvPrgActual : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
  {
    // TODO: chuyen thanh cac bang tuong ung
    public virtual int? AppointmentId { get; set; }
    public virtual int? CustomerVisitId { get; set; }
    public virtual int? ROId { get; set; }
    public virtual int VehicleId { get; set; }
    public virtual int CustomerId { get; set; }
    public virtual int PendingId { get; set; }

    public virtual MstSrvWorkshop MstSrvWorkshop { get; set; }
    public virtual SrvPrgPlan SrvPrgPlan { get; set; }
    public virtual MstSrvBPProcess MstSrvBPProcess { get; set; }
    public virtual MstSrvBPGroup MstSrvBPGroup { get; set; }

    public virtual string PendingNote { get; set; }
    public virtual ROType JobType { get; set; }
    public virtual QCLevel QCLevel { get; set; }
    public virtual bool? IsCustomerWait { get; set; }
    public virtual bool? IsCarWash { get; set; }
    public virtual bool? IsTakeParts { get; set; }
    public virtual bool? IsPriority { get; set; }
    public virtual DateTime? ActualFromTime { get; set; }
    public virtual DateTime? ActualToTime { get; set; }
    public virtual int? ActualCalcTime { get; set; }
    public virtual RPState? ActualState { get; set; }

    public virtual int TenantId { get; set; }
  }
}
