﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using tmss.Core.Shared.Services.RepairProgress;
using tmss.Master.ServicesRepairProgress;

namespace tmss.Services.RepairProgress
{
  // [Table("SRV_PRG_PLAN")]  // ~ SRV_B_RO_WSHOPS_PLAN
  public class SrvPrgPlan : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
  {
    // TODO: chuyen thanh cac bang tuong ung
    public virtual int? AppointmentId { get; set; }
    public virtual int? CustomerVisitId { get; set; }
    public virtual int? ROId { get; set; }
    public virtual int VehicleId { get; set; }
    public virtual int CustomerId { get; set; }

    public virtual MstSrvWorkshop MstSrvWorkshop { get; set; }
    public virtual MstSrvBPProcess MstSrvBPProcess { get; set; }
    public virtual MstSrvBPGroup MstSrvBPGroup { get; set; }
    public virtual SrvPrgActual SrvPrgActual { get; set; }

    public virtual ROType JobType { get; set; }
    public virtual QCLevel QCLevel { get; set; }
    public virtual bool? IsCustomerWait { get; set; }
    public virtual bool? IsCarWash { get; set; }
    public virtual bool? IsTakeParts { get; set; }
    public virtual bool? IsPriority { get; set; }
    public virtual DateTime? PlanFromTime { get; set; }
    public virtual DateTime? PlanToTime { get; set; }
    public virtual int? PlanCalcTime { get; set; }

    public virtual int TenantId { get; set; }
  }
}