﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using tmss.Core.Shared.Services.RepairProgress;
using tmss.Master.ServicesRepairProgress;

namespace tmss.Services.RepairProgress
{
  // [Table("SRV_PRG_EMP_PLAN")] // ~ SRV_B_RO_WSHOP_EMP_PLAN
  public class SrvPrgEmpPlan : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
  {
    // TODO: chuyen thanh bang DEALER, EMPLOYEE
    public virtual int EmployeeId { get; set; }

    public virtual SrvPrgPlan SrvPrgPlan { get; set; }
    public virtual SrvPrgActual SrvPrgActual { get; set; }
    public virtual SrvPrgEmpActual SrvPrgEmpActual { get; set; }
    public virtual MstSrvWorkshop MstSrvWorkshop { get; set; }
    public virtual MstSrvBPProcess MstSrvBPProcess { get; set; }
    public virtual MstSrvBPGroup MstSrvBPGroup { get; set; }

    public virtual ROType JobType { get; set; }
    public virtual DateTime? EmployeePlanFromTime { get; set; }
    public virtual DateTime? EmployeePlanToTime { get; set; }
    public virtual int? EmployeePlanCalcTime { get; set; }

    public virtual int TenantId { get; set; }
  }
}
