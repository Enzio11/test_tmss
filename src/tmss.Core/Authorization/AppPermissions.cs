﻿namespace tmss.Authorization
{
  /// <summary>
  /// Defines string constants for application's permission names.
  /// <see cref="AppAuthorizationProvider"/> for permission definitions.
  /// </summary>
  public static class AppPermissions
  {
    //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

    public const string Pages = "Pages";

    // Order Method Master
    public const string Pages_OrderMethods = "Pages.OrderMethods";
    public const string Pages_OrderMethods_Create = "Pages.OrderMethods.Create";
    public const string Pages_OrderMethods_Edit = "Pages.OrderMethods.Edit";
    public const string Pages_OrderMethods_Delete = "Pages.OrderMethods.Delete";

    public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
    public const string Pages_Administration = "Pages.Administration";

    public const string Pages_Administration_Roles = "Pages.Administration.Roles";
    public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
    public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
    public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

    public const string Pages_Administration_Users = "Pages.Administration.Users";
    public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
    public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
    public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
    public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
    public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
    public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

    public const string Pages_Administration_Languages = "Pages.Administration.Languages";
    public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
    public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
    public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
    public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

    public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

    public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
    public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
    public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
    public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

    public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

    public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

    public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
    public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
    public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
    public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
    public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
    public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
    public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

    public const string Pages_Administration_DynamicParameters = "Pages.Administration.DynamicParameters";
    public const string Pages_Administration_DynamicParameters_Create = "Pages.Administration.DynamicParameters.Create";
    public const string Pages_Administration_DynamicParameters_Edit = "Pages.Administration.DynamicParameters.Edit";
    public const string Pages_Administration_DynamicParameters_Delete = "Pages.Administration.DynamicParameters.Delete";

    public const string Pages_Administration_DynamicParameterValue = "Pages.Administration.DynamicParameterValue";
    public const string Pages_Administration_DynamicParameterValue_Create = "Pages.Administration.DynamicParameterValue.Create";
    public const string Pages_Administration_DynamicParameterValue_Edit = "Pages.Administration.DynamicParameterValue.Edit";
    public const string Pages_Administration_DynamicParameterValue_Delete = "Pages.Administration.DynamicParameterValue.Delete";

    public const string Pages_Administration_EntityDynamicParameters = "Pages.Administration.EntityDynamicParameters";
    public const string Pages_Administration_EntityDynamicParameters_Create = "Pages.Administration.EntityDynamicParameters.Create";
    public const string Pages_Administration_EntityDynamicParameters_Edit = "Pages.Administration.EntityDynamicParameters.Edit";
    public const string Pages_Administration_EntityDynamicParameters_Delete = "Pages.Administration.EntityDynamicParameters.Delete";

    public const string Pages_Administration_EntityDynamicParameterValue = "Pages.Administration.EntityDynamicParameterValue";
    public const string Pages_Administration_EntityDynamicParameterValue_Create = "Pages.Administration.EntityDynamicParameterValue.Create";
    public const string Pages_Administration_EntityDynamicParameterValue_Edit = "Pages.Administration.EntityDynamicParameterValue.Edit";
    public const string Pages_Administration_EntityDynamicParameterValue_Delete = "Pages.Administration.EntityDynamicParameterValue.Delete";
    //TENANT-SPECIFIC PERMISSIONS

    public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

    public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

    public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

    //HOST-SPECIFIC PERMISSIONS

    public const string Pages_Editions = "Pages.Editions";
    public const string Pages_Editions_Create = "Pages.Editions.Create";
    public const string Pages_Editions_Edit = "Pages.Editions.Edit";
    public const string Pages_Editions_Delete = "Pages.Editions.Delete";
    public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

    public const string Pages_Tenants = "Pages.Tenants";
    public const string Pages_Tenants_Create = "Pages.Tenants.Create";
    public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
    public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
    public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
    public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

    public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
    public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
    public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    // MASTER
    public const string Pages_Master = "Pages.Master";
    // MASTER -- Common
    public const string Pages_Master_Common = "Pages.Master.Common";
    // MASTER -- Sales
    public const string Pages_Master_Sales = "Pages.Master.Sales";
        public const string Pages_MstGenDealers = "Pages.Master.Sales.MstGenDealers";
        public const string Pages_MstGenDealers_Create = "Pages.Master.Sales.MstGenDealers.Create";
        public const string Pages_MstGenDealers_Edit = "Pages.Master.Sales.MstGenDealers.Edit";
        public const string Pages_MstGenDealers_Delete = "Pages.Master.Sales.MstGenDealers.Delete";

        public const string Pages_MstGenProvinces = "Pages.MstGenProvinces";
        public const string Pages_MstGenProvinces_Create = "Pages.MstGenProvinces.Create";
        public const string Pages_MstGenProvinces_Edit = "Pages.MstGenProvinces.Edit";
        public const string Pages_MstGenProvinces_Delete = "Pages.MstGenProvinces.Delete";

        public const string Pages_MstSleColors = "Pages.MstSleColors";
        public const string Pages_MstSleColors_Create = "Pages.MstSleColors.Create";
        public const string Pages_MstSleColors_Edit = "Pages.MstSleColors.Edit";
        public const string Pages_MstSleColors_Delete = "Pages.MstSleColors.Delete";

        public const string Pages_MstSleYards = "Pages.MstSleYards";
        public const string Pages_MstSleYards_Create = "Pages.MstSleYards.Create";
        public const string Pages_MstSleYards_Edit = "Pages.MstSleYards.Edit";
        public const string Pages_MstSleYards_Delete = "Pages.MstSleYards.Delete";

        public const string Pages_MstSleAreas = "Pages.MstSleAreas";
        public const string Pages_MstSleAreas_Create = "Pages.MstSleAreas.Create";
        public const string Pages_MstSleAreas_Edit = "Pages.MstSleAreas.Edit";
        public const string Pages_MstSleAreas_Delete = "Pages.MstSleAreas.Delete";

        public const string Pages_MstSleLookup = "Pages.MstSleLookup";
        public const string Pages_MstSleLookup_Create = "Pages.MstSleLookup.Create";
        public const string Pages_MstSleLookup_Edit = "Pages.MstSleLookup.Edit";
        public const string Pages_MstSleLookup_Delete = "Pages.MstSleLookup.Delete";

        public const string Pages_MstSleDealerGroups = "Pages.Master.Sales.MstSleDealerGroups";
        public const string Pages_MstSleDealerGroups_Create = "Pages.Master.Sales.MstSleDealerGroups.Create";
        public const string Pages_MstSleDealerGroups_Edit = "Pages.Master.Sales.MstSleDealerGroups.Edit";
        public const string Pages_MstSleDealerGroups_Delete = "Pages.Master.Sales.MstSleDealerGroups.Delete";

        public const string Pages_MstSleLogisticCompanies = "Pages.MstSleLogisticCompanies";
        public const string Pages_MstSleLogisticCompanies_Create = "Pages.MstSleLogisticCompanies.Create";
        public const string Pages_MstSleLogisticCompanies_Edit = "Pages.MstSleLogisticCompanies.Edit";
        public const string Pages_MstSleLogisticCompanies_Delete = "Pages.MstSleLogisticCompanies.Delete";

        public const string Pages_Master_Sales_Model = "Pages.Master.Sales.Model";
        public const string Pages_Master_Sales_Model_CreateEdit = "Pages.Master.Sales.Model.CreateEdit";
        public const string Pages_Master_Sales_Model_Delete = "Pages.Master.Sales.Model.Delete";
        //MASTER -- Services
        public const string Pages_Master_Services = "Pages.Master.Services";
        //Bank Master Service 
        public const string Pages_Master_Services_Bank = "Pages.Master.Services.Bank";
        public const string Pages_Master_Services_Bank_List = "Pages.Master.Services.Bank.List";
        public const string Pages_Master_Services_Bank_CreateEdit = "Pages.Master.Services.Bank.CreateEdit";
        public const string Pages_Master_Services_Bank_Delete = "Pages.Master.Services.Bank.Delete";

        // Supplier Master Service
        public const string Pages_Master_Services_Supplier = "Pages.Master.Services.Supplier";
        public const string Pages_Master_Services_Supplier_CreateEdit = "Pages.Master.Services.Supplier.CreateEdit";
        public const string Pages_Master_Services_Supplier_Delete = "Pages.Master.Services.Supplier.Delete";

        //CustomerType Master Service
        public const string Pages_Master_Services_CustomerType = "Pages.Master.Services.CustomerType";
        public const string Pages_Master_Services_CustomerType_CreateEdit = "Pages.Master.Services.CustomerType.CreateEdit";
        public const string Pages_Master_Services_CustomerType_Delete = "Pages.Master.Services.CustomerType.Delete";

        //Desk Advisor Master Service
        public const string Pages_Master_Services_DeskAdvisor = "Pages.Master.Services.DeskAdvisor";
        public const string Pages_Master_Services_DeskAdvisor_CreateEdit = "Pages.Master.Services.DeskAdvisor.CreateEdit";
        public const string Pages_Master_Services_DeskAdvisor_Delete = "Pages.Master.Services.DeskAdvisor.Delete";

        // Employee Service Master
        public const string Pages_Master_Services_Employee = "Pages.Master.Services.Employee";
        public const string Pages_Master_Services_Employee_CreateEdit = "Pages.Master.Services.Employee.CreateEdit";
        public const string Pages_Master_Services_Employee_Delete = "Pages.Master.Services.Employee.Delete";

        // Division Service Master
        public const string Pages_Master_Services_Division = "Pages.Master.Services.Division";
        public const string Pages_Master_Services_Division_CreateEdit = "Pages.Master.Services.Division.CreateEdit";
        public const string Pages_Master_Services_Division_Delete = "Pages.Master.Services.Division.Delete";

        //Master Service Floor
        public const string Pages_Master_Services_Floor = "Pages.Master.Services.Floor";
        public const string Pages_Master_Services_Floor_CreateEdit = "Pages.Master.Services.Floor.CreateEdit";

        //Master Service Footer
        public const string Pages_Master_Services_Footer = "Pages.Master.Services.Footer";
        public const string Pages_Master_Services_Footer_CreateEdit = "Pages.Master.Services.Footer.CreateEdit";
        public const string Pages_Master_Services_Footer_Delete = "Pages.Master.Services.Footer.Delete";

        // MASTER -- Services Part
        public const string Pages_Master_ServicesPart = "Pages.Master.ServicesPart";
    //PartsType Master Service
    public const string Pages_Master_ServicesPart_PartsType = "Pages.Master.ServicesPart.PartsType";
    public const string Pages_Master_ServicesPart_PartsType_CreateEdit = "Pages.Master.ServicesPart.PartsType.CreateEdit";
    public const string Pages_Master_ServicesPart_PartsType_Delete = "Pages.Master.ServicesPart.PartsType.Delete";
    // MASTER -- Services Quotation
    public const string Pages_Master_ServicesQuotation = "Pages.Master.ServicesQuotation";
    // MASTER -- Services Repair Progress
    public const string Pages_Master_ServicesRepairProgress = "Pages.Master.ServicesRepairProgress";
    public const string Pages_Master_ServicesRepairProgress_BPGroup = "Pages.Master.ServicesRepairProgress.BPGroup";
    public const string Pages_Master_ServicesRepairProgress_BPGroup_List = "Pages.Master.ServicesRepairProgress.BPGroup.List";
    public const string Pages_Master_ServicesRepairProgress_BPGroup_CreateEdit = "Pages.Master.ServicesRepairProgress.BPGroup.CreateEdit";
    public const string Pages_Master_ServicesRepairProgress_BPGroup_Delete = "Pages.Master.ServicesRepairProgress.BPGroup.Delete";
    // MASTER -- Services Warranty
    public const string Pages_Master_ServicesWarranty = "Pages.Master.ServicesWarranty";

    // SALES
    public const string Pages_Sales = "Pages.Sales";
    // SALES -- Contract
    public const string Pages_Sales_Contract = "Pages.Sales.Contract";
    // SALES -- Vehicle
    public const string Pages_Sales_Vehicle = "Pages.Sales.Vehicle";

    // SERVICES
    public const string Pages_Services = "Pages.Services";
    // SERVICES -- Part
    public const string Pages_Services_Part = "Pages.Services.Part";
    // SERVICES -- Quotation
    public const string Pages_Services_Quotation = "Pages.Services.Quotation";
    // SERVICES -- Repair Progress
    public const string Pages_Services_RepairProgress = "Pages.Services.RepairProgress";
    // SERVICES -- Warranty
    public const string Pages_Services_Warranty = "Pages.Services.Warranty";
  }
}