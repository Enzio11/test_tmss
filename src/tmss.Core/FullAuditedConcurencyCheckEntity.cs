﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace tmss
{
    public abstract class FullAuditedConcurencyCheckEntity : FullAuditedEntity
    {
        public virtual byte[] RowVersion { get; set; }
    }
}
