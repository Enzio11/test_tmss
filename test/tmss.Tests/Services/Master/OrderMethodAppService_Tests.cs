﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Runtime.Validation;
using Shouldly;
using tmss.Application.Shared.Services.Master.OrderMethod;
using tmss.Application.Shared.Services.Master.OrderMethod.Dto;
using Xunit;

namespace tmss.Tests.Services.Master
{
    public class OrderMethodAppService_Test : AppTestBase
    {
        private readonly IOrderMethodAppService _orderMethodAppService;

        public OrderMethodAppService_Test()
        {
            _orderMethodAppService = Resolve<IOrderMethodAppService>();
        }

        // Test GET w/o filter
        //[Fact]
        //public void Should_Get_All_OrderMethod_Without_Any_Filter()
        //{
        //    // Act
        //    var orderMethods = _orderMethodAppService.GetOrderMethod(new GetOrderMethodInput());

        //    // Assert
        //    orderMethods.Items.Count.ShouldBe(2);
        //}

        // Test GET with filter
        //[Fact]
        //public void Should_Get_All_OrderMethod_With_Filter()
        //{
        //    // Act
        //    var orderMethods = _orderMethodAppService.GetOrderMethod(new GetOrderMethodInput
        //    {
        //        Filter = "Phu"
        //    });

        //    // Assert
        //    orderMethods.Items.Count.ShouldBe(1);
        //    orderMethods.Items[0].Name.ShouldBe("Phu kien");
        //    orderMethods.Items[0].Code.ShouldBe("A");
        //}

        // Test CREATE with VALID arguments
        //[Fact]
        //public async Task Should_Create_OrderMethod_With_Valid_Arguments()
        //{
        //    // Act
        //    await _orderMethodAppService.CreateOrderMethod(
        //        new CreateOrderMethodInput
        //        {
        //            Code = "B",
        //            Name = "Back Order"
        //        });

        //    // Assert
        //    UsingDbContext(
        //        context =>
        //        {
        //            var b = context.OrderMethods.FirstOrDefault(om => om.Code == "B");
        //            b.ShouldNotBe(null);
        //            b.Name.ShouldBe("Back Order");
        //        });
        //}

        // Test CREATE with INVALID arguments
        //public async Task Should_Not_Create_OrderMethod_With_Invalid_Arguments()
        //{
        //    // Act and Assert
        //    await Assert.ThrowsAsync<AbpValidationException>(
        //        async () => {
        //            await _orderMethodAppService.CreateOrderMethod(new CreateOrderMethodInput
        //            {
        //                Name = "Test"
        //            }); ;
        //        }
        //        );
        //}
    }
}
